import java.io.IOException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:字符串反转
 * User: 我很浮躁
 * Date: 2022-08-28
 * Time: 21:24
 */
public class TestDome {
    public static void main(String []args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        char[] chars = str.toCharArray();
        int right = chars.length-1;
        int left = 0;
        while(left < right){
            char ch = chars[left];
            chars[left] = chars[right];
            chars[right] = ch;
            right--;
            left++;
        }
        System.out.println(chars);
    }
}
