import java.io.IOException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-08-28
 * Time: 21:27
 */
public class TestDome2 {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.nextLine();
        String str2 = sc.nextLine();
        int sum = 0;
        int i=0;
        for(i=0; i<str1.length(); i++){
            int j=0;
            for(j=0; j<str2.length(); j++){
                int count = 0;
                if(str1.charAt(i) == str2.charAt(j)){
                    count = commonCount(i, str1, j, str2);
                }
                //确保相同字符串的个数是否为最大
                if(count > sum){
                    sum = count;
                }
            }
        }
        System.out.println(sum);
    }

    //计算相同字符串的大小
    public static int commonCount(int i, String str1, int j, String str2){
        int count = 0;
        while (i<str1.length() && j<str2.length()){
            if(str1.charAt(i) != str2.charAt(j)){
                break;
            }
            count++;
            i++;
            j++;
        }
        return count;
    }
}
