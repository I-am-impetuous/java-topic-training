/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-11-07
 * Time: 22:02
 */

class TreeNode{

    public int val;
    public TreeNode left;
    public TreeNode right;
     public TreeNode(int val){
         this.val = val;
    }
}

public class TestDome {

    public TreeNode reConstructBinaryTreeCore(int[] pre, int preStart, int preEnd, int[] vin, int vinStart, int vinEnd){
        if(preStart > preEnd || vinStart > vinEnd){
            return null;
        }
        int length = pre.length;
        TreeNode root = new TreeNode(pre[preStart]);
        for(int i=0; i<length; i++){
            if(vin[i] == pre[preStart]){
                root.left = reConstructBinaryTreeCore(pre, preStart+1, preStart+i-vinStart, vin, vinStart, i-1);
                root.right = reConstructBinaryTreeCore(pre, preStart+i-vinStart+1, preEnd, vin, i+1, vinEnd);
                break;
            }
        }
        return root;
    }

    public TreeNode reConstructBinaryTree(int [] pre,int [] vin) {
        if(pre.length == 0 || vin.length == 0){
            return null;
        }
        return reConstructBinaryTreeCore(pre, 0, pre.length-1, vin, 0, vin.length-1);
    }

    public static void main(String[] args) {

    }
}
