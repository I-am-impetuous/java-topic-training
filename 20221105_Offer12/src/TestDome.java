/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-11-05
 * Time: 22:35
 */
public class TestDome {
    public int Fibonacci(int n) {
        if(n == 0){
            return 0;
        }
        int first = 1;
        int second = 1;
        int third = 1; //因为从0开始，third等于n就不用判定了
        while(n > 2){
            third = first + second;
            first = second;
            second = third;
            --n;
        }
        return third;
    }
}
