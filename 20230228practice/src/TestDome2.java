import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-02-28
 * Time: 21:27
 */
public class TestDome2 {
    public static void main(String[] args) {
        String string = "abcab";
        char ch = findChar(string);
        System.out.println(ch);
    }
    public static char findChar(String str){
        HashMap<Character, Integer> hashMap = new HashMap<>();
        for(int i=0; i<str.length(); i++){
            if(hashMap.get(str.charAt(i)) != null){
                hashMap.put(str.charAt(i), hashMap.get(str.charAt(i)) + 1);
            }else{
                hashMap.put(str.charAt(i), 1);
            }
        }
        char ch = ' ';
        for(int i=0; i<str.length(); i++){
            if(hashMap.get(str.charAt(i)) == 1){
                ch = str.charAt(i);
                break;
            }
        }
        return ch;
    }
}
