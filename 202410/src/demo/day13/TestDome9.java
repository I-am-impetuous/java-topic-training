package demo.day13;

import java.util.*;

public class TestDome9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int minAverageLost = sc.nextInt();
        sc.nextLine();
        String[] inputStr = sc.nextLine().split(" ");
        int len = inputStr.length;
        int[] inputArr = new int[len];
        for(int i=0; i<len; i++){
            inputArr[i] = Integer.parseInt(inputStr[i]);
        }
        int sum = 0;
        int count = 0;
        int left = 0;
        int maxTime = 0;
        List<int[]> list = new ArrayList<>();
        for(int i=0; i<len; i++){
            count++;
            sum += inputArr[i];
            if(count*minAverageLost >= sum){
                if(maxTime<=count){
                    maxTime = count;
                    list.add(new int[]{count, left});
                }
            }else {
                while (count*minAverageLost < sum && left<=i){
                    sum -= inputArr[left];
                    left++;
                    count--;
                }
            }
        }
        if(list.size() == 0){
            System.out.println("NULL");
            return;
        }
        for(int[] arr: list){
            if(maxTime == arr[0]){
                System.out.print(arr[1]+"-"+(arr[0]+arr[1]-1)+" ");
            }
        }
    }
}
