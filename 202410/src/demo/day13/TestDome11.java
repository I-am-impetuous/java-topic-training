package demo.day13;

import java.util.*;

public class TestDome11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] inputs = sc.nextLine().split(" ");
        String input1 = inputs[0];
        int len1 = input1.length();

        String input2 = inputs[1];
        int len2 = input2.length();
        int index = -1;
        for(int i=len1; i<=len2; i++){
            String str = input2.substring(i-len1, i);
            int result = isFalg(input1, str, len1);
            if(result == 0){
                index = i-len1;
                break;
            }
        }
        System.out.println(index);
    }

    private static int isFalg(String input1, String str, int len) {
        int[] tolChar = new int[26];
        for(int i=0; i<len; i++){
            char c = input1.charAt(i);
            int pos = c-'a';
            tolChar[pos]++;
        }
        int count = 0;
        for(int i=0; i<len; i++){
            char c = str.charAt(i);
            int pos = c-'a';
            if(tolChar[pos] != 0){
                tolChar[pos]--;
                count++;
            }else {
                break;
            }
        }
        return len - count;
    }
}
