package demo.day13;

import java.util.*;

public class TestDome12 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] arr = new int[num];
        for(int i=0; i<num; i++){
            arr[i] = sc.nextInt();
        }
        int[][] dp = new int[num][num];
        for(int i=0; i<num; i++){
            Arrays.fill(dp[i], -1);
        }
        int ans = 0;
        for(int i=0; i<num; i++){
            ans = Math.max(maxFac((i+num-1)%num, (i+1)%num, num, arr, dp)+arr[i], ans);
        }
        System.out.println(ans);
    }

    private static int maxFac(int left, int right, int num, int[] arr, int[][] dp) {
        if(dp[left][right] != -1){
            return dp[left][right];
        }
        if(arr[left] > arr[right]){
            left = (left+num-1)%num;
        }else {
            right = (right+1)%num;
        }
        if(right == left){
            dp[left][right] = arr[left];
        }else {
            dp[left][right] = Math.max(maxFac(((left+num-1)%num), right, num, arr, dp)+arr[left],
                    maxFac(left, ((right+1)%num), num, arr, dp)+arr[right]);
        }
        return dp[left][right];
    }

}
