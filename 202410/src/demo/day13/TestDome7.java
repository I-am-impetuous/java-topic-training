package demo.day13;

import java.util.*;

public class TestDome7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] input1 = sc.nextLine().split(";");
        String[] input2 = sc.nextLine().split(";");
        HashMap<String, Integer> tempMap = new HashMap<>();
        for(int i=0; i<input1.length; i++){
            String[] temp = input1[i].split(",");
            tempMap.put(temp[0], Integer.parseInt(temp[1]));
        }
        HashMap<String, Integer> map = new HashMap<>();
        for(int i=0; i<input2.length; i++){
            String[] temp = input2[i].split(",");
            if(tempMap.containsKey(temp[0])){
                int tempInt = tempMap.get(temp[0]);
                map.put(temp[0], Integer.parseInt(temp[1])+tempInt);
            }
        }
        if(map.size() == 0){
            System.out.println("NULL");
            return;
        }
        List<String[]> list = new ArrayList<>();
        for(Map.Entry entry: map.entrySet()){
            String[] temp = new String[2];
            temp[0] = entry.getKey().toString();
            temp[1] = entry.getValue().toString();
            list.add(temp);
        }
        list.sort((s1, s2)->{
            int val1 = Integer.parseInt(s1[1]);
            int val2 = Integer.parseInt(s2[1]);
            String class1 = s1[0].substring(0, 5);
            String class2 = s2[0].substring(0, 5);
            String student1 = s1[0].substring(5);
            String student2 = s2[0].substring(5);
            if(!class1.equals(class2)){
                return class1.compareTo(class2);
            }
            if(val1 != val2){
                return val2-val1;
            }
            return student1.compareTo(student2);
        });
        HashMap<String, Integer> tempClass = new HashMap<>();
        String str = "";
        for(String[] strings: list){
            String class1 = strings[0].substring(0, 5);
            if(!tempClass.containsKey(class1)){
                if(str.length() != 0){
                    System.out.println(str.substring(0, str.length()-1));
                    str = "";
                }
                tempClass.put(class1, 1);
                System.out.println(class1);
            }
            str+=strings[0]+";";
        }
        if(str.length() != 0){
            System.out.println(str.substring(0, str.length()-1));
        }
    }
}
