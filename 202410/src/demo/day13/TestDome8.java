package demo.day13;

import java.util.*;

public class TestDome8 {

    public static double exchange(String str){
        switch (str){
            case "CNY":
                return 100.0;
            case "HKD":
                return 10000/123.0;
            case "JPY":
                return 10000/1825.0;
            case "EUR":
                return 10000/14.0;
            case "GBP":
                return 10000/12.0;
            case "fen":
                return 1.0;
            case "cents":
                return 100/123.0;
            case "sen":
                return 100/1825.0;
            case "eurocents":
                return 100/14.0;
            case "pence":
                return 100/12.0;
            default:
                return 0.0;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        sc.nextLine();
        double sum = 0;
        for(int i=0; i<num; i++){
            String input = sc.nextLine();
            int len = input.length();
            String strNum = "";
            String unit = "";
            for(int j=0; j<len; j++){
                char c = input.charAt(j);
                if(c>='0'&&c<='9'){
                    strNum+=c;
                }else {
                    unit+=c;
                }
                if(exchange(unit) != 0.0){
                    sum = sum + Integer.parseInt(strNum)*exchange(unit);
                    unit = "";
                    strNum = "";
                }
            }
        }
        System.out.println((int)sum);
    }
}
