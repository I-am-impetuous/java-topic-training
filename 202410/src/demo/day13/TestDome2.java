package demo.day13;

import java.util.*;

public class TestDome2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] inputF = sc.nextLine().split(" ");
        String addressFirst = inputF[0];
        int num = Integer.parseInt(inputF[1]);
        HashMap<String, String[]> map = new HashMap<>();
        for(int i=0; i<num; i++){
            String[] inputs = sc.nextLine().split(" ");
            map.put(inputs[0], new String[]{inputs[1], inputs[2]});
        }
        String[] fast = map.get(inputF[0]);
        String[] slow = map.get(inputF[0]);
        while (fast!=null && map.containsKey(fast[1])){
            fast = map.get(fast[1]);
            if(fast == null){
                break;
            }
            fast = map.get(fast[1]);
            slow = map.get(slow[1]);
        }
        System.out.println(slow[0]);
    }
}
