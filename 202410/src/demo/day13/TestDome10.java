package demo.day13;

import java.util.*;

public class TestDome10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] inputs = sc.nextLine().split(" ");
        int len = inputs.length;
        int[] arr = new int[len];
        for(int i=0; i<len; i++){
            arr[i] = Integer.parseInt(inputs[i]);
            if(arr[i] == 0){
                System.out.println(-1);
                return;
            }
        }
        for(int i=0; i<len; i++){
            if(arr[i]<0){
                int left = i-1;
                while (left >= 0 && arr[i] != 0){
                    if(arr[left] <= 0){
                        left--;
                        continue;
                    }
                    if(arr[left] >= -arr[i]){
                        arr[left] += arr[i];
                        arr[i] = 0;
                    }else {
                        arr[i] += arr[left];
                        arr[left] = 0;
                    }
                    left--;
                }
            }
        }
        int count = 0;
        for(int i=0; i<len; i++){
            if(arr[i] != 0){
                count++;
            }
        }
        System.out.println(count);
    }
}
