
package demo.day13;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.StringJoiner;

public class Test {
    static class Student {
        String studentId; // 学生学号
        String classId; // 班级编号
        int score1 = -1; // 第一门选修课成绩
        int score2 = -1; // 第二门选修课成绩

        public int getSumScore() { // 计算两门选修课成绩和
            return this.score1 + this.score2;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String scores1 = sc.nextLine(); // 第一门选修课学生的成绩
        String scores2 = sc.nextLine(); // 第二门选修课学生的成绩
        HashMap<String, Student> students = new HashMap<>(); // 存储学生信息的HashMap
        divide(scores1, 1, students); // 将第一门选修课学生成绩划分到HashMap中
        divide(scores2, 2, students); // 将第二门选修课学生成绩划分到HashMap中
        Student[] selectedStudents = students.values().stream().filter(stu -> stu.score1 != -1 && stu.score2 != -1)
                .toArray(Student[]::new); // 选取同时选修了两门选修课的学生
        if (selectedStudents.length == 0) {
            System.out.println("NULL"); // 如果没有同时选修两门选修课的学生，则输出NULL
            return;
        }
        HashMap<String, ArrayList<Student>> ans = new HashMap<>(); // 存储按班级划分的学生信息的HashMap
        for (Student stu : selectedStudents) {
            ans.putIfAbsent(stu.classId, new ArrayList<>()); // 如果班级还没有被加入HashMap中，则加入
            ans.get(stu.classId).add(stu); // 将学生加入对应班级的ArrayList中
        }
        ans.keySet().stream().sorted(String::compareTo).forEach(classId -> {
            System.out.println(classId); // 先输出班级编号
            ArrayList<Student> studentsInClass = ans.get(classId);
            studentsInClass.sort((a, b) -> a.getSumScore() != b.getSumScore() ? b.getSumScore() - a.getSumScore()
                    : a.studentId.compareTo(b.studentId)); // 按照成绩和的降序和学号的升序排序
            StringJoiner sj = new StringJoiner(";"); // 用于拼接学生学号的StringJoiner
            for (Student student : studentsInClass)
                sj.add(student.studentId); // 将学生学号加入StringJoiner中
            System.out.println(sj); // 输出学生学号
        });
    }

    public static void divide(String str, int courseId, HashMap<String, Student> students) {
        for (String sub : str.split(";")) {
            String[] tmp = sub.split(",");
            String studentId = tmp[0]; // 学生学号
            String classId = studentId.substring(0, 5); // 班级编号
            int score = Integer.parseInt(tmp[1]); // 选修课成绩
            students.putIfAbsent(studentId, new Student()); // 如果学生还没有被加入HashMap中，则加入
            Student stu = students.get(studentId);
            stu.studentId = studentId;
            stu.classId = classId;
            if (courseId == 1)
                stu.score1 = score; // 将第一门选修课成绩加入学生对象中
            else
                stu.score2 = score; // 将第二门选修课成绩加入学生对象中
        }
    }
}


