package demo.day13;

import com.sun.scenario.effect.impl.prism.PrReflectionPeer;

import java.util.*;

public class TestDome6 {
    public static void main(String[] args) {
        //1、输入
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int K = sc.nextInt();
        int x = sc.nextInt();
        int[] arr = new int[num];
        for(int i=0; i<arr.length; i++){
            arr[i] = sc.nextInt();
        }
        //2、使用priority来保存数据
        PriorityQueue<int[]> queue = new PriorityQueue<>((s1, s2)->{
            return s2[0]-s1[0];
        });
        //3、得到的数据用数组保存
        for(int i=0; i<arr.length; i++){
            int temp = Math.abs(x-arr[i]);
            if(queue.size() < K){
                queue.offer(new int[]{temp, arr[i]});
            }else {
                int[] tempArr= queue.peek();
                if(tempArr[0] > temp || (tempArr[0]==temp && tempArr[1]>arr[i])){
                    queue.poll();
                    queue.add(new int[]{temp, arr[i]});
                }
            }
        }
        List<Integer> list = new LinkedList<>();
        while (!queue.isEmpty()){
            int[] tempArr= queue.poll();
            list.add(tempArr[1]);
        }
        Collections.sort(list);
        //4、输出
        String out = "";
        for(int n: list){
            out += n + " ";
        }
        System.out.println(out);
    }
}
