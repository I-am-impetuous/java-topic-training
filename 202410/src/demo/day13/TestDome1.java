package demo.day13;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] arr = new int[num];
        for(int i=0; i<num; i++){
            arr[i] = sc.nextInt();
        }
        Arrays.sort(arr);
        List<Integer> list = new LinkedList<>();
        if(num == 0){
            System.out.println(0);
            return;
        }
        list.add(arr[0]);
        for(int i=1; i<num; i++){
            boolean flag = false;
            for(int a: list){
                if(arr[i]%a == 0){
                    flag = true;
                    break;
                }
            }
            if(!flag){
                list.add(arr[i]);
            }
        }
        System.out.println(list.size());
    }
}
