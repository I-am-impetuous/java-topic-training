package demo.day13;

import java.util.*;

public class TestDome4 {
    public static void main(String[] args) {
        //1、输入
        Scanner sc = new Scanner(System.in);
        String[] tempIn = sc.nextLine().split(" ");
        int K = sc.nextInt();
        int[] arr = new int[tempIn.length];
        int len = arr.length;
        int size = 0;
        //2、有多少数据是小于K的，作为滑动窗口的大小
        for(int i=0; i<len; i++){
            arr[i] = Integer.parseInt(tempIn[i]);
            if(arr[i] < K){
                size++;
            }
        }
        if(size == 1){
            System.out.println(0);
            return;
        }
        //3、进行滑动窗口遍历，在滑动窗口中找到最多小于K值的，并记录由多少个
        int maxCount = 0;
        int count = 0;
        int left = 0;
        for(int right = 0; right<len; right++){
            if(right-left >= size){
                if(arr[left] < K){
                    count--;
                }
                left++;
            }
            if(arr[right] < K){
                count++;
                maxCount = Math.max(count, maxCount);
            }
        }
        //4、输出
        System.out.println(size - maxCount);
    }
}
