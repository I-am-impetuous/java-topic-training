package demo.day13;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] arr = new int[num];
        int sum = 0;
        for(int i=0; i<num; i++){
            arr[i] = sc.nextInt();
            sum += arr[i];
        }
        int leftSum = arr[0];
        int rightSum = sum - arr[0];
        int maxDiff = Math.abs(leftSum-rightSum);
        for(int i=1; i<arr.length-1; i++){
            leftSum += arr[i];
            rightSum = sum-leftSum;
            int temp = Math.abs(leftSum-rightSum);
            maxDiff = Math.max(maxDiff, temp);
        }
        System.out.println(maxDiff);
    }
}
