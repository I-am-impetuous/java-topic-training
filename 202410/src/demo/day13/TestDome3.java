package demo.day13;

import java.util.*;

public class TestDome3 {
    public static void main(String[] args) {
        //1、输入
        Scanner sc = new Scanner(System.in);
        String[] inputStr = sc.nextLine().split("#");
        //2、判断字符串是否非法
        //没有前置的0，都是0-9的数字，大小最多为三位，整体的数字大小为0-255之内，第一个数字为0-128.
        boolean invalid = true;
        int len = inputStr.length;

        invalid = isIpv4(inputStr);
        if(!invalid){
            System.out.println("invalid IP");
            return;
        }

        //3、计算32为数字
        long sum = 0;
        for(int i=0; i<4; i++){
            sum = sum*256+Integer.parseInt(inputStr[i]);
        }
        //4、输出
        System.out.println(sum);
    }

    public static boolean isIpv4(String[] inputStr){
        int len = inputStr.length;
        if(len != 4){
            return false;
        }else {
            for(int i=0; i<len; i++){
                if(inputStr[i].length() > 3 || inputStr[i].length() == 0){
                    return false;
                }else {
                    char c = inputStr[i].charAt(0);
                    if(c == '0' && inputStr[i].length() != 1){
                        return false;
                    }
                }
            }
        }

        for(int i=0; i<len; i++){
            int tempLen = inputStr[i].length();
            int sum = 0;
            for(int j=0; j<tempLen; j++){
                char c = inputStr[i].charAt(j);
                sum = sum*10 + Integer.parseInt(""+c);
                if(c < '0' || c>'9'){
                    return false;
                }
                if(sum < 0 || sum > 255){
                    return false;
                }
            }
            if(i == 0 && (sum>128)){
                return false;
            }
        }
        return true;
    }
}
