package demo.day11;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] inputs = sc.nextLine().split(" ");
        int person = sc.nextInt();
        int[] arr = new int[inputs.length];
        int sum = 0;
        for(int i=0; i<arr.length; i++){
            arr[i] = Integer.parseInt(inputs[i]);
            sum+=arr[i];
        }
        Arrays.sort(arr);
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        if(person >= arr.length){
            System.out.println(arr[arr.length-1]);
            return;
        }
        for(int i=0; i<person; i++){
            queue.offer(arr[arr.length-1-i]);
        }
        for(int i=person; i<arr.length; i++){
            int tempN = queue.poll();
            queue.offer(tempN+arr[arr.length-1-i]);
        }
        for(int i=0; i<person-1; i++){
            queue.poll();
        }
        int right = queue.poll();
        int left = sum/person;
        while (left<right){
            int mid = (left+right)/2;
            int[] workers = new int[person];
            if(backTrack(arr, workers, mid, arr.length-1)){
                right = mid;
            }else {
                left = mid+1;
            }
        }
        System.out.println(left);
    }

    private static boolean backTrack(int[] arr, int[] workers, int mid, int index) {
        if(index < 0){
            return true;
        }
        int nowTack = arr[index];
        for(int i=0; i<workers.length; i++){
            if(workers[i]+nowTack <= mid){
                workers[i]+=nowTack;
                if(backTrack(arr, workers, mid, index-1)){
                    return true;
                }
                workers[i] -= nowTack;
            }
            if(workers[i]==mid || workers[i]+nowTack==mid){
                break;
            }
        }
        return false;
    }
}
