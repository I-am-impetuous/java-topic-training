package demo.day11;

import java.util.*;

public class TestDome2 {

    public static int[][] pos = new int[][]{{1,0},{0,1},{-1,0},{0,-1}};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int col = sc.nextInt();
        int[][] arr = new int[row][col];
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                arr[i][j] = sc.nextInt();
            }
        }
        int[][] dp = new int[row+1][col+1];
        for(int i=0; i<row; i++){
            Arrays.fill(arr[i], (Integer.MAX_VALUE/2));
        }

        for(int i=1; i<=row; i++){
            for(int j=1; j<=col; j++){
                if(i==1&&j==1){
                    dp[i][j] = arr[i-1][j-1];
                }else {
                    if(arr[i-1][j-1] == -1){
                        dp[i][j] = 0;
                    }else if(arr[i-1][j-1] == 0){
                        dp[i][j] = -1;
                    }else {
                        if(dp[i-1][j] != -1 && dp[i][j-1] != -1){
                            dp[i][j] = Math.min(dp[i-1][j], dp[i][j-1])+arr[i-1][j-1];
                            if(dp[i][j] > 100){
                                dp[i][j] = -1;
                            }
                        }else {
                            dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1])+arr[i-1][j-1];
                            if(dp[i][j] > 100){
                                dp[i][j] = -1;
                            }
                        }
                    }
                }
            }
        }
    }
}
