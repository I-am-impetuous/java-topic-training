package demo.day11;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;
import java.util.*;

public class TestDome3 {

    public static final int MaxOil = 100;
    public static int[][] pos = new int[][]{{1,0},{-1,0},{0,1},{0,-1}};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in).useDelimiter(",|\\s+");
        int row = sc.nextInt();
        int col = sc.nextInt();
        int[][] arr = new int[row][col];
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                arr[i][j] = sc.nextInt();
            }
        }

        //使用二分法判断汽车耗油
        int left = 0, right = MaxOil, suitOil = -1;
        while(left <= right){
            int mid = (left+right)/2;
            //使用BFS来寻找路线，如果小于零就不能达到
            if(isReachToEnd(mid, arr, row, col)){
                suitOil = mid;
                right = mid-1;
            }else {
                left = mid+1;
            }
        }
        System.out.println(suitOil);
    }

    //使用BFS来寻找路线，如果小于零就不能达到

    private static boolean isReachToEnd(int mid, int[][] arr, int row, int col) {
        if(arr[0][0] == 0){
            return false;
        }
        //保存每个位置的油量
        int[][] temp = new int[row][col];
        for(int i=0; i<row; i++){
            Arrays.fill(temp[i], -1);
        }
        temp[0][0] = arr[0][0] == -1 ? MaxOil : mid - arr[0][0];
        PriorityQueue<int[]> queue = new PriorityQueue<>((s1, s2)->{
            return s2[2] - s1[2];
        });
        queue.offer(new int[]{0, 0, temp[0][0]});
        while (!queue.isEmpty()){
            int[] diffOil = queue.poll();
            if(diffOil[0]==row-1&&diffOil[1]==col-1){
                return true;
            }
            for(int i=0; i<4; i++){
                int x1 = diffOil[0] + pos[i][0];
                int y1 = diffOil[1] + pos[i][1];
                if(x1>=0&&x1<row&&y1>=0&&y1<col&&arr[x1][y1]!=0){
                    int nowOil = -1;
                    if(arr[x1][y1] == -1){
                        nowOil = MaxOil;
                    }else if(arr[x1][y1] <= diffOil[2]){
                        nowOil = diffOil[2] - arr[x1][y1];
                    }
                    if(nowOil>temp[x1][y1]){
                        queue.offer(new int[]{x1, y1, nowOil});
                        temp[x1][y1] = nowOil;
                    }
                }
            }
        }
        return false;
    }
}
