package demo.day11;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        int len = input.length();
        for(int i=0; i<len; i++){
            char c = input.charAt(i);
            if(c<'a' || c>'z'){
                System.out.println("!error");
                return;
            }
        }
        StringBuffer buffer = new StringBuffer();
        int right = 0;
        for(int left = 0; left<len; left++){
            char c = input.charAt(left);
            right = left+1;
            while(right < len && input.charAt(right) == c){
                right++;
            }
            int sampleChar = right-left;
            if(sampleChar > 2){
                buffer.append(""+sampleChar+c);
                left = right-1;
            }else if(sampleChar == 2){
                buffer.append(""+c+c);
                left = right-1;
            }else {
                buffer.append(c);
            }
        }
        System.out.println(buffer.toString());
    }
}
