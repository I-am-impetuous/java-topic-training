package demo.day11;

import java.util.*;

public class TestDome4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] inputs = sc.nextLine().split(" ");
        String inputStr = inputs[0];
        int len = Integer.parseInt(inputs[1]);
        HashSet<String> set = new HashSet<>();
        int tempLen = inputStr.length();
        boolean[] booleans = new boolean[tempLen];
        tarString(inputStr, len, "", set, booleans);
        System.out.println(set.size());
    }

    private static void tarString(String inputStr, int len, String str, HashSet<String> set, boolean[] booleans) {
        if(len == str.length()){
            set.add(str);
            return;
        }
        int tempLen = inputStr.length();
        for(int i=0; i<tempLen; i++){
            if(!booleans[i]){
                if((str.length()>=1 && str.charAt(str.length()-1) != inputStr.charAt(i))||(str.length() == 0)){
                    booleans[i] = true;
                    tarString(inputStr, len, str+inputStr.charAt(i), set, booleans);
                    booleans[i] = false;
                }
            }
        }
    }
}
