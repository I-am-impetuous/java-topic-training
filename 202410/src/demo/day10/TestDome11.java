package demo.day10;

import java.util.*;

public class TestDome11 {
    public static int[][] arr = new int[][]{{0,1},{0,-1},{1,0},{-1,0}};
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int col = sc.nextInt();
        int[][] ranks = new int[row][col];
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                ranks[i][j] = sc.nextInt();
            }
        }
        int maxLen = 1;
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                boolean[][] booleans = new boolean[row][col];
                int len = BFS1(ranks, row, col, i, j, booleans);
                if(len > maxLen){
                    maxLen = len;
                }
            }
        }
        System.out.println(maxLen);
    }

    private static int BFS1(int[][] ranks, int row, int col, int i, int j, boolean[][] booleans) {
        int count = 0;
        for(int k=0; k<4; k++){
            int x = arr[k][0]+i;
            int y = arr[k][1]+j;
            if(x>=0&&x<row&&y>=0&&y<col){
                if(Math.abs(ranks[x][y] - ranks[i][j])<=1){
                    if(!booleans[x][y]){
                        booleans[x][y] = true;
                        count += BFS1(ranks, row, col, x, y,  booleans) + 1;
                    }
                }
            }
        }
        return count;
    }
}
