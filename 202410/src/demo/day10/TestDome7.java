package demo.day10;

import java.util.*;

public class TestDome7 {
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
        String input1 = sc.nextLine();
        String input2 = sc.nextLine();
        HashMap<Character, Integer> map = new HashMap<>();
        int count = 0;
        int maxLen = 0;
        int len = input2.length();
        int left = 0;
        for(int right=0; right<len; right++){
            //判断为特殊字符
            while (map.size()!=0 && map.containsKey(input1.charAt(0))
                    && left<=right && map.get(input1.charAt(0))!=0){
                if(map.get(input2.charAt(left))==2){
                    map.put(input2.charAt(left), 1);
                }else if(map.get(input2.charAt(left))==1){
                    map.remove(input2.charAt(left));
                }
                left++;
                count--;
            }
            //判断出现次数
            if(map.containsKey(input2.charAt(right))){
                int n = map.get(input2.charAt(right));
                map.put(input2.charAt(right), n+1);
                count++;
                if(n == 2){
                    char c = input2.charAt(right);
                    while (map.size()!=0 && map.get(c)>2){
                        count--;
                        if(map.get(input2.charAt(left))==1){
                            map.remove(input2.charAt(left));
                        }else if(map.get(input2.charAt(left)) == 2) {
                            map.put(input2.charAt(left), 1);
                        }else if(map.get(input2.charAt(left)) == 3){
                            map.put(input2.charAt(left), 2);
                        }
                        left++;
                    }
                }
                maxLen = Math.max(count, maxLen);
            }else {
                map.put(input2.charAt(right), 1);
                count++;
                if(map.containsKey(input1.charAt(0))){
                    maxLen = Math.max(count-1, maxLen);
                }else {
                    maxLen = Math.max(count, maxLen);
                }
            }

        }
        System.out.println(maxLen);

    }
}
