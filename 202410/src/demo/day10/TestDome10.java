package demo.day10;

import java.util.*;

public class TestDome10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(" ");
        int[] arr = new int[strings.length];
        for(int i=0; i<arr.length; i++){
            arr[i] = Integer.parseInt(strings[i]);
        }
        int time = sc.nextInt();
        if(time<strings.length){
            System.out.println(0);
            return;
        }
        Arrays.sort(arr);
        int len = arr.length;
        int flag = time/len;
        int maxK = arr[(flag+1)*arr.length - time - 1]/flag;
        int count = 0;
        while (true){
            int sum = 0;
            for(int i=0; i<arr.length; i++){
                if(arr[i]%maxK == 0){
                    sum += arr[i]/maxK;
                }else {
                    sum += arr[i]/maxK+1;
                }
            }
            if(sum == time){
                break;
            }else if(sum > time){
                maxK++;
            }else{
                maxK--;
                count++;
                if(count > arr[arr.length-1]){
                    break;
                }

            }
        }
//        if(count > arr[arr.length-1]){
//            System.out.println(maxK);
//            return;
//        }
        while (true){
            int sum = 0;
            if(maxK == 0){
                break;
            }
            for(int i=0; i<arr.length; i++){
                if(arr[i]%maxK == 0){
                    sum += arr[i]/maxK;
                }else {
                    sum += arr[i]/maxK+1;
                }
            }
            if(sum > time){
                break;
            }
            maxK--;
        }
        System.out.println(maxK+1);
    }
}
