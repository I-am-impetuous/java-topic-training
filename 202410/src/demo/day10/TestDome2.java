package demo.day10;

import java.util.*;

public class TestDome2 {
    public static int[][] arrs = new int[][]{{0,1},{1,0},{0,-1},{-1,0}};
    public static List<String> outs = new ArrayList<>();
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        sc.nextLine();
        String[][] inputStr = new String[num][num];
        boolean[][] inputBool = new boolean[num][num];
        for(int i=0; i<num; i++){
            inputStr[i] = sc.nextLine().split(",");
        }
        String handle = sc.nextLine();
        int len = handle.length();
        String out = "";
        int x = 0;
        int y = 0;
        String tempChar = handle.charAt(0) + "";
        for(int j=0; j<num; j++){
            for(int k=0; k<num; k++){
                if(inputStr[j][k].equals(tempChar)){
                    x = j;
                    y = k;
                    inputBool[j][k] = true;
                    out += j+","+k+",";
                    j=num;k=num;
                }
            }
        }
        BFS3(inputStr, inputBool, handle, 1, x, y, out);
        if(outs.size() == 0){
            System.out.println("N");
        }else {
            String temp = outs.get(0);
            System.out.println(temp.substring(0, temp.length()-1));
        }
    }

    private static boolean BFS3(String[][] inputStr, boolean[][] inputBool, String handle, int index, int x, int y, String out) {
        if(handle.length() == index){
            return true;
        }
        String tempChar = handle.charAt(index)+"";
        for(int j=0; j<4; j++){
            int x1 = arrs[j][0] + x;
            int y1 = arrs[j][1] + y;
            int num = inputBool.length;
            if(x1>=0&&x1<num&&y1>=0&&y1<num){
                if(!inputBool[x1][y1] && inputStr[x1][y1].equals(tempChar)){
                    inputBool[x1][y1] = true;
                    if(!BFS3(inputStr, inputBool, handle, index+1, x1, y1, (out+x1+","+y1+","))){
                        inputBool[x1][y1] = false;
                    }else {
                        outs.add(out+x1+","+y1+",");
                        return false;
                    }
                }
            }
        }
        return false;
    }
}
