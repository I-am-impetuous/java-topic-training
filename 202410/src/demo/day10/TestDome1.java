package demo.day10;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        sc.nextLine();
        String[][] inputStr = new String[num][num];
        boolean[][] inputBool = new boolean[num][num];
        for(int i=0; i<num; i++){
            inputStr[i] = sc.nextLine().split(",");
        }
        String handle = sc.nextLine();
        int len = handle.length();
        StringBuffer outStr = new StringBuffer();
        int x=0 ,y=0;
        int[][] arrs = new int[][]{{1,0},{-1,0},{0,1},{0,-1}};
        for(int i=0; i<len; i++){
            String tempChar = handle.charAt(i) + "";
            boolean flag = false;
            if (i == 0) {
                for(int j=0; j<num; j++){
                    for(int k=0; k<num; k++){
                        x = j;
                        y = k;
                        inputBool[j][k] = true;
                        outStr.append(j+","+k+",");
                        flag = true;
                        j=num;k=num;
                    }
                }
            }else {
                for(int j=0; j<4; j++){
                    int x1 = arrs[j][0] + x;
                    int y1 = arrs[j][1] + y;
                    if(x1>=0&&x1<num&&y1>=0&&y1<num){
                        if(!inputBool[x1][y1] && inputStr[x1][y1].equals(tempChar)){
                            x = x1;
                            y = y1;
                            inputBool[x1][y1] = true;
                            outStr.append(x1+","+y1+",");
                            flag = true;
                            break;
                        }
                    }
                }

            }
            if(!flag){
                outStr = new StringBuffer("N ");
                break;
            }
        }
        String out = outStr.toString();
        System.out.println(out.substring(0, out.length()-1));
    }
}
