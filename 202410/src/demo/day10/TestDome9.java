package demo.day10;

import java.util.*;

public class TestDome9 {

    static class Node{
        int val;
        Node left;
        Node right;
        Node(int val){
            this.val = val;
        }
    }

    public static Node buildTree(int preStart, int preEnd, String[] preStr, String[] inStr, int inStart, int inEnd){
        if(preStart > preEnd){
            return null;
        }
        Node root = new Node(Integer.parseInt(preStr[preStart]));
        int index = 0;
        for(int i=inStart; i<=inEnd; i++){
            if(inStr[i].equals(preStr[preStart])){
                index = i;
                break;
            }
        }
        int len = index - inStart;
        root.left = buildTree(preStart+1, preStart+len, preStr, inStr, inStart, index-1);
        root.right = buildTree(preStart+len+1, preEnd, preStr, inStr, index+1, inEnd);
        return root;
    }

    public static void main(String[] args) {
        //构造树
        Scanner sc = new Scanner(System.in);
        String[] inputs1 = sc.nextLine().split(" ");
        String[] inputs2 = sc.nextLine().split(" ");
        Node root = buildTree(0, inputs1.length-1, inputs2, inputs1, 0, inputs2.length-1);
        sumTree(root);
        inorder(root);
    }

    private static void inorder(Node root) {
        if(root == null){
            return;
        }
        inorder(root.left);
        System.out.print(root.val + " ");
        inorder(root.right);
    }

    private static int sumTree(Node root) {
        if(root == null){
            return 0;
        }
        int temp = root.val;
        root.val = sumTree(root.left)+sumTree(root.right);
        return root.val+temp;
    }
}
