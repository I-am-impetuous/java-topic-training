package demo.day10;

import java.util.*;

public class TestDome4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] str1 = sc.nextLine().split(",");

        int row = Integer.parseInt(str1[0]);
        int col = Integer.parseInt(str1[1]);
        String[][] inputStrs = new String[row][col];
        for(int i=0; i<row; i++){
            inputStrs[i] = sc.nextLine().split(",");
        }
        int maxLen = 0;
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                String temp = inputStrs[i][j];
                if(temp.equals("M")){
                    int count = 1;
                    int k = j+1;
                    for(;k<col; k++){
                        if(inputStrs[i][k].equals("M")){
                            count++;
                        }else {
                            break;
                        }
                    }
                    maxLen = Math.max(maxLen, count);

                    count = 1;
                    k = i+1;
                    for(;k<row; k++){
                        if(inputStrs[k][j].equals("M")){
                            count++;
                        }else {
                            break;
                        }
                    }
                    maxLen = Math.max(maxLen, count);

                    count = 1;
                    k = 1;
                    for(;k+i<row&&k+j<col; k++){
                        if(inputStrs[k+i][j+k].equals("M")){
                            count++;
                        }else {
                            break;
                        }
                    }
                    maxLen = Math.max(maxLen, count);

                    count = 1;
                    k = 1;
                    for(;k+i<row&&j-k>=0; k++){
                        if(inputStrs[k+i][j-k].equals("M")){
                            count++;
                        }else {
                            break;
                        }
                    }
                    maxLen = Math.max(maxLen, count);
                }
            }
        }
        System.out.println(maxLen);
    }
}
