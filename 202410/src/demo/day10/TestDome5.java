package demo.day10;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()){
            String[] temp1 = sc.nextLine().split(" ");
            int n = Integer.parseInt(temp1[0]);
            int m = Integer.parseInt(temp1[1]);
            String[] temp2 = sc.nextLine().split(" ");
            int[] arr = new int[n];
            for(int i=0; i<n; i++){
                arr[i] = Integer.parseInt(temp2[i]);
            }
            int sum = 0;
            boolean b = false;
            for(int j=0; j<n; j++){
                sum = 0;
                for(int i=j; i<n; i++){
                    sum += arr[i];
                    if(sum%m==0){
                        System.out.println(1);
                        b = true;
                        break;
                    }
                }
                if(b){
                    break;
                }
            }
            if(!b){
                System.out.println(0);
            }
        }
    }
}
