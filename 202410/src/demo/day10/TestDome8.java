package demo.day10;

import java.util.*;

public class TestDome8 {
    public static void main(String[] args) {
        //1、如果score[i]后两个数据有一个小于零，采用正数
        //如果score[i]后两个数据都小于零，采用较大数据
        //如果score[i]后面两个数据都是正数，采用较大数字
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] arr = new int[num];
        for(int i=0; i<num; i++){
            arr[i] = sc.nextInt();
        }
        int[] dp = new int[num];
        int maxStep = sc.nextInt();
        dp[0] = arr[0];
        for(int i=1; i<arr.length; i++){
            int maxNum = Integer.MIN_VALUE;
            for(int j=1; i-j>=0&&j<=maxStep; j++){
                if(dp[i-j]>maxNum){
                    maxNum = dp[i-j];
                }
            }
            dp[i] = maxNum + arr[i];
        }
        System.out.println(dp[arr.length-1]);
    }
}
