package demo.day10;

import java.util.*;

public class TestDome3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int[] arr = new int[n];
        for(int i=0; i<n; i++){
            arr[i] = sc.nextInt()+sc.nextInt();
        }
        Arrays.sort(arr);
        int count = 1;
        int lastTime = arr[0];
        for(int i=1; i<n; i++){
            if(arr[i] >= lastTime+m){
                count++;
                lastTime = arr[i];
            }
        }
        System.out.println(count);
    }
}
