package demo.day10;

import java.util.*;

public class TestDome6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int target = sc.nextInt();
        sc.nextLine();
        String[] inputStrs = sc.nextLine().split(" ");
        int[] inputs = new int[inputStrs.length];
        for(int i=0; i<inputs.length; i++){
            inputs[i] = Integer.parseInt(inputStrs[i]);
        }
        int[][] dp = new int[inputs.length+1][target+1];
        for(int i=1; i<=inputs.length; i++){
            for(int j=1; j<=target; j++){
                if(j<i){
                    dp[i][j] = dp[i-1][j];
                }else {
                    int last = dp[i-1][j];
                    int now = dp[i-1][j-i] + inputs[i-1];
                    dp[i][j] = Math.max(last, now);
                }
            }
        }
        System.out.println(dp[inputs.length][target]);
    }
}
