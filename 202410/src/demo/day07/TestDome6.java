package demo.day07;

import java.util.*;

public class TestDome6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(",");
        int num = (int)Math.sqrt(strings.length);
        Boolean[][] booleans = new Boolean[num][num];
        int[][] numbers = new int[num][num];
        for(int i=0; i<num; i++){
            for(int j=0; j<num; j++){
                numbers[i][j] = Integer.parseInt(strings[i*num+j]);
            }
        }
        int count = -1;
        int day = -1;
        while (count != 0){
            day++;
            count = 0;
            for(int i=0; i<num; i++){
                Arrays.fill(booleans[i], false);
            }
            for(int i=0; i<num; i++){
                for (int j=0; j<num; j++){
                    if(numbers[i][j] == 0){
                        boolean flag = false;
                        if(i-1>=0){
                            if(numbers[i-1][j]==1 && !booleans[i-1][j]){
                                flag = true;
                            }
                        }
                        if(j-1>=0){
                            if(numbers[i][j-1]==1 && !booleans[i][j-1]){
                                flag = true;
                            }
                        }
                        if(i+1<num){
                            if(numbers[i+1][j]==1 && !booleans[i+1][j]){
                                flag = true;
                            }
                        }
                        if(j+1<num){
                            if(numbers[i][j+1]==1 && !booleans[i][j+1]){
                                flag = true;
                            }
                        }
                        if(flag){
                            numbers[i][j] = 1;
                            booleans[i][j] = true;
                            count++;
                        }
                    }
                }
            }
        }
        if(day == 0){
            day = -1;
        }
        System.out.println(day);
    }
}
