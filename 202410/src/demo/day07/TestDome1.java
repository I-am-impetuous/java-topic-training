package demo.day07;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(";");
        Double[][] doubles = new Double[3][5];
        for(int i=0; i<3; i++){
            String[] temp = strings[i].split(",");
            for(int j=0; j<5; j++){
                doubles[i][j] = Double.parseDouble(temp[j]);
            }
        }
        int[] vars = new int[5];
        String[] temp = strings[3].split(",");
        for(int i=0; i<5; i++){
            vars[i] = Integer.parseInt(temp[i]);
        }
        Double[] results = new Double[3];
        temp = strings[4].split(",");
        for(int i=0; i<3; i++){
            results[i] = Double.parseDouble(temp[i]);
        }
        temp = strings[5].split(",");
        boolean flag = true;
        Double maxDiff = Double.MIN_VALUE;
        for(int i=0; i<3; i++){
            Double sum = 0.0;
            for(int j=0; j<5; j++){
                sum += doubles[i][j]*vars[j];
            }
            if(sum < results[i]){
                if(!temp[i].contains("<")){
                    flag = false;
                }
            }else if(sum == results[i]){
                if(!temp[i].contains("=")){
                    flag = false;
                }
            }else {
                if(!temp[i].contains(">")){
                    flag = false;
                }
            }
            sum = sum - results[i];
            if(sum > maxDiff){
                maxDiff = sum;
            }
        }
        String[] out = Double.toString(maxDiff).split("\\.");
        System.out.println(flag + " " + out[0]);
    }
}
