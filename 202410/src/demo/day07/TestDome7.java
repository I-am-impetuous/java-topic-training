package demo.day07;

import java.util.*;

public class TestDome7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        int len = input.length();
        List<StringBuffer> list = new ArrayList<>();
        StringBuffer out = new StringBuffer();
        boolean flag = false;
        for(int i=0; i<len; i++){
            char c = input.charAt(i);
            if(c == '('){
                flag = true;
                list.add(new StringBuffer());
            }else if(c == ')'){
                flag = false;
                int len2 = list.size();
                if(list.get(len2-1).length() == 0){
                    list.remove(len2-1);
                    continue;
                }
                StringBuffer now = list.get(len2-1);
                for(int j=0; j<len2-1; j++){
                    StringBuffer temp = list.get(j);
                    for(int k=0; k<now.length(); k++){
                        char tempC = now.charAt(k);
                        String up = (tempC+"").toUpperCase();
                        String low = (tempC+"").toLowerCase();
                        if((temp.indexOf(up)!=-1 || temp.indexOf(low)!=-1)){
                            temp.append(now);
                            list.remove(len2-1);
                            break;
                        }
                    }
                    if(len2 != list.size()){
                        break;
                    }
                }
            }else {
                if(flag){
                    int len1 = list.size();
                    StringBuffer temp = list.get(len1-1);
                    list.set(len1-1, temp.append(c));
                }else {
                    out.append(c);
                }
            }
        }

        for(int i=0; i<list.size(); i++){
            StringBuffer buffer = list.get(i);
            char[] chars = buffer.toString().toCharArray();
            Arrays.sort(chars);
            list.set(i, buffer.insert(0, chars[0]));
        }

        String output = "";
        for(int i=0; i<out.length(); i++){
            char c = out.charAt(i);
            boolean f = true;
            for(int j=0; j<list.size(); j++){
                String temp = list.get(j).toString();
                if(temp.contains(c+"")){
                    output += temp.charAt(0);
                    f = false;
                    break;
                }
            }
            if(f){
                output += c;
            }
        }
        System.out.println(output);
    }
}
