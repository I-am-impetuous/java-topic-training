package demo.day07;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        int left = 0;
        int len = str.length();
        int maxLen = 0;
        for(int right=1; right<len; right++){
            if(str.charAt(right) == '1'){
                if(str.charAt(right-1) != '0'){
                    continue;
                }
                int tempLen = 0;
                left = right-1;
                for(; right<len; right++){
                    if((right-left)%2 == 1){
                        if(str.charAt(right) != '1'){
                            tempLen = right-left;
                            break;
                        }
                    }else {
                        if(str.charAt(right) != '0'){
                            tempLen = right-left;
                            break;
                        }
                    }
                }
                if(right == len){
                    if(right - left > maxLen)
                        maxLen = right - left;
                }else {
                    if(str.charAt(right) == '0' && str.charAt(right-1) == '0'){
                        if(tempLen > maxLen){
                            maxLen = tempLen;
                        }
                    }
                }

            }
        }
        for(int i=0; i<maxLen; i++){
            if(i%2==0){
                System.out.print(0);
            }else {
                System.out.print(1);
            }
        }
    }
}
