package demo.day07;

import java.util.*;

public class TestDome4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        long num = sc.nextLong();
        int pos = sc.nextInt();
        long temp = 1;
        int count = 0;
        for(int i=0; i<pos; i++){
            temp *= 26;
            if(temp > num){
                break;
            }
            count++;
        }
        if(count != pos){
            System.out.println(1);
        }else {
            if(temp == num){
                System.out.println(1);
                return;
            }
            while(temp < num){
                temp *= 10;
                count++;
            }
            System.out.println(count-pos);
        }
    }
}
