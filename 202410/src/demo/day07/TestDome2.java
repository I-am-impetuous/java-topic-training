package demo.day07;

import java.util.*;

public class TestDome2 {

    static class Node{
        int val;
        Node left;
        Node right;
        int height;
        Node(int val, Node left, Node right, int height){
            this.val = val;
            this.left = left;
            this.right = right;
            this.height = height;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        sc.nextLine();
        String[] strings = sc.nextLine().split(" ");
        int[] numbers = new int[num];
        for(int i=0; i<num; i++){
            numbers[i] = Integer.parseInt(strings[i]);
        }
        Node root = crateHuffTree(numbers);
        StringBuffer buffer = new StringBuffer();
        inorderVal(root, buffer);
        System.out.println(buffer.toString());
    }

    private static void inorderVal(Node root, StringBuffer buffer) {
        if(root == null){
            return;
        }
        inorderVal(root.left, buffer);
        buffer.append(root.val+" ");
        inorderVal(root.right, buffer);
    }

    private static Node crateHuffTree(int[] numbers) {
        PriorityQueue<Node> queue = new PriorityQueue<>(new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                if(o1.val != o2.val){
                    return o1.val-o2.val;
                }else {
                    return o1.height-o2.height;
                }
            }
        });
        for(int n: numbers){
            Node node = new Node(n, null, null, 1);
            queue.add(node);
        }
        while (queue.size()>1){
            Node node1 = queue.poll();
            Node node2 = queue.poll();
            int height = Math.max(node1.height, node2.height)+1;
            Node node = new Node(node1.val+node2.val, node1, node2, height);
            queue.add(node);
        }
        return queue.poll();
    }
}
