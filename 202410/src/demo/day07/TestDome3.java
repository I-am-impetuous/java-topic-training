package demo.day07;

import java.util.*;

public class TestDome3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String strings[] = sc.nextLine().split(" ");
        Stack<Integer> stack = new Stack<>();
        for(int i=0; i<strings.length; i++){
            int temp = Integer.parseInt(strings[i]);
            int index = -1;
            if(!stack.isEmpty()){
                int len = stack.size();
                int sum = 0;
                for(int j=0; j<len; j++){
                    int n2 = stack.get(len-j-1);
                    sum += n2;
                    if(sum == temp){
                        index = j;
                        break;
                    }
                }
            }
            if(index >= 0){
                while (index-- >= 0){
                    stack.pop();
                }
                stack.push(2*temp);
            }else {
                stack.push(temp);

            }
        }
        while (!stack.isEmpty()){
            System.out.print(stack.pop()+" ");
        }
    }
}
