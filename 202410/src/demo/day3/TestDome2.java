package demo.day3;

import java.util.*;

public class TestDome2 {
    public static void main(String[] args) {
        //1、保存输入数据
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[][] responseTexts = new int[num][2];
        for(int i=0; i<num; i++){
            responseTexts[i][0] = sc.nextInt();
            responseTexts[i][1] = sc.nextInt();
        }
        //2、根据输入的数据进行解析，判断数据的最晚响应时间
        int minTime = Integer.MAX_VALUE;
        for(int i=0; i<num; i++){
            int time = responseTexts[i][1];
            //当最大相应时间小于128
            if(time<128){
                time = time+responseTexts[i][0];
            }else {
                //当最大相应时间大于128
                int exp = (time & 0x70) >> 4;
                int mant = (time & 0x0F);
                time = (mant | 0x10) << (exp+3);
            }
            if(time < minTime){
                minTime = time;
            }
        }
        //3、输出数据
        System.out.println(minTime);
    }
}
