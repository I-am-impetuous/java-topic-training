package demo.day3;

import java.util.*;

public class TestDome3 {
    public static void main(String[] args) {
        //1、保存输入数据
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        int k = sc.nextInt();
        //2、对输入的数据进行设计数据结构hashmap
        HashMap<Character, Integer> map = new HashMap<>();
        int len = str.length();
        for(int i=0; i<len; i++){
            char c = str.charAt(i);
            int count = 1;
            while (i+1<len && str.charAt(i+1)==c){
                count++;
                i++;
            }
            if(map.containsKey(c)){
                if(map.get(c) < count){
                    map.put(c, count);
                }
            }else {
                map.put(c, count);
            }
        }
        //3、使用数组大小为K，按照顺序排序进行选取前K大的数据
        List<Integer> list = new ArrayList<>();
        for(int value: map.values()){
            for(int i=0; i<k; i++){
                if(i>=list.size() || value > list.get(i)){
                    list.add(i, value);
                    break;
                }
            }
        }
        //4、输出
        if(list.size() >= k)
            System.out.println(list.get(k-1));
        else
            System.out.println(-1);
    }
}
