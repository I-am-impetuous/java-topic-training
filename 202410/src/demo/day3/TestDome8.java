package demo.day3;

import java.util.*;

public class TestDome8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] weights = new int[num];
        int sum = 0;
        int count = 0;
        for(int i=0; i<num; i++){
            weights[i] = sc.nextInt();
            count = weights[i] ^ count;
            sum += weights[i];
        }
        if(count != 0){
            System.out.println(-1);
            return;
        }
        Arrays.sort(weights);
        System.out.println(sum - weights[0]);
    }
}
