package demo.day3;

import java.util.*;

public class TestDome6 {
    public static void main(String[] args) {
        //1、保存输入数据
        Scanner sc = new Scanner(System.in);
        int K = Integer.parseInt(sc.nextLine());
        String string = sc.nextLine();
        //2、通过遍历字符串进行判断命令字，并进行保存
        List<String> commands = new ArrayList<>();
        String buffer = "";
        int len = string.length();
        for(int i=0; i<len; i++){
            char c = string.charAt(i);
            if(c == '"' && buffer.contains("\"")){
                buffer += "\"";
                commands.add(buffer);
                buffer = "";
            }else if(c == '_' && !buffer.contains("\"")){
                if(buffer == ""){
                    continue;
                }
                commands.add(buffer);
                buffer = "";
            }else if(i == len-1){
                buffer += c;
                commands.add(buffer);
            }else {
                buffer += c;
            }
        }
        //3、判断是那一个命令字并进行输出
        if(commands.size() <= K){
            System.out.println("ERROR");
        }else {
            commands.set(K, "******");
            StringBuffer temp = new StringBuffer();
            for(int i=0; i<commands.size(); i++){
                temp.append("_");
                temp.append(commands.get(i));
            }
            temp.deleteCharAt(0);
            System.out.println(temp.toString());
        }
    }
}
