package demo.day3;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        //1、保存输入数据
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int maxX = sc.nextInt();
        int[][] trace = new int[num+1][2];
        for(int i=0; i<num; i++){
            trace[i][0] = sc.nextInt();
            trace[i][1] = sc.nextInt();
        }
        trace[num][0] = maxX;
        trace[num][1] = 0;
        //2、其中设置变量wide表示x轴变动的距离
        //hight表示现在的高，并计算面积
        long sum = 0;
        int hight = 0;
        for(int i=1; i<=num; i++){
            int wide = trace[i][0] - trace[i-1][0];
            hight += trace[i-1][1];
            sum += wide*Math.abs(hight);
        }
        //3、输出面积
        System.out.println(sum);
    }
}
