package demo.day3;

import java.util.*;

public class TestDome10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        sc.nextLine();
        String string = sc.nextLine();
        int len = string.length();
        StringBuffer buffer = new StringBuffer();
        StringBuffer firstStr = new StringBuffer();
        boolean flag = false;
        int count = 0;
        for(int i=0; i<len; i++){
            char c = string.charAt(i);
            if(c == '-'){
                flag = true;
            }else{
                if(flag){
                    count++;
                    if((count-1)%k == 0){
                        buffer.append('-');
                    }
                    buffer.append(c);
                }else {
                    firstStr.append(c);
                }
            }
        }
        String[] temps = buffer.toString().split("-");
        StringBuffer out = new StringBuffer();
        out.append(firstStr);
        for(String temp: temps){
            if(temp.length() == 0){
                continue;
            }
            int big = 0;
            int small = 0;
            for(int i=0; i<temp.length(); i++){
                char c = temp.charAt(i);
                if(c>='a' && c<='z'){
                    small++;
                }else if (c>='A' && c<='Z'){
                    big++;
                }
            }
            if(big>small)
                out.append('-').append(temp.toUpperCase());
            else if(small > big)
                out.append('-').append(temp.toLowerCase());
            else
                out.append('-').append(temp);
        }
        System.out.println(out.toString());
    }
}
