package demo.day3;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        //1、保存输入的数据
        Scanner sc = new Scanner(System.in);
        String str1 = sc.nextLine();
        String str2 = sc.nextLine();
        int len1 = str1.length();
        int len2 = str2.length();
        //2、把需要匹配的字符串改变数据结构
        List<List<Character>> lists = new ArrayList<>();
        for(int i=0; i<len2; i++){
            List<Character> list = new ArrayList<>();
            if(str2.charAt(i) == '['){
                while (str2.charAt(++i) != ']'){
                    list.add(str2.charAt(i));
                }
            }else {
                list.add(str2.charAt(i));
            }
            lists.add(list);
        }
        //3、使用滑动窗口进行匹配
        int left1 = 0;
        for(; left1+lists.size()<=len1; left1++){
            int right = left1;
            Boolean flag1 = true;
            for(int left2 = 0; left2<lists.size(); left2++, right++){
                Boolean flag2 = false; //用于判断字符是否被匹配
                for(char b: lists.get(left2)){
                    if(b == str1.charAt(right)){
                        flag2 = true;
                        break;
                    }
                }
                if(!flag2){
                    flag1 = false;
                    break;
                }
            }
            if(flag1){
                System.out.println(left1);
                return;
            }
        }
        //4、返回输出
        System.out.println(-1);
    }
}
