package demo.day3;

import java.util.*;

public class TestDome4 {
    public static void main(String[] args) {
        //1、处理输入
        Scanner sc = new Scanner(System.in);
        String string = sc.nextLine();
        String[] chars = string.split(",");
        int len = chars.length;
        int[] numbers = new int[len];
        for(int i=0; i<len; i++){
            numbers[i] = Integer.parseInt(chars[i]);
        }
        int target = sc.nextInt();
        //2、使用滑动窗口，根据值，判断是否停止
        int count = 0;
        //使用一个整数记录数据
        int maxLength = -1;
        for( int left = 0; left<len; left++){
            int right = left;
            while(right<len && count<target){
                count += numbers[right];
                if(count == target && right-left+1 > maxLength){
                    maxLength = right-left+1;
                }
                right++;
            }
        }
        //3、输出
        System.out.println(maxLength);
    }
}
