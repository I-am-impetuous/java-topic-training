package demo.day3;

import java.util.*;

public class TestDome7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String string = sc.nextLine();
        int len = string.length();
        int sum = 0;
        for(int i=0; i<len; i++){
            int temp = string.charAt(i) - '0';
            if(temp > 4){
                temp--;
            }
            sum = sum*9 + temp;
        }
        System.out.println(sum);
    }
}
