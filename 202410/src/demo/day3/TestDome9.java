package demo.day3;

import java.util.*;

public class TestDome9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String string = sc.nextLine();
        char[] chars = string.toCharArray();
        Arrays.sort(chars);
        int len = string.length();
        for(int i=0; i<=len; i++){
            if(i == len){
                System.out.println(string);
                break;
            }
            char temp = string.charAt(i);
            if(temp == chars[i]){
                continue;
            }
            int index = i+1;
            for(int j=i+1; j<len; j++){
                if(chars[i] == string.charAt(j)){
                    index = j;
                }
            }
            StringBuffer buffer = new StringBuffer(string);
            buffer.setCharAt(i, string.charAt(index));
            buffer.setCharAt(index, temp);
            System.out.println(buffer.toString());
            break;
        }
    }
}
