package demo.day08;

import java.util.*;

public class test {

    static class Point{
        int x;
        int y;
        int val;
        Point(int x, int y, int val){
            this.x = x;
            this.y = y;
            this.val = val;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt(); //行
        int col = sc.nextInt(); //列
        sc.nextLine();
        String[][] ranks = new String[row][col];
        for(int i=0; i<row; i++){
            ranks[i] = sc.nextLine().split(" ");
        }
        List<Point> list = new ArrayList<>();
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                if(!ranks[i][j].equals(".")){
                    list.add(new Point(i, j, Integer.parseInt(ranks[i][j])));
                }
            }
        }

        int[][] steps = new int[row][col];
        int[][] sumPoints = new int[row][col];
        for(Point p: list){
            boolean[][] booleans = new boolean[row][col];
            BFS(p, booleans, sumPoints, steps);
        }
        int minStep = Integer.MAX_VALUE;
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                if(sumPoints[i][j] == 9 && steps[i][j] < minStep){
                    minStep = steps[i][j];
                }
            }
        }
        System.out.println(minStep);


        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                int sum = 0;
                boolean flag = true;
                boolean[][] booleans = new boolean[row][col];
                for(Point p: list){
                    int val = p.val;
//                    BFS(i, j, p, booleans);
                }
                if(flag){
                    minStep = Math.min(minStep, sum);
                }
            }
        }
        if(minStep == Integer.MAX_VALUE){
            System.out.println(0);
        }else {
            System.out.println(minStep);
        }
    }

    private static void BFS(Point p, boolean[][] booleans, int[][] sumPoints, int[][] steps) {
        Queue<Point> queue = new LinkedList<>();
        queue.add(p);
        int[][] list = {{-1, 2}, {1, 2}, {-1, -2}, {1, -2}, {2, 1}, {-2, 1}, {2, -1}, {-2, -1}};
        while (!queue.isEmpty()){
            Point temp = queue.poll();
            if(temp.val == 0){
                continue;
            }
            for(int[] arr: list){
                int x = arr[0]+temp.x;
                int y = arr[1]+temp.y;
                if(x>=0 && x<booleans.length && y>=0 && y<booleans[0].length && !booleans[x][y]){
                    booleans[x][y] = true;
                    steps[x][y] += p.val - temp.val + 1;
                    sumPoints[x][y]++;
                    queue.add(new Point(x, y, temp.val-1));
                }
            }
        }

    }
}
