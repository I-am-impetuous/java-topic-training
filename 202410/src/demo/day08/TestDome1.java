package demo.day08;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        int num1 = 1474560;
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[][] numbers = new int[num][2];
        for(int i=0; i<num; i++){
            numbers[i][0] = sc.nextInt();
            if(numbers[i][0]%512 == 0){
                numbers[i][1] = numbers[i][0]/512;
//                System.out.println(numbers[i][1]);
            }else {
                numbers[i][1] = numbers[i][0]/512+1;
            }
        }
        int blocks = num1/512;
        int[][] dp = new int[num+1][blocks+1];
        for(int i=1; i<=num; i++){
            for(int j=1; j<=blocks; j++){
                if(j < numbers[i-1][1]){
                    dp[i][j] = dp[i-1][j];
                }else {
                    int last = dp[i-1][j];
                    int now = dp[i-1][j-numbers[i-1][1]] + numbers[i-1][0];
                    dp[i][j] = Math.max(last, now);
                }
            }
        }
        System.out.println(dp[num][blocks]);
    }
}
