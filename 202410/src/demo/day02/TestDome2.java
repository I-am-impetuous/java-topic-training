package demo.day02;

import java.util.*;

public class TestDome2 {
    public static void main(String[] args) {
        //1、构建数据关系
        //hashmap记录每一个分销商的收入，分销商ID和收入 -- hashmap为了方便通过id查找收入进行计算
        HashMap<Integer, Integer> income = new HashMap<>();
        //hashset记录每一个分销商的id -- 为了找到boos的id
        HashSet<Integer> ids = new HashSet<>();
        //hashmap记录孩子父亲关系，一个孩子只有一个父亲
        HashMap<Integer, Integer> childParent = new HashMap<>();
        //hashmap记录父亲孩子的关系，一个父亲有多个孩子
        HashMap<Integer, List<Integer>> parentChild = new HashMap<>();
        //2、通过数据构建映射关系树
        Scanner sc = new Scanner(System.in);
        int sum = sc.nextInt();
        for(int i=0; i<sum; i++){
            int childId = sc.nextInt();
            int parentId = sc.nextInt();
            int chlidIncome = sc.nextInt();

            income.put(childId, chlidIncome);
            ids.add(childId);
            ids.add(parentId);
            childParent.put(childId, parentId);
            parentChild.putIfAbsent(parentId, new ArrayList<>());
            parentChild.get(parentId).add(childId);
        }
        //3、对boos的收入进行计算，进行深度优先算法
        for(Integer id: ids){
            if(!childParent.containsKey(id)){
                income.put(id, 0);
                calTalIncome(id, parentChild, income);
                //4、输入数据
                System.out.println(id + " " + income.get(id));
                break;
            }
        }
    }

    //进行深度优先算法
    private static void calTalIncome(Integer parentId,
                                     HashMap<Integer, List<Integer>> parentChild,
                                     HashMap<Integer, Integer> income) {
        List<Integer> childIds = parentChild.get(parentId);
        if(childIds!=null && !childIds.isEmpty()){
            for(int childId: childIds){
                calTalIncome(childId, parentChild, income);
                int parentAddIncome = income.get(childId)/100*15;
                income.put(parentId, income.get(parentId)+parentAddIncome);
            }
        }
    }
}
