package demo.day02;

import java.util.*;

public class TestDome6 {
    public static void main(String[] args) {
        //1、保存输入
        Scanner sc = new Scanner(System.in);
        System.out.println(Long.MAX_VALUE);
        long number = sc.nextLong();
        //2、最快分完，如果+1是4的倍数取出2糖果，否则放回糖果
        int count = 0;
        for(long i=number; i!=1; count++, i/=2){
            if(i == 3){
                count+=2;
                break;
            }
            if(i%2 != 0){
                if((i+1)/2%2 == 0){
                    i+=1;
                }else {
                    i-=1;
                }
                count++;
            }
        }
        //3、输出
        System.out.println(count);
    }
}
