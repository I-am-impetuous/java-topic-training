package demo.day02;

import java.util.*;

public class TestDome8 {
    public static void main(String[] args) {
        //1、处理数据
        Scanner sc = new Scanner(System.in);
        String string1 = sc.nextLine();
        String string2 = sc.nextLine();
        int len2 = string2.length();
        int len1 = string1.length();
        int k = sc.nextInt();
        //2、设置滑动窗口范围left right;
        int[] str1Counts = new int[26];
        for(int i=0; i<len1; i++){
            str1Counts[(string1.charAt(i) - 'a')]++;
        }
        int count = len1;
        //3、字串长度为n1+k；
        if(len1+k > len2){
            System.out.println(-1);
            return;
        }
        int left = 0;
        int right = 0;
        //4、判断是否包含全部字母-进行滑动窗口
        int[] str2Counts = new int[26];
        while (right < len2){
            //移动华东窗口 - 减少最左侧的
            if(right-left-len1-k>0){
                str2Counts[string2.charAt(left) - 'a']--;
                if(str1Counts[string2.charAt(left) - 'a'] - str2Counts[string2.charAt(left) - 'a'] > 0){
                    count++;
                }
                left++;
            }
            //
            str2Counts[string2.charAt(right) - 'a']++;
            if(str1Counts[string2.charAt(right) - 'a'] - str2Counts[string2.charAt(right) - 'a'] >= 0){
                count--;
                if(count <= 0){
                    System.out.println(left);
                    return;
                }
            }
            right++;
        }
        //5、输出
        System.out.println(-1);
    }
}
