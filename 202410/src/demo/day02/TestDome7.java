package demo.day02;

import java.util.*;

public class TestDome7 {
    public static void main(String[] args) {
        //1、输入
        //2、对数据进行贪心算法
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(" ");
        int len = strings.length;
        int[] dp = new int[len];
        dp[0] = Integer.parseInt(strings[0]);
        int maxScore = dp[0];
        int sum = dp[0];
        for(int i=1; i<len; i++){
            int num = Integer.parseInt(strings[i]);
            if(sum+num <= 100){
                dp[i] = dp[i-1] + num - sum;
                maxScore = Math.max(dp[i], maxScore);
                sum += num;
            }else {
                int temp = 100 - sum;
                dp[i] = dp[i-1] + temp - sum;
                maxScore = Math.max(dp[i], maxScore);
                break;
            }
        }
        System.out.println(maxScore);
    }
}
