package demo.day02;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        //1、处理输入：二维数组
        Scanner sc = new Scanner(System.in);
        int itemNumber = sc.nextInt();
        int days = sc.nextInt();
        int[] maxNumber = new int[itemNumber];
        for(int i=0; i<itemNumber; i++){
            maxNumber[i] = sc.nextInt();
        }
        int[][] prices = new int[itemNumber][days];
        for(int i=0; i<itemNumber; i++){
            for(int j=0; j<days; j++){
                prices[i][j] = sc.nextInt();
            }
        }
        //2、使用贪心算法对每一个商品进行获取最大例利润，然后再每一个商品最大利润相加
        int sumProfit = 0;
        for(int i=0; i<itemNumber; i++){
            int maxProfit = 0;
            for(int j=1; j<days; j++){
                maxProfit += Math.max(0, prices[i][j]-prices[i][j-1]);
            }
            sumProfit += maxProfit*maxNumber[i];
        }
        //3、处理输出
        System.out.println(sumProfit);
    }
}
