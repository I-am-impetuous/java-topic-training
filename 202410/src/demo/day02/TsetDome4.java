package demo.day02;

import java.util.*;

public class TsetDome4 {
    public static void main(String[] args) {
        //1、读取数据，保存数据
        Scanner sc = new Scanner(System.in);
        int sumTime = sc.nextInt();
        int numWork = sc.nextInt();
        int[][] works = new int[numWork][2];
        for(int i=0; i<numWork; i++){
            works[i][0] = sc.nextInt();
            works[i][1] = sc.nextInt();
        }
        //2、找到耗时最短的工作
        int minTime = Integer.MAX_VALUE;
        for(int i=0; i<numWork; i++){
            if(minTime > works[i][0]){
                minTime = works[i][0];
            }
        }
        //3、处理背包问题，使用动态规划
        int[][] dp = new int[numWork+1][sumTime+11];
        for(int i=1; i<numWork+1; i++){
            for(int j=minTime; j<sumTime+1; j++){
                int last = dp[i-1][j]; //不做这个工作
                int flag = j-works[i-1][0]; //判断数组是否越界
                int temp = 0; //如果越界temp等于0;
                if(flag>=0){
                    temp = works[i-1][1] + dp[i-1][flag];
                }
                int now =  works[i-1][0]>j? 0 : temp; //做这个工作
                dp[i][j] = Math.max(last, now);
            }
        }
        //4、输出最大工作回报
        System.out.println(dp[numWork][sumTime]);
    }
}
