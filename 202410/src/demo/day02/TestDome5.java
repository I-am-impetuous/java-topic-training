package demo.day02;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        //1、保存输入数据，存储数据
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        String[] guessNumbers = new String[num];
        String[] guessResult = new String[num];
        sc.nextLine();
        for(int i=0; i<num; i++){
            String[] temp = sc.nextLine().split(" ");
            guessNumbers[i] = temp[0];
            guessResult[i] = temp[1];
        }
        //2、筛选0A0B和0A的数据，通过这些数据进行剪枝
        //3、暴力求解
        int sumValue = 0;
        String out = "";
        for(int i=0; i<10000; i++){
            String answerNum = String.format("%04d", i);
            Boolean flag = true;
            int index = 0;
            for(String guessNumber: guessNumbers){
                int x = 0;
                int y = 0;
                int[] answerCount = new int[10];
                int[] guessCount = new int[10];
                for(int j=0; j<4; j++){
                    int answerJ = answerNum.charAt(j) - '0';
                    int guessJ = guessNumber.charAt(j) - '0';
                    if(answerJ == guessJ){
                        x++;
                    }else {
                        answerCount[answerJ]++;
                        guessCount[guessJ]++;
                    }
                }
                for(int k=0; k<10; k++){
                    y += Math.min(answerCount[k], guessCount[k]);
                }
                String temp = x + "A" + y + "B";
                if(!temp.equals(guessResult[index++])){
                    flag = false;
                    break;
                }
            }
            if(flag){
                out = answerNum;
                sumValue++;
                if(sumValue > 1){
                    break;
                }
            }
        }
        //4、判断并输出数据
        if(sumValue != 1){
            out = "NA";
        }
        System.out.println(out);
    }
}
