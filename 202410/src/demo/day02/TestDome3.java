package demo.day02;

import java.util.*;

public class TestDome3 {
    public static void main(String[] args) {
        //1、对输入进行数据保存
        Scanner sc = new Scanner(System.in);
        String[] guessWords = sc.nextLine().split(",");
        String[] answerWords = sc.nextLine().split(",");
        //2、对数据进行拆分，进行去重排序的过程
        List<String> guessList = toSortNoRepeat(guessWords);
        List<String> answerList = toSortNoRepeat(answerWords);
        //3、进行对比谜面上的数据和谜底中的数据
        List<String> outs = new ArrayList<>();
        for(String str: guessList){
            boolean statue = false;
            for(int i=0; i<answerList.size(); i++){
                if(str.equals(answerList.get(i))){
                    statue = true;
                    outs.add(answerWords[i]);
                    break;
                }
            }
            if(!statue){
                outs.add("not found");
            }
        }
        //4、进行结果输出，进行拼接
        String string = outs.get(0);
        for(int i=1; i<outs.size(); i++){
            string = string + "," + outs.get(i);
        }
        System.out.println(string);
    }

    public static List<String> toSortNoRepeat(String[] strings) {
        List<String> list = new ArrayList<>();
        for(int i=0; i<strings.length; i++) {
            HashSet<Character> set = new HashSet<>();
            for (int j = 0; j < strings[i].length(); j++) {
                set.add(strings[i].charAt(j));
            }
            char[] chars = set.toString().toCharArray();
            Arrays.sort(chars);
            String temp = Arrays.toString(chars);
            list.add(temp);
        }
        return list;
    }
}
