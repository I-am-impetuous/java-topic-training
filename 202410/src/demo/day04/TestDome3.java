package demo.day04;

import sun.java2d.pipe.AAShapePipe;

import java.util.*;

public class TestDome3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int talNum = sc.nextInt();
        boolean[] booleans = new boolean[talNum+1];
        int[] dp = new int[talNum+1];
        dp[0] = 0;
        Arrays.fill(booleans, true);
        int noAliveNum = sc.nextInt();
        for(int i=0; i<noAliveNum; i++){
            int index = sc.nextInt();
            booleans[index] = false;
        }
        int K = sc.nextInt();
        int left = 1;
        int right = 1;
        int max = Integer.MIN_VALUE;
        while (right <= talNum && left<=right){
            if(booleans[right]){
                dp[right]=dp[right-1]+1;
            }else {
                if(K!=0){
                    K--;
                    dp[right] = dp[right-1]+1;
                }else {
                    while (left <= right && booleans[left]){
                        left++;
                        dp[right]--;
                    }
                    left++;
                    dp[right] = dp[right-1]+dp[right];
                }
            }
            if(dp[right] > max){
                max = dp[right];
            }
            right++;
        }
        System.out.println(max);
    }
}
