package demo.day04;

import java.util.*;

public class TestDome6 {
    static class Student{
        String name;
        HashMap<String, Integer> map;
        int talNum;

        Student(String name){
            this.name = name;
            this.map = new HashMap<>();
            talNum = 0;
        }

        void addScore(String reject, int score){
            map.put(reject, score);
            this.talNum += score;
        }

        int getScore (String reject){
            return this.map.getOrDefault(reject, this.talNum);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int studentNum = sc.nextInt();
        int rejectNum = sc.nextInt();
        sc.nextLine();
        String[] rejects = sc.nextLine().split(" ");
        List<Student> students = new ArrayList<>();
        for(int i=0; i<studentNum; i++){
            String[] temp = sc.nextLine().split(" ");
            Student student = new Student(temp[0]);
            for(int j=1; j<temp.length; j++){
                student.addScore(rejects[j-1], Integer.parseInt(temp[j]));
            }
            students.add(student);
        }

        String sortReject = sc.nextLine();
        String out = "";
        students.sort((s1, s2)->{
            int score1 = sortReject.equals("") ? s1.talNum : s1.getScore(sortReject);
            int score2 = sortReject.equals("") ? s2.talNum : s2.getScore(sortReject);
            if(score1 != score2){
                return score2 - score1;
            }else {
                return s1.name.compareTo(s2.name);
            }
        });
        for(int i=0; i<studentNum; i++){
            String temp = students.get(i).name;
            out += temp + " ";
        }
        System.out.println(out);
    }
}
