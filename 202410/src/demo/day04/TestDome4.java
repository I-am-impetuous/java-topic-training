package demo.day04;

import java.util.*;

public class TestDome4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        Queue<Integer> queue = new LinkedList<>();
        boolean flag = true;
        int count = 0;
        for(int i=0; i<2*num; i++){
            String str = sc.nextLine();
            if(str.contains("head")){
                if(!queue.isEmpty() && flag){
                    flag = false;
                }
                queue.add(i+1);
            }else if(str.contains("tail")){
                queue.add(i+1);
            }else {
                if(queue.isEmpty()){
                    continue;
                }
                if(!flag){
                    flag = true;
                    count++;
                }
                queue.poll();
            }
        }
        System.out.println(count);
    }
}
