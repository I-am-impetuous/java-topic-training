package demo.day04;

import java.util.*;

public class TestDome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int productNum = sc.nextInt();
        int tolMoney = sc.nextInt();
        int riskNum = sc.nextInt();
        sc.nextLine();
        String[] returns = sc.nextLine().split(" ");
        String[] risks = sc.nextLine().split(" ");
        String[] productMoneys = sc.nextLine().split(" ");

        int maxMoney = 0;
        int[] outs = new int[productNum];
        for(int i=0; i<productNum; i++){
            int risksI = Integer.parseInt(risks[i]);
            int returnI = Integer.parseInt(returns[i]);
            int productMoneyI = Integer.parseInt(productMoneys[i]);
            if(risksI <= riskNum){
                int temp = Math.min(tolMoney, productMoneyI);
                int nowMoney = temp * returnI;
                if(nowMoney > maxMoney){
                    maxMoney = nowMoney;
                    outs = new int[productNum];
                    outs[i] = temp;
                }
            }
            for(int j=i+1; j<productNum; j++){
                int risksJ = Integer.parseInt(risks[j]);
                int returnJ = Integer.parseInt(returns[j]);
                int productMoneyJ = Integer.parseInt(productMoneys[j]);
                if(risksI+risksJ <= riskNum){
                    int temp1, temp2;
                    int nowMoney = 0;
                    if(returnJ < returnI){
                        temp1 = Math.min(tolMoney, productMoneyI);
                        temp2 = Math.min(tolMoney-productMoneyI, productMoneyJ);

                    }else {
                        temp2 = Math.min(tolMoney, productMoneyJ);
                        temp1 = Math.min(tolMoney-productMoneyJ, productMoneyI);
                    }
                    nowMoney = temp1*returnI + temp2*returnJ;
                    if(nowMoney > maxMoney){
                        maxMoney = nowMoney;
                        outs = new int[productNum];
                        outs[i] = temp1;
                        outs[j] = temp2;
                    }
                }
            }
        }
        String out = "";
        for(int i=0; i<productNum; i++){
            out+=outs[i]+" ";
        }
        System.out.println(out);
    }
}
