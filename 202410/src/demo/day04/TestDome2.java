package demo.day04;

import java.util.*;

public class TestDome2 {

    public static int getHeight(TreeNode root){
        if(root == null){
            return 0;
        }
        int leftHeight = getHeight(root.left);
        int rightHeight = getHeight(root.right);
        int midHeight = getHeight(root.mid);
        return Math.max((Math.max(leftHeight, rightHeight)), midHeight)+1;
    }

    public static TreeNode insert(TreeNode root, int val){
        if(root == null){
            return new TreeNode(val);
        }
        if(val < root.val-500){
            root.left = insert(root.left, val);
        }else if(val > root.val+500){
            root.right = insert(root.right, val);
        }else {
            root.mid = insert(root.mid, val);
        }
        return root;
    }

    static class TreeNode{
        int val;
        TreeNode left, mid, right;
        TreeNode(int val){
            this.val = val;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        TreeNode root = null;
        for(int i=0; i<num; i++){
            int temp = sc.nextInt();
            root = insert(root, temp);
        }
        int height = getHeight(root);
        System.out.println(height);
    }
}
