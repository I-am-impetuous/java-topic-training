package demo.day04;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int deviceNum = sc.nextInt();
        int[] devices = new int[deviceNum];
        for(int i=0; i<deviceNum; i++){
            devices[i] = sc.nextInt();
        }
        int deviceMax = sc.nextInt();
        int[][] dp = new int[deviceNum+1][deviceMax+1];
        for(int i=1; i<=deviceNum; i++){
            for(int j=1; j<=deviceMax; j++){
                if(j < devices[i-1]){
                    dp[i][j] = 0;
                }else {
                    int last = dp[i-1][j];
                    int now = devices[i-1] + dp[i][j-devices[i-1]];
                    dp[i][j] = Math.max(last, now);
                }
            }
        }
        System.out.println(dp[deviceNum][deviceMax]);
    }
}
