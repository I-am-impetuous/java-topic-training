package demo.day04;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        //1、处理输入数据
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(" ");
        int[] numbers = new int[strings.length];
        int sum = 0;
        for(int i=0; i<strings.length; i++){
            numbers[i] = Integer.parseInt(strings[i]);
            sum += numbers[i];
        }
        //2、求出输入数据的平均值，计算平均值和128之间的差距
        int diff = 128 - (sum/numbers.length);
        //3、再次遍历一遍数据，diffNum表示还差多少数，为正K++，为负K--；
        double min = Double.MAX_VALUE;
        while (true){
            sum = 0;
            for(int i=0; i<numbers.length; i++){
                if(numbers[i]+diff < 0){
                    sum = sum + 0;
                }else if(numbers[i]+diff > 255){
                    sum = sum + 255;
                }else {
                    sum += (numbers[i]+diff);
                }
            }
            if(Math.abs(((double)sum/numbers.length) - 128) < min){
                min = Math.abs(((double)sum/numbers.length) - 128);
            }else {
                break;
            }
            if(diff<0){
                diff--;
            }else if (diff>0){
                diff++;
            }else {
                break;
            }
        }
        //4、输出数据
        if(diff > 0){
            diff--;
        }
        System.out.println(diff);
    }
}
