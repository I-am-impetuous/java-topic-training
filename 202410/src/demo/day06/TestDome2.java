package demo.day06;

import java.util.*;

public class TestDome2 {

    static class Point{
        int x, y;
        Point(int x, int y){
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int hang = sc.nextInt();
        int lie = sc.nextInt();
        int[][] nums = new int[hang][lie];
        HashMap<Integer, List<Point>> map = new HashMap<>();
        for(int i=0; i<hang; i++){
            for(int j=0; j<lie; j++){
                nums[i][j] = sc.nextInt();
                if(map.containsKey(nums[i][j])) {
                    map.get(nums[i][j]).add(new Point(i, j));
                }else {
                    List<Point> listP = new ArrayList<>();
                    listP.add(new Point(i, j));
                    map.put(nums[i][j], listP);
                }
            }
        }

        int[][] outs = new int[hang][lie];
        for(int i=0; i<hang; i++){
            for(int j=0; j<lie; j++){
                List<Point> list = map.get(nums[i][j]);
                if(list.size() == 1){
                    outs[i][j] = -1;
                }else if(list.size() >= 2){
                    Point p1 = new Point(i, j);
                    int minNum = Integer.MAX_VALUE;
                    int index = -1;
                    for(int k=0; k<list.size(); k++){
                        Point p2 = list.get(k);
                        int tempNum = (p1.x-p2.x)*(p1.x-p2.x) + (p1.y-p2.y)*(p1.y-p2.y);
                        if(minNum > tempNum && tempNum != 0){
                            index = k;
                            minNum = tempNum;
                        }
                    }
                    Point p2 = list.get(index);
                    outs[i][j] = Math.abs(p1.x-p2.x) + Math.abs(p1.y-p2.y);
                }
            }
        }
        String str = "[";
        for(int[] arr: outs){
            str += Arrays.toString(arr)+", ";
        }
        System.out.println(str.substring(0,str.length()-2) + "]");
    }
}
