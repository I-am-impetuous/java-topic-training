package demo.day06;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strs1 = sc.nextLine().split(",");
        String[] strs2 = sc.nextLine().split(",");

        List<int[]> list = new ArrayList<>();
        for(int i=0; i<strs1.length; i++){
            String[] temp = strs1[i].split(":");
            int memory = Integer.parseInt(temp[0]);
            int num = Integer.parseInt(temp[1]);
            list.add(new int[]{memory, num});
        }
        list.sort((s1, s2)->{
            return s1[0] - s2[0];
        });
        String out = "";
        for(String str: strs2){
            int num = Integer.parseInt(str);
            boolean b = false;
            for(int[] n: list){
                if(n[0] >= num && n[1] != 0){
                    n[1]--;
                    out += "true,";
                    b = true;
                    break;
                }
            }
            if(!b){
                out += "false,";
            }
        }
        System.out.println(out.substring(0, out.length()-1));
    }
}
