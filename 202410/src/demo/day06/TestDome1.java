package demo.day06;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.nextLine();
        String str = str1 + str1;
        int len = str1.length();
        int[] dp = new int[str.length()];
        int oNum = 0;
        int left = 0;
        int maxLen = 0;
        for(int right=0; right<dp.length; right++){
            char c = str.charAt(right);
            if(c == 'o'){
                oNum++;
            }
            if(right - left >= len){
                while (left<=right ){
                    char tempC = str.charAt(left);
                    if(tempC == 'o'){
                        oNum--;
                    }
                    left++;
                    if(oNum%2==0){
                        break;
                    }
                }
            }
            if(oNum%2 == 0){
                dp[right] = right - left + 1;
                if(dp[right] > maxLen){
                    maxLen = dp[right];
                }
            }
        }
        System.out.println(maxLen);
    }
}
