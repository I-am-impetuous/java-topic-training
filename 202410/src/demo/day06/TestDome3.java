package demo.day06;

import java.util.*;

public class TestDome3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int height = sc.nextInt();
        int wide = sc.nextInt();
        int targetNum = sc.nextInt();
        int targetValue = sc.nextInt();
        if(wide < targetNum || height < targetNum){
            System.out.println(0);
            return;
        }
        int[][] nums = new int[height][wide];
        for(int i=0; i<height; i++){
            for(int j=0; j<wide; j++){
                nums[i][j] = sc.nextInt();
            }
        }
        int count = 0;
        for(int i=targetNum-1; i<height; i++){
            for(int j=targetNum-1; j<wide; j++){
                if (targetValue <= talTargetValue(nums, i, j, targetNum)){
                    count++;
                }
            }
        }
        System.out.println(count);
    }

    private static int talTargetValue(int[][] nums, int i, int j, int target) {
        int sum = 0;
        for(int x=0; x<target; x++){
            for(int y=0; y<target; y++){
                sum += nums[i-x][j-y];
            }
        }
        return sum;
    }
}
