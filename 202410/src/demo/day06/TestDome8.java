package demo.day06;

import java.util.*;

public class TestDome8 {

    static class KeyValue{
        char key;
        int value;
        KeyValue(char key, int value){
            this.value = value;
            this.key = key;}
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        HashMap<Character, Integer> map = new HashMap<>();
        int len = str.length();
        for(int i=0; i<len; i++){
            char c = str.charAt(i);
            if(map.containsKey(c)){
                map.put(c, map.get(c)+1);
            }else {
                map.put(c, 1);
            }
        }
        List<KeyValue> list = new ArrayList<>();
        for(Map.Entry entry: map.entrySet()){
            list.add(new KeyValue((char)entry.getKey(), (int)entry.getValue()));
        }
        list.sort((s1, s2)->{
            if(s1.value == s2.value){
                return s2.key - s1.key;
            }else {
                return s2.value - s1.value;
            }
        });
        StringBuffer buffer = new StringBuffer();
        for(KeyValue kv: list){
            buffer.append(kv.key+":"+kv.value+";");
        }
        System.out.println(buffer.toString());
    }
}
