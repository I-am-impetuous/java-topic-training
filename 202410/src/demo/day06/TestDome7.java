package demo.day06;

import java.util.*;

public class TestDome7 {
    public static void main(String[] args) {
//        System.out.println(Integer.MAX_VALUE);
//        System.out.println(Math.pow(2, 4) == 16);
        Scanner sc = new Scanner(System.in);
        int pos = sc.nextInt();
        int k = sc.nextInt();
        int m = k;
        if(pos<3 || pos>7){
            System.out.println(-1);
            return;
        }
        int start = (int)Math.pow(10, pos-1);
        int end = (int)Math.pow(10, pos);
        int out = -1;
        for(; start<end; start++){
            if(k<0){
                break;
            }else {
                int sum = 0;
                int temp = start;
                for(int i=0; i<pos; i++){
                    sum+=Math.pow(temp%10, pos);
                    temp = temp/10;
                }
                if(sum == start){
                    out = start;
                    k--;
                }
            }
        }
        if(start==end){
            System.out.println(out*m);
        }else
            System.out.println(out);
    }
}
