package demo.day06;

import java.util.*;

public class TestDome6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[][] numbers = new int[4][3];
        for(int i=0; i<3; i++){
            for(int j=0; j<4; j++){
                numbers[j][i] = sc.nextInt();
            }
        }
        for(int i=0; i<3; i++){
            numbers[2][i] += numbers[0][i];
            numbers[3][i] = numbers[1][i] - numbers[3][i];
        }
        int maxX1 = maxNumber(numbers[0]);
        int minX2 = minNumber(numbers[2]);
        int maxY2 = maxNumber(numbers[3]);
        int minY1 = minNumber(numbers[1]);
        int sum = (minY1-maxY2)*(minX2-maxX1);
        if(sum < 0){
            sum = 0;
        }
        System.out.println(sum);
    }

    private static int minNumber(int[] number) {
        int min = number[0];
        for(int i=1; i<number.length; i++){
            if(min > number[i]){
                min = number[i];
            }
        }
        return min;
    }

    private static int maxNumber(int[] number) {
        int max = number[0];
        for(int i=1; i<number.length; i++){
            if(max < number[i]){
                max = number[i];
            }
        }
        return max;
    }
}
