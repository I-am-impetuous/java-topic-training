package demo.day14;

import java.util.*;

public class TestDome7 {
    public static List<int[]> list = new ArrayList<>();
    public static int count = 0;
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        int M = sc.nextInt();
        int K = sc.nextInt();
        boolean[] booleansN = new boolean[N];
        boolean[] booleansM = new boolean[M];
        int[][] arr = new int[N][M];
        for(int i=0; i<N; i++){
            for(int j=0; j<M; j++){
                arr[i][j] = sc.nextInt();
            }
        }
        int[] list1 = Arrays.copyOf(arr[0], arr[0].length);
        list1[0] = 0;
        arr[0][1] = 0;
        int minNum = Math.min(M, N);
        int[] buffer = new int[minNum];
        for(int i=0; i<N; i++){
            booleansN[0] = true;
            booleansM[i] = true;
            buffer[0] = arr[0][i];
            facMN(buffer, booleansN, booleansM, arr, 1);
            booleansN[0] = false;
            booleansM[i] = false;
        }
        for(int i=0; i<list.size(); i++){
            int[] tempArr = list.get(i);
            Arrays.sort(tempArr);
        }
        list.sort((s1, s2)->{
            return s1[minNum-K]-s2[minNum-K];
        });
        System.out.println(list.get(0)[minNum-K]);
    }

    private static void facMN(int[] buffer, boolean[] booleansN, boolean[] booleansM, int[][] arr, int index) {
        if(buffer.length == index){
            list.add(Arrays.copyOf(buffer, index));
            return;
        }
        if(count >= booleansM.length*booleansN.length*booleansM.length){
            return;
        }
        for(int i=0; i<booleansN.length; i++){
            if(booleansN[i]){
                continue;
            }
            for(int j=0; j<booleansM.length; j++){
                if(booleansM[j]){
                    continue;
                }
                booleansN[i] = true;
                booleansM[j] = true;
                buffer[index] = arr[i][j];
                count++;
                facMN(buffer, booleansN, booleansM, arr, index+1);
                booleansN[i] = false;
                booleansM[j] = false;
            }
        }
    }
}
