package demo.day14;

import java.util.*;

public class TestDome5 {
    public static int[][] pos = new int[][]{{-1,0},{0,-1},{0,1},{1,0}};
    public static List<String> list = new ArrayList<>();
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] arr = new int[num];
        for(int i=0; i<num; i++){
            arr[i] = sc.nextInt();
        }
        int row = sc.nextInt();
        int[][] guessArr = new int[row][row];
        for(int i=0; i<row; i++){
            for(int j=0; j<row; j++){
                guessArr[i][j] = sc.nextInt();
            }
        }
        boolean[][] booleans = new boolean[row][row];
        boolean[] arrBoolean = new boolean[num];
        for(int i=0; i<row&&list.size()!=1; i++){
            for(int j=0; j<row&&list.size()!=1; j++){
                if(guessArr[i][j] == arr[0]){
                    arrBoolean[0] = true;
                    booleans[i][j] = false;
                    dfs9(i, j, guessArr, booleans, arr, arrBoolean, 1, i+" "+j+" ");
                    arrBoolean[0] = true;
                    booleans[i][j] = false;
                }
            }
        }
        if(list.size() == 0){
            System.out.println("error");
        }else {
            System.out.println(list.get(0));
        }
    }

    private static void dfs9(int i, int j, int[][] guessArr, boolean[][] booleans, int[] arr,
                             boolean[] arrBoolean, int len, String str) {
        if(len == arr.length){
            list.add(str);
            return;
        }
        for(int k=0; k<4; k++){
            int x1 = pos[k][0]+i;
            int y1 = pos[k][1]+j;
            if(x1>=0&&x1<guessArr.length&&y1>=0&&y1<guessArr.length&&!booleans[x1][y1]){
                if(guessArr[x1][y1] == arr[len]){
                    booleans[x1][y1] = true;
                    dfs9(x1, y1, guessArr, booleans, arr, arrBoolean, len+1, str+x1+" "+y1+" ");
                    booleans[x1][y1] = false;
                }
            }
        }
    }
}
