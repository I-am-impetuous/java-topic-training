package demo.day14;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(" ");
        int len = strings.length;
        int[][] arr = new int[len][len];
        for(int i=0; i<len; i++){
            arr[0][i] = Integer.parseInt(strings[i]);
        }
        for(int i=1; i<len; i++){
            for(int j=0; j<len; j++){
                arr[i][j] = sc.nextInt();
            }
        }

        List<Integer> list = new ArrayList<>();
        int count = 0;
        for(int i=0; i<len; i++){
            if(!list.contains(i)){
                dfs5(arr, list, len, i);
                count++;
            }
        }
        System.out.println(count);
    }

    private static void dfs5(int[][] arr, List<Integer> list, int len, int index) {
        list.add(index);
        for(int i=index+1; i<len; i++){
            if(arr[index][i] == 1 && !list.contains(i)){
                dfs5(arr, list, len, i);
            }
        }
    }
}
