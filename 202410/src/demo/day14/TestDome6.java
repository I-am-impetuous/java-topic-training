package demo.day14;

import java.util.*;

public class TestDome6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        PriorityQueue<int[]> queue = new PriorityQueue<>((s1, s2)->{
            if(s1[0] == s2[0]){
                return s1[1] - s2[1];
            }
            return s1[0] - s2[0];
        });
        for(int i=0; i<num; i++){
            queue.offer(new int[]{sc.nextInt(), sc.nextInt()});
        }
        int Time = 1;
        int count = 0;
        while (!queue.isEmpty()){
            while (true){
                if(!queue.isEmpty()&&queue.peek()[1] < Time){
                    queue.poll();
                }else {
                    break;
                }
            }
            if(!queue.isEmpty()){
                int[] arr = queue.peek();
                if(Time>=arr[0] && Time<=arr[1]){
                    queue.poll();
                    count++;
                }
                Time++;
            }
        }
        System.out.println(count);
    }
}
