package demo.day14;

import java.util.*;

public class TestDome4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] arr = new int[num];
        int sum = 0;
        for(int i=0; i<num; i++){
            arr[i] = sc.nextInt();
            sum+=arr[i];
        }
        if(sum%2!=0){
            System.out.println(-1);
            return;
        }
        sum /= 2;
        int[][] dp = new int[num+1][sum+1];
//        for(int i=0; i<=num; i++){
//            Arrays.fill(dp, Integer.MAX_VALUE/2);
//        }
        int[][] step = new int[num+1][sum+1];
        for(int i=1; i<=num; i++){
            for(int j=1; j<=sum; j++){
                if(j<arr[i-1]){
                    dp[i][j] = 0;
                }else {
                    int last = dp[i-1][j];
                    int now = dp[i-1][j-arr[i-1]]+arr[i-1];
                    if(last > now){
                        dp[i][j] = last;
                        step[i][j] = step[i-1][j];
                    }else {
                        dp[i][j] = now;
                        step[i][j] = step[i-1][j-arr[i-1]]+1;
                    }
                }
            }
        }
        int minStep = Integer.MAX_VALUE;
        for(int i=1; i<=num; i++){
            if(dp[i][sum] == sum){
                if(minStep > step[i][sum]){
                    minStep = step[i][sum];
                }
            }
        }
        if(minStep == Integer.MAX_VALUE){
            System.out.println(-1);
        }else {
            System.out.println(minStep);
        }
    }
}
