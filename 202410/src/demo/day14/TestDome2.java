package demo.day14;

import java.util.*;

public class TestDome2 {
    static class Node{
        String val;
        List<Node> nextList;
        Node(String val){
            this.val = val;
            this.nextList = new ArrayList<>();
        }
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        sc.nextLine();
        HashMap<String, Node> map = new HashMap<>();
        for(int i=0; i<num; i++){
            String[] strings = sc.nextLine().split(" ");
            creatTree(strings, map);
        }
        String str = sc.nextLine();
        Node root = map.get(str);
        StringBuffer buffer = new StringBuffer();
        inorder1(root, buffer);
        char[] chars = buffer.toString().substring(1).toCharArray();
        Arrays.sort(chars);
        for(int i=0; i<chars.length; i++){
            System.out.println(chars[i]);
        }
    }

    private static void inorder1(Node root, StringBuffer s) {
        if(root.nextList == null){
            return;
        }
        int len = root.nextList.size();
        s.append(root.val);
        for(int i=0; i<len; i++){
            inorder1(root.nextList.get(i), s);
        }
    }

    private static void creatTree(String[] strings, HashMap<String, Node> map) {
        if(map.containsKey(strings[1]) && !map.containsKey(strings[0])){
            Node left = new Node(strings[0]);
            Node root = map.get(strings[1]);
            root.nextList.add(left);
            map.put(strings[0], left);
        }else if(!map.containsKey(strings[1]) && map.containsKey(strings[0])){
            Node root = new Node(strings[1]);
            Node left = map.get(strings[0]);
            root.nextList.add(left);
            map.put(strings[1], root);
        }else if(!map.containsKey(strings[1]) && !map.containsKey(strings[0])){
            Node root = new Node(strings[1]);
            Node left = new Node(strings[0]);
            root.nextList.add(left);
            map.put(strings[1], root);
            map.put(strings[0], left);
        }
    }
}
