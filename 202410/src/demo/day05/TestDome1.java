package demo.day05;

import java.util.*;

public class TestDome1 {

    static class App{
        String name;
        int priority;
        int startTime;
        int endTime;

        App(String name, int priority, int startTime, int endTime){
            this.name = name;
            this.priority = priority;
            this.startTime = startTime;
            this.endTime = endTime;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int appNum = Integer.parseInt(sc.nextLine());
        List<App> apps = new ArrayList<>();
        for(int i=0; i<appNum; i++){
            String[] strings = sc.nextLine().split(" ");
            String[] temp = strings[2].split(":");
            int startTime = Integer.parseInt(temp[0])*60 + Integer.parseInt(temp[1]);
            temp = strings[3].split(":");
            int endTime = Integer.parseInt(temp[0])*60 + Integer.parseInt(temp[1]);
            int priority = Integer.parseInt(strings[1]);
            apps.add(new App(strings[0], priority, startTime, endTime));
        }

        apps.sort((s1, s2)->{
            return s2.priority - s1.priority;
        });

        String[] temp = sc.nextLine().split(":");
        int findTime = Integer.parseInt(temp[0])*60 + Integer.parseInt(temp[1]);
        List<App> list = new ArrayList<>();
        list.add(apps.get(0));
        for(int i=1; i<appNum; i++){
            int listLen = list.size();
            Boolean flag = true;
            for(int j=listLen-1; j>=0; j--){
                App tempApp = list.get(j);
                //有交叉
                if(apps.get(i).endTime > tempApp.startTime && apps.get(i).startTime < tempApp.endTime) {
                    if (apps.get(i).priority <= tempApp.priority) {
                        flag = false;
                        break;
                    }else {
                        continue;
                    }
                }
            }
            if(flag){
                list.add(apps.get(i));
            }
        }
        String out = "NA";
        for(App app : list){
            if(findTime >= app.startTime && findTime < app.endTime){
                out = app.name;
                break;
            }
        }
        System.out.println(out);
    }
}
