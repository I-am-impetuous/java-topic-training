package demo.day05;

import java.util.*;

public class TestDome10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String string = sc.nextLine();
        for(int i=0; i<string.length(); i++){
            char c = string.charAt(i);
            if((c<'0' || c>'9') && c !=' '){
                System.out.println("[]");
                return;
            }
        }
        String[] strings = string.split(" ");
        int[] numbers = new int[strings.length];
        for(int i=0; i<strings.length; i++){
            numbers[i] = Integer.parseInt(strings[i]);
        }
        int left = 0;
        int right = 1;
        while (right<strings.length){
            if(numbers[right] != numbers[left] && (numbers[left]>numbers[right] != (left%2 == 0))){
                int temp = numbers[left];
                numbers[left] = numbers[right];
                numbers[right] = temp;
            }
            left++;
            right++;
        }
        String out = "";
        for(int n: numbers){
            out += n+" ";
        }
        System.out.println(out);
    }
}
