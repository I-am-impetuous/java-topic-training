package demo.day05;

import java.util.*;

public class TestDome7 {

    public static int fac(int num){
        int sum = 1;
        for(int i=1; i<=num; i++){
            sum *= i;
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int K = sc.nextInt();
        int index = sc.nextInt();
        List<Integer> list = new LinkedList<>();
        for(int i=0; i<K; i++){
            list.add(i+1);
        }
        String str = "";
        for(int i=0; i<K; i++){
            int temp = (index-1)/fac(K-i-1);
            index = index - temp*fac(K-i-1);
            str += list.get(temp);
            list.remove(temp);
        }
        System.out.println(str);
    }
}
