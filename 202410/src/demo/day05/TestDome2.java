package demo.day05;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TestDome2 {
    static class App{
        String name;
        int priority;
        int startTime;
        int endTime;

        App(String name, int priority, int startTime, int endTime){
            this.name = name;
            this.priority = priority;
            this.startTime = startTime;
            this.endTime = endTime;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int appNum = Integer.parseInt(sc.nextLine());
        TestDome1.App[] apps = new TestDome1.App[appNum];
        for(int i=0; i<appNum; i++){
            String[] strings = sc.nextLine().split(" ");
            String[] temp = strings[2].split(":");
            int startTime = Integer.parseInt(temp[0])*60 + Integer.parseInt(temp[1]);
            temp = strings[3].split(":");
            int endTime = Integer.parseInt(temp[0])*60 + Integer.parseInt(temp[1]);
            int priority = Integer.parseInt(strings[1]);
            apps[i] = new TestDome1.App(strings[0], priority, startTime, endTime);
        }

        String[] temp = sc.nextLine().split(":");
        int findTime = Integer.parseInt(temp[0])*60 + Integer.parseInt(temp[1]);
        int nowPriority = -1;
        String out = "NA";
        for(int i=0; i<appNum; i++){
            if(findTime >= apps[i].startTime && findTime < apps[i].endTime){
                if(nowPriority < apps[i].priority){
                    nowPriority = apps[i].priority;
                    out = apps[i].name;
                }
            }
        }

        System.out.println(out);
    }
}
