package demo.day05;

import java.util.*;

public class TestDome12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(" ");
        int[] numbers = new int[strings.length];
        int[] dp = new int[strings.length];
        Arrays.fill(dp, -1);
        dp[0] = 0;
        for(int i=0; i<strings.length; i++){
            numbers[i] = Integer.parseInt(strings[i]);
        }
        for(int i=1; i<strings.length/2; i++){
            dp[i] = 1;
        }
        for(int i=1; i<strings.length; i++){
            if(i + numbers[i] < strings.length){
                if(dp[i] != -1){
                    if(dp[i+numbers[i]] != -1){
                        dp[i+numbers[i]] = Math.min(dp[i]+1, dp[i+numbers[i]]);
                    }else {
                        dp[i+numbers[i]] = dp[i]+1;
                    }
                }
            }
        }
        System.out.println(dp[strings.length-1]);
    }
}
