package demo.day05;

import java.util.*;

public class TestDome3 {

    static class Point{
        int x;
        int y;

        Point(int x, int y){
            this.x = x;
            this.y = y;
        }

        boolean equals(Point p){
            return (this.x == p.x && this.y == p.y);
        }
    }

    static boolean pointExists(Point[] points, Point point){
        for(Point p: points){
            if(p.equals(point)){
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int pointNum = sc.nextInt();
        Point[] points = new Point[pointNum];
        for(int i=0; i<pointNum; i++){
            int x = sc.nextInt();
            int y = sc.nextInt();
            points[i] = new Point(x, y);
        }
        int count = 0;
        if(pointNum >= 4){
            for(int i=0; i<pointNum; i++){
                Point p1 = points[i];
                for(int j=i+1; j<pointNum; j++){
                    Point p2 = points[j];
                    Point p3 = new Point((p1.x+p1.y-p2.y), (p1.y-(p1.x-p2.x)));
                    Point p4 = new Point((p2.x+p1.y-p2.y), (p2.y-(p1.x-p2.x)));
                    if(pointExists(points, p3) && pointExists(points, p4)){
                        count++;
                    }
                    Point p5 = new Point((p1.x-(p1.y-p2.y)), (p1.y+(p1.x-p2.x)));
                    Point p6 = new Point((p2.x-(p1.y-p2.y)), (p2.y+(p1.x-p2.x)));
                    if(pointExists(points, p5) && pointExists(points, p6)){
                        count++;
                    }
                }
            }
        }
        System.out.println(count/4);
    }

}
