package demo.day05;

import java.util.*;

public class TestDome14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arrs1 = new int[n];
        for(int i=0; i<n; i++){
            arrs1[i] = sc.nextInt();
        }
        int m = sc.nextInt();
        int[] arrs2 = new int[m];
        for(int i=0; i<m; i++){
            arrs2[i] = sc.nextInt();
        }
        int K = sc.nextInt();
        List<Integer> list = new ArrayList<>();
        for(int arr1: arrs1){
            for (int arr2: arrs2){
                list.add(arr2 + arr1);
            }
        }
        Collections.sort(list);

        int sum = 0;
        for(int i=0; i<K; i++){
            sum += list.get(i);
        }
        System.out.println(sum);
    }
}
