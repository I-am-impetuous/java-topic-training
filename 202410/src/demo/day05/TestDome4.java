package demo.day05;

import java.util.*;

public class TestDome4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int index = sc.nextInt();
        int wordNum = sc.nextInt();
        sc.nextLine();
        List<String> list = new ArrayList<>();
        for(int i=0; i<wordNum; i++){
            list.add(sc.nextLine());
        }
        String indexWord = list.get(index);
        char indexChar = indexWord.charAt(indexWord.length()-1);
        list.sort((s1, s2)->{
            if(s1.length() == s2.length()){
                return s1.compareTo(s2);
            }else {
                return s2.length() - s1.length();
            }
        });
        Boolean[] booleans = new Boolean[wordNum];
        Arrays.fill(booleans, true);
        int count = 1;
        index = list.indexOf(indexWord);
        booleans[index] = false;
        String out = indexWord;
        while (count != wordNum){
            boolean flag = false;
            for(int i=0; i<wordNum; i++){
                String temp = list.get(i);
                if(booleans[i] && indexChar == temp.charAt(0)){
                    indexChar = temp.charAt(temp.length()-1);
                    out += temp;
                    count++;
                    booleans[i] = false;
                    flag = true;
                    break;
                }
            }
            if(!flag){
                break;
            }
        }
        System.out.println(out);
    }
}
