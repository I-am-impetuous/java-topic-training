package demo.day05;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String string = sc.nextLine();
        int len = string.length();
        String[] strings = string.substring(1, len-1).split(",");
        int[] numbers = new int[strings.length];
        List<Integer> list = new ArrayList<>();
        for(int i=0; i<numbers.length; i++){
            numbers[i] = Integer.parseInt(strings[i]);
            list.add(numbers[i]);
        }
        Arrays.sort(numbers);
        int target = Integer.parseInt(sc.nextLine());
        int left = 0;
        int right = numbers.length-1;
        int[] outs = new int[2];
        int sum = Integer.MAX_VALUE;
        while (left < right){
            if(numbers[left] + numbers[right] == target){
                int num1 = list.indexOf(numbers[left]);
                int num2 = list.indexOf(numbers[right]);
                if(num1+num2 < sum){
                    sum = num2 + num1;
                    outs[0] = num1;
                    outs[1] = num2;
                }
                left++;
            }else if((numbers[left] + numbers[right]) >= target){
                right--;
            }else {
                left++;
            }
        }

        if(outs[0] < outs[1]){
            System.out.println("[" + list.get(outs[0]) + "," + list.get(outs[1]) + "]");
        }else {
            System.out.println("[" + list.get(outs[1]) + "," + list.get(outs[0]) + "]");
        }
    }
}
