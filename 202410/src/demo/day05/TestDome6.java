package demo.day05;

import java.util.*;

public class TestDome6 {

    public static List<List> lists = new ArrayList<>();
    public static Boolean[] booleans;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int K = sc.nextInt();
        int index = sc.nextInt();

        booleans = new Boolean[K];
        Arrays.fill(booleans, false);

        int[] numbers = new int[K];
        for(int i=0; i<K; i++){
            numbers[i] = i+1;
        }

        List<Integer> list = new ArrayList<>();

        getList(numbers, 0, list);

        String temp = "";
        for(int i=0; i<lists.get(index-1).size(); i++){
            temp += lists.get(index-1).get(i);
        }
        System.out.println(temp);

    }

    private static void getList(int[] numbers, int length, List<Integer> list) {
        if(length == numbers.length){
            lists.add(new ArrayList(list));
            return;
        }
        for(int i=0; i<numbers.length; i++){
            if(!booleans[i]){
                list.add(numbers[i]);
                booleans[i] = true;
                getList(numbers, length+1, list);
                booleans[i] = false;
                list.remove(list.size()-1);
            }
        }
        return;
    }
}
