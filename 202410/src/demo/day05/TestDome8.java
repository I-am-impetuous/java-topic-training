package demo.day05;

import java.util.*;

public class TestDome8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(" ");
        int count = 0;
        int personNum = strings.length;
        for(int i=0; i<personNum; i++){
            count += Integer.parseInt(strings[i]);
        }
        int i = 1;
        int[] results = new int[personNum];
        while (count > 0){
            if(i%7 == 0 || (""+i).contains("7")){
                count--;
                int temp = (i-1)%personNum;
                results[temp]++;
            }
            i++;
        }
        String str = "";
        for(int n: results){
            str += n + " ";
        }
        System.out.println(str);
    }
}
