package demo.day05;

import java.util.*;

public class TestDome13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int K = sc.nextInt();
        int hang = sc.nextInt();
        sc.nextLine();
        List<String> listString = new ArrayList<>();
        for(int i=0; i<hang; i++){
            String string = sc.nextLine();
            listString.add(string);
        }
        StringBuffer buffer = new StringBuffer();
        int count = 0;
        Boolean[] booleans = new Boolean[hang];
        Arrays.fill(booleans, true);
        int i=0;
        while (count!=hang){
            i++;
            for(int j=0; j<hang; j++){
                String[] temp = listString.get(j).split(",");
                for(int k=0; k<K; k++){
                    if((i-1)*K+k < temp.length){
                        buffer.append(temp[(i-1)*K+k] + ",");
                    }else {
                        if(booleans[j]){
                            count++;
                            booleans[j] = false;
                        }
                        break;
                    }
                }
            }
        }
        String str = buffer.toString();
        System.out.println(str.substring(0, str.length()-1));
    }
}
