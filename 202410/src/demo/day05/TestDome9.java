package demo.day05;

import java.util.*;

public class TestDome9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.nextLine();
        String str2 = sc.nextLine();
        String[] strings = str1.split("[^a-zA-Z]");
        Set<String> set = new TreeSet<>();
        for(int i=0; i<strings.length; i++){
            set.add(strings[i]);
        }
        String str = "";
        for(String s: set){
            if(s.startsWith(str2)){
                str += s + " ";
            }
        }
        if(str == ""){
            System.out.println(str2);
        }else {
            System.out.println(str);
        }
    }
}
