package demo.day05;

import java.util.*;

public class TestDome11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int workerNum = sc.nextInt();
        sc.nextLine();
        Boolean[] booleans = new Boolean[workerNum];
        Arrays.fill(booleans, true);
        for(int i=0; i<workerNum; i++){
            String string = sc.nextLine();
            String[] strings = string.split(" ");
            int[] info = new int[3];  //总信息
            int count = 0;
            int left = 0;
            for(int right=0; right<strings.length; right++){
                if(right - left >= 7){
                    if(strings[left].equals("present")){
                        count--;
                        left++;
                    }
                }
                if(strings[right].equals("absent")){
                    info[0]++;
                    count++;
                    info[1] = 0;
                }else if(strings[right].equals("late")||strings[right].equals("leaveearly")){
                    count++;
                    info[1]++;
                }else {
                    info[1] = 0;
                }
                if(info[0] > 1 || info[1] > 2 || count > 3){
                    booleans[i] = false;
                    break;
                }
            }
        }
        String str = "";
        for(boolean b: booleans){
            if(b){
                str += "true ";
            }else {
                str += "false ";
            }
        }
        System.out.println(str);
    }
}
