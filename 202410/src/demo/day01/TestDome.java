package demo.day01;

import java.util.*;

public class TestDome {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int sum = sc.nextInt();
        int num = sc.nextInt();

        //2、使用数组记住每一个发动机的启动时间
        int[] endEngineTimes = new int[sum];
        Arrays.fill(endEngineTimes, 1001);

        //1、输入：使用queue来记录手动启动发动机的位置和时刻
        Queue<int []> queue = new LinkedList<>();
        for(int i=0; i<num; i++){
            int time = sc.nextInt();
            int pos = sc.nextInt();
            endEngineTimes[pos] = time;
            queue.offer(new int[]{time, pos});
        }

        //3、每一处手动启动的发动机单独计算
        while(!queue.isEmpty()){
            int[] temp = queue.poll();
            int time = temp[0];
            int pos = temp[1];

            int[] neighbors = new int[]{(pos+1)%sum, (pos-1+sum)%sum};
            for(int p: neighbors){
                if(endEngineTimes[p] > time+1){
                    endEngineTimes[p] = time+1;
                    queue.offer(new int[]{time+1, p});
                }
            }
        }

        //4、找到最大时刻的发动机位置
        int maxTime = -1;
        List<Integer> list = new LinkedList<>();
        for(int i=0; i<sum; i++){
            if(maxTime < endEngineTimes[i]){
                maxTime = endEngineTimes[i];
                list.clear();
                list.add(i);
            }else if(maxTime == endEngineTimes[i]){
                list.add(i);
            }
        }

        //5、输出发动机个数和发动机位置
        System.out.println(list.size());
        Collections.sort(list);
        for(int l: list){
            System.out.print(l+" ");
        }
    }
}
