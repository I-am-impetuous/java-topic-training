package demo.day01;

import java.util.*;

public class TestDome3 {
    public static void main(String[] args) {
        //1、处理输入，字符串
        Scanner sc = new Scanner(System.in);
        String string = sc.nextLine();
        //2、其中使用数组长度表示有几只大燕，数组中的数值表示大燕在第几个字符上
        List<Integer> list = new ArrayList<>();
        //3、使用另一个数组表示当前大燕的状态，是否为空闲期，true表示运行，false表示空闲
        List<Boolean> status = new ArrayList<>();
        //便利字符串，判断当前字符在quack的第几位；根据这个第几位然后判断是哪一个大燕的
        for(int i=0; i<string.length(); i++){
            char c = string.charAt(i);
            int index = "quack".indexOf(c);
            if(index == -1){
                continue;
            }
            //是否需要多加一只大雁
            if(index == 0){
                //判断是否有空闲大雁
                int statusIndex = status.indexOf(false);
                //没有空闲大燕
                if(statusIndex == -1){
                    list.add(0);
                    status.add(true);
                    continue;
                }else {
                    //有空闲大燕
                    status.set(statusIndex, true);
                    list.set(statusIndex, list.get(statusIndex)+1);
                }
            }else {
                for(int j=0; j<list.size(); j++){
                    if(index == (list.get(j)+1)%5){
                        list.set(j, list.get(j)+1);
                        if(list.get(j)%5==4){
                            //空闲大燕
                            status.set(j, false);
                        }
                    }
                }
            }
        }
        //4、其中数组中的数字大于等于5表示有一个完成的大燕
        int sum = 0;
        for(Integer l: list){
            if(l>=4){
                sum++;
            }
        }
        if(sum==0){
            sum = -1;
        }
        //5、输出
        System.out.println(sum);
    }
}
