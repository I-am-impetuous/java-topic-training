package demo.day01;

import java.util.*;

public class TestDome2 {

    public static int strToInt(String s){
        switch (s){
            case "3": return 3;
            case "4": return 4;
            case "5": return 5;
            case "6": return 6;
            case "7": return 7;
            case "8": return 8;
            case "9": return 9;
            case "10": return 10;
            case "J": return 11;
            case "Q": return 12;
            case "K": return 13;
            case "A": return 14;
            case "2": return 16;
            default: return 0;
        }
    }

    public static void main(String[] args) {
        //1、输入：进行数据保存和数据转换
        Scanner sc = new Scanner(System.in);
        String[] strings = sc.nextLine().split(" ");
        int len = strings.length;
        int[] number = new int[len];
        Boolean[] booleans = new Boolean[len];
        Arrays.fill(booleans, false);
        for(int i=0; i<len; i++){
            number[i] = strToInt(strings[i]);
        }
        //2、进行数据排序
        Arrays.sort(number);
        //3、滑动窗口进行判断是否为顺子
        List<List<Integer>> lists = new ArrayList<>(); //保存合格的顺子
        for(int i=0; i<len; i++){
            if(booleans[i]) continue;
            List<Integer> list = new LinkedList<>(); //判断顺子是否合格
            list.add(number[i]);
            booleans[i] = true;
            for(int j=i+1; j<len; j++){
                if(booleans[j]) continue;
                if(number[j]-list.get(list.size()-1)==1){
                    list.add(number[j]);
                    booleans[j] = true;
                }else if(number[j]-list.get(list.size()-1)>1){
                    break;
                }
            }
            if(list.size()>=5){
                lists.add(list);
            }
        }
        //4、进行输出标准化
        if(lists.size() == 0){
            System.out.println("No");
            return;
        }
        for(int i=0; i<lists.size(); i++){
            List<Integer>list = lists.get(i);
            for(int j=0; j<list.size(); j++){
                if(list.get(j)==11){
                    System.out.print("J ");
                }else if(list.get(j)==12){
                    System.out.print("Q ");
                }else if(list.get(j)==13){
                    System.out.print("K ");
                }else if(list.get(j)==14){
                    System.out.print("A ");
                }else {
                    System.out.print(list.get(j)+" ");
                }
            }
            System.out.println();
        }
    }
}

