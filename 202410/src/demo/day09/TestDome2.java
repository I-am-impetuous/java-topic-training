package demo.day09;

import java.util.*;

public class TestDome2 {

    public static Map<String, Node> tolNode = new HashMap<>();

    static class Node{
        String path;
        Map<String, Node> nexts;
        Node(String path, Map<String, Node> nexts){
            this.path = path;
            this.nexts = nexts;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Node root = new Node("/", null);
        tolNode.put(root.path, root);
        while (sc.hasNextLine()){
            String[] strs = sc.nextLine().split(" ");
            if(strs[0].equals("mkdir") && strs.length == 2 && (root.nexts==null || !root.nexts.containsKey(strs[1]))){
                Node node = new Node(root.path+strs[1]+"/", null);
                if(root.nexts == null){
                    root.nexts = new HashMap<>();
                }
                root.nexts.put(strs[1], node);
            }else if(strs[0].equals("cd") && strs.length == 2){
                if(root.nexts != null && root.nexts.containsKey(strs[1])){
                    root = root.nexts.get(strs[1]);
                    tolNode.put(root.path, root);
                }else if("..".equals(strs[1])){
                    String temp = root.path;
                    int index = temp.substring(0, temp.length()-1).lastIndexOf('/');
                    if(!temp.equals("/")){
                        root = tolNode.get(root.path.substring(0, index+1));
                    }
                }
            }else if(strs[0].equals("pwd")){
                System.out.println(root.path);
            }
        }
    }
}
