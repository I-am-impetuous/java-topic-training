package demo.day09;

import java.util.*;

public class TestDome01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int posNum = sc.nextInt();
        sc.nextLine();
        String str = sc.nextLine();
        String[] strings = str.substring(1, str.length()-1).split(", ");
        boolean[] booleans = new boolean[posNum+1];
        booleans[booleans.length-1] = true;
        int count = 0;
        int endPer = -1;
        for(int i=0; i<strings.length; i++){
            int index = Integer.parseInt(strings[i]);
            if (count == 0 && index == 1){
                count++;
                booleans[0] = true;
                endPer = 0;
                continue;
            }
            if(index == 1){
                if(count >= posNum){
                    endPer = -1;
                    break;
                }
                count++;
                int left = 0;
                int tempIndex = 0;
                int maxLen = -1;
                for(int right = 0; right<booleans.length; right++){
                    if(booleans[right]){
                        if(right-left > maxLen+1 || (maxLen%2==0 && right-left > maxLen) ||
                                (right-left > maxLen && right==booleans.length-1)){
                            maxLen = right-left;
                            tempIndex = left;
                        }
                        left = right+1;
                    }
                }
                if(tempIndex == 0){
                    booleans[tempIndex] = true;
                }else if(tempIndex + maxLen == booleans.length-1){
                    tempIndex += maxLen-1;
                    booleans[tempIndex] = true;
                }else {
                    tempIndex = (tempIndex+maxLen+tempIndex-1)/2;
                    booleans[tempIndex] = true;
                }
                endPer = tempIndex;
            }else {
                count--;
                booleans[-index] = false;
            }
        }
        System.out.println(endPer);
    }
}
