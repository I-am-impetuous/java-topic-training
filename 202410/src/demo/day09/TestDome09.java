package demo.day09;

import java.util.*;

public class TestDome09 {

    public static String[] strings = new String[]{"adc", "def", "ghi", "jkl", "mno", "pqr", "st", "uv", "wx", "yz"};

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input1 = sc.nextLine();
        String input2 = sc.nextLine();
        List<String> list = new ArrayList<>();
        dfs(input1, input2, list, 0, new String(""));
        for(String str : list){
            System.out.print(str + ",");
        }
    }

    private static void dfs(String input1, String input2, List<String> list, int length, String temp) {
        if(length == input1.length()){
            return;
        }
        char c1 = input1.charAt(length);
        int index = Integer.parseInt((c1+""));
        for(int i=0; i<strings[index].length(); i++){
            char c2 = strings[index].charAt(i);
            dfs(input1, input2, list, length+1, temp+c2);
            String out = temp + c2;
            int count = 0;
            for(int j=0; j<out.length(); j++){
                if(input2.contains(out.charAt(j)+"")){
                    count++;
                }
            }
            if(count!=input2.length() && out.length()==input1.length()){
                list.add(out);
            }
        }
    }
}
