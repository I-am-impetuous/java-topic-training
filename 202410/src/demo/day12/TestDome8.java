package demo.day12;

import java.util.*;

public class TestDome8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] inputs = sc.nextLine().split(" ");
        List<String> list = new ArrayList<>();
        for(String str: inputs){
            list.add(str);
        }
        list.sort((anotherString, value)->{
            int len1 = value.length();
            int len2 = anotherString.length();
            int lim = Math.min(len1, len2);
            char[] v1 = value.toCharArray();
            char[] v2 = anotherString.toCharArray();

            int k = 0;
            while (k < lim) {
                char c1 = v1[k];
                char c2 = v2[k];
                if (c1 != c2) {
                    return c1 - c2;
                }
                k++;
            }
            if(len1>lim){
                while (k<len1){
                    char c1 = v1[k];
                    char c2 = v2[lim-1];
                    if(c1 > c2){
                        return c1-c2;
                    }
                    k++;
                }
            }else if(len2>lim) {
                while (k < len2) {
                    char c1 = v2[k];
                    char c2 = v1[lim - 1];
                    if (c1 > c2) {
                        return c2 - c1;
                    }
                    k++;
                }
            }
            return len2 - len1;
        });
        String out = "";
        for(String str: list){
            out+=str;
        }
        for(int i=0;i<out.length();){
            if(out.charAt(i) == '0' && i+1<out.length()){
                out = out.substring(1, out.length());
            }else {
                break;
            }
        }
        System.out.println(out);
    }
}
