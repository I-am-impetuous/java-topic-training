package demo.day12;

import java.text.DateFormatSymbols;
import java.util.*;

public class TestDome3 {
    public static int[][] pos = new int[][]{{0,1},{-1,0},{1,0},{0,-1}};
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int col = sc.nextInt();
        int k = sc.nextInt();
        int[][] arrMap = new int[row][col];
        for(int i=0; i<row; i++){
            for (int j=0; j<col; j++){
                arrMap[i][j] = sc.nextInt();
            }
        }
        int[][] dp = new int[row][col];
        boolean[][] isUse = new boolean[row][col];
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(arrMap[0][0], 0);
        dp[0][0] = 0;
        DFS(arrMap, dp, isUse, map, 0, 0, row, col, k);
        int maxHeight = 0;
        int minStep = 0;
        for(Map.Entry entry: map.entrySet()){
            if(maxHeight < (int)entry.getKey()){
                maxHeight = (int)entry.getKey();
                minStep = (int) entry.getValue();
            }
        }
        System.out.println(maxHeight+" "+minStep);
    }

    private static void DFS(int[][] arrMap, int[][] dp, boolean[][] isUse, HashMap<Integer, Integer> map,
                            int x, int y, int row, int col, int k) {
        int height = arrMap[x][y];
        for(int i=0; i<4; i++){
            int x1 = pos[i][0]+x;
            int y1 = pos[i][1]+y;
            if(x1>=0&&x1<row&&y1>=0&&y1<col&&!isUse[x1][y1]){
                int nowHeight = arrMap[x1][y1];
                if(Math.abs(height-nowHeight)>k){
                    continue;
                }
                isUse[x1][y1] = true;
                dp[x1][y1] = dp[x][y]+1;
                if(!map.containsKey(nowHeight)){
                    map.put(nowHeight, dp[x1][y1]);
                }else {
                    int step = map.get(nowHeight);
                    map.put(nowHeight, Math.min(step, dp[x1][y1]));
                }
                DFS(arrMap, dp, isUse, map, x1, y1, row, col, k);
                isUse[x1][y1] = false;
            }
        }
    }
}
