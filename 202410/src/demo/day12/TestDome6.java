package demo.day12;

import java.util.*;

public class TestDome6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        int num = sc.nextInt();
        boolean[] booleans = new boolean[4095];
        String[] strings = input.split(",");
        for(String str: strings){
            String[] oranges = str.split("-");
            if(oranges.length != 1){
                int left = Integer.parseInt(oranges[0]);
                int right = Integer.parseInt(oranges[1]);
                for(int i=left; i<=right; i++){
                    booleans[i] = true;
                }
            }else {
                int index = Integer.parseInt(str);
                booleans[index] = true;
            }
        }
        if(booleans[num]){
            booleans[num] = false;
        }
        StringBuffer buffer = new StringBuffer();
        for(int i=1; i<=4094; i++){
            int right = i;
            while (booleans[right]){
                right++;
            }
            int count = right-i;
            if(count==1){
                buffer.append(i+",");
            }else if(count>1){
                buffer.append(i+"-"+(right-1)+",");
                i = right-1;
            }
        }
        String out = buffer.toString();
        System.out.println(out.substring(0, out.length()-1));
    }
}
