package demo.day12;

import java.util.*;

public class TestDome2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int pcNum = sc.nextInt();
        int connectNum = sc.nextInt();
        int[][] arr = new int[connectNum][3];
        for(int i=0; i<connectNum; i++){
            for(int j=0; j<3; j++){
                arr[i][j] = sc.nextInt();
            }
        }
        int startNum = sc.nextInt();
        int[] dp = new int[pcNum+1];
        Arrays.fill(dp, Integer.MAX_VALUE/2);
        dp[startNum] = 0;
        for(int i=0; i<pcNum; i++){
            for(int[] temp: arr){
                int a = temp[0], b = temp[1], c = temp[2];
                if(dp[a]+c < dp[b]){
                    dp[b] = dp[a] + c;
                }
            }
        }

        int maxTime = 0;
        for(int i=1; i<=pcNum; i++){
            if(dp[i] == Integer.MAX_VALUE/2){
                System.out.println(-1);
                return;
            }
            maxTime = Math.max(dp[i], maxTime);
        }
        System.out.println(maxTime);
    }
}
