package demo.day12;

import java.util.*;

public class TestDome5 {
    public static void main(String[] args) {
        //1、处理输入
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        StringBuffer buffer = new StringBuffer();
        int len = input.length();

        //2、判断字符串中的字符
        //使用变量inEndlish判断在什么模式
        //如果字符为#号，切换模式
        //如果字符为/，数字模式直接跳过，英文模式用来判断多次点击同一数字的次数
        //如果在数字模式输入12345
        //如果在英语模式输入字符
        String[] arrStr = new String[]{"abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
        boolean inEndlish = false;
        for(int i=0; i<len; i++){
            char c = input.charAt(i);
            if(c == '#'){
                if(!inEndlish){
                    inEndlish = true;
                }else {
                    inEndlish = false;
                }
                continue;
            }
            if(c == '/'){
                continue;
            }

            if(inEndlish){
                if(i+1<len){
                    char temp = input.charAt(i+1);
                    if(temp == c){
                        for(int j=i+1; j<=len; j++){
                            int count = j-i-1;
                            int index = Integer.parseInt(c+"")-2;
                            if(j == len){
                                i = j-1;
                                buffer.append(arrStr[index].charAt(count%(arrStr[index].length())));
                                break;
                            }
                            temp = input.charAt(j);
                            if(temp == '/'){
                                i = j;
                                buffer.append(arrStr[index].charAt(count%(arrStr[index].length())));
                                break;
                            }else if(temp != c){
                                i = j-1;
                                buffer.append(arrStr[index].charAt(count%(arrStr[index].length())));
                                break;
                            }
                        }
                    }else {
                        int index = Integer.parseInt(c+"")-2;
                        buffer.append(arrStr[index].charAt(0));
                    }
                }else {
                    int index = Integer.parseInt(c+"")-2;
                    buffer.append(arrStr[index].charAt(0));
                }
                continue;
            }

            if(!inEndlish){
                buffer.append(c);
                continue;
            }
        }

        //3、输出
        System.out.println(buffer.toString());
    }
}
