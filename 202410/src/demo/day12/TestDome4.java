package demo.day12;

import java.util.*;

public class TestDome4 {
    public static void main(String[] args) {
        //1、处理输入
        Scanner sc = new Scanner(System.in);
        int row = sc.nextInt();
        int col = sc.nextInt();
        int[][] arrSignal = new int[row][col];
        //2、找到最大值，信号源
        Queue<int[]> queue = new LinkedList<>();
        for(int i=0; i<row; i++){
            for(int j=0; j<col; j++){
                arrSignal[i][j] = sc.nextInt();
                if(arrSignal[i][j] > 0){
                    queue.offer(new int[]{i, j, arrSignal[i][j]});
                }
            }
        }
        int x = sc.nextInt();
        int y = sc.nextInt();
        //3、从信号源开始进行使用广度优先遍历处理
        //4、使用队列来记录不为0的值
        //如果遇到-1，不记录
        int[][] pos = new int[][]{{0,1},{0,-1},{1,0},{-1,0}};
        while (!queue.isEmpty()){
            int[] temp = queue.poll();
            int signal = temp[2];
            if(signal == 0){
                continue;
            }
            for(int i=0; i<4; i++){
                int x1 = temp[0] + pos[i][0];
                int y1 = temp[1] + pos[i][1];
                if(x1>=0&&x1<row&&y1>=0&&y1<col){
                    if(arrSignal[x1][y1] == -1){
                        continue;
                    }
                    if(arrSignal[x1][y1] < signal-1){
                        arrSignal[x1][y1] = signal-1;
                        queue.offer(new int[]{x1, y1, signal-1});
                    }
                }
            }

        }
        //5、输出
        System.out.println(arrSignal[x][y]);
    }
}
