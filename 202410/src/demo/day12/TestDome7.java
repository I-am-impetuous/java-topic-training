package demo.day12;

import java.util.*;

public class TestDome7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        String[] strings = sc.nextLine().split(" ");
        int len = strings.length;
        int index = 0;
        while (index<len){
            int tempLen = Integer.parseInt(strings[index+2]+strings[index+1], 16);

            if(input.equals(strings[index])){
                for (int i=index+3; i<index+3+tempLen; i++){
                    System.out.print(strings[i] + " ");
                }
                break;
            }else {
                index = tempLen+index+2+1;
            }
        }
    }
}
