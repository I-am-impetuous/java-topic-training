package demo.day12;

import java.util.*;

public class TestDome1 {
    public static void main(String[] args) {
        //1、处理输入
        Scanner sc = new Scanner(System.in);
        StringBuffer buffer = new StringBuffer();
//        while (!sc.hasNext("EOF")){
        while (sc.hasNext()){
            buffer.append(sc.nextLine()).append("\n");
        }
        //2、如果没有遇到；符号多行输入算一行
        //判断字符串，成对的单引号，使用一个变量记录是否在字符串内，遇到双引号单引号判断是否由转义字符
        //如果出现了--，并且不在字符串以内，是注释，注释中的；字符不算结束。
        String input = buffer.toString();
        int len = input.length();
        boolean inString = false;
        boolean inCommit = false;
        boolean isEmpty = true;
        int count = 0;
        char strNum = 0;
        for(int i=0; i<len; i++){
            char c = input.charAt(i);
            if(c == ';'){
                if(!inString&&!inCommit&&!isEmpty){
                    count++;
                    isEmpty = true;
                }
                continue;
            }

            if((c == '\"' || c=='\'')&&!inCommit&&!inString){
                inString = true;
                strNum = c;
                isEmpty = false;
                continue;
            }

            if((c == '\"' || c=='\'') && inString && !inCommit){
                if(strNum == c){
                    if(!(input.charAt(i-1) == '\\')){
                        inString = false;
                    }
                }
                continue;
            }

            if(c == '-'){
                if(!inString){
                    if(i+1 < len){
                        if(input.charAt(i+1) == '-'){
                            inCommit = true;
                        }
                    }
                }
                isEmpty = false;
                continue;
            }

            if(c == '\n'){
                inCommit = false;
                continue;
            }

            if(c != ' ' && !inString){
                isEmpty = false;
            }
        }
        if(!isEmpty){
            count++;
        }
        //3、输出
        System.out.println(count);
    }
}
