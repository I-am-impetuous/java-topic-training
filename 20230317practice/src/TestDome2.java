/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-17
 * Time: 22:47
 */
public class TestDome2 {
    public int jumpFloor(int target) {
        if(target <= 1){
            return 1;
        }
        int num1 = 1;
        int num2 = 1;
        while(target-- > 1){
            int temp = num1 + num2;
            num1 = num2;
            num2 = temp;
        }
        return num2;
    }
}
