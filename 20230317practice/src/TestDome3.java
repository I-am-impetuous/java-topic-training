/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-17
 * Time: 23:02
 */
public class TestDome3 {
    public int minCostClimbingStairs (int[] cost) {
        // write code here
        int num = cost.length+1;
        int[] dp = new int[num];
        dp[0] = 0;
        dp[1] = 0;
        for (int i = 2; i < num; i++) {
            dp[i] = Math.min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
        }
        return dp[num-1];
    }
}
