import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-20
 * Time: 22:40
 */
public class TestDome2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.nextLine().toUpperCase();
        String str2 = sc.nextLine().toUpperCase();
        int length1 = str1.length();
        int length2 = str2.length();
        boolean[][] booleansArray = new boolean[length2+1][length1+1];
        booleansArray[0][0] = true;
        for(int j=1; j<=length1; j++){
            if(str1.charAt(j-1) == '*'){
                booleansArray[0][j] = booleansArray[0][j-1];
            }else{
                booleansArray[0][j] = false;
            }
        }
        for(int i=1; i<=length2; i++){
            booleansArray[i][0] = false;
        }
        for(int i=1; i<=length2; i++){
            for(int j=1; j<=length1; j++){
                if(str1.charAt(j-1) == '*'){
                    booleansArray[i][j] = (booleansArray[i-1][j] || booleansArray[i][j-1]);
                }else if(str1.charAt(j-1) == '?'){
                    booleansArray[i][j] = booleansArray[i-1][j-1];
                    if(booleansArray[i][j]){
                        if(!((str2.charAt(i-1)>='0' && str2.charAt(i-1)<='9') ||
                                (str2.charAt(i-1)>='A' && str2.charAt(i-1)<='Z'))){
                            System.out.println("false");
                            return;
                        }
                    }
                }else{
                    if(str1.charAt(j-1) == str2.charAt(i-1) && booleansArray[i-1][j-1]){
                        booleansArray[i][j] = true;
                    }else {
                        booleansArray[i][j] = false;
                    }
                }
            }
        }
        System.out.println(booleansArray[length2][length1]);
    }
}
