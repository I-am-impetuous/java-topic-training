import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-02-25
 * Time: 23:15
 */
public class TestDome {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] array = new int[num*3];
        for(int i=0; i<array.length; i++){
            array[i] = sc.nextInt();
        }
        Arrays.sort(array);
        int length = array.length-2;
        long sum = 0;
        while(length>num-1){
            sum += array[length];
            length = length - 2;
        }
        System.out.println(sum);
    }
}
