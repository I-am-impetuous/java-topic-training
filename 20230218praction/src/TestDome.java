import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-02-18
 * Time: 23:59
 */
public class TestDome {
    public static void playCard(int[] cards, int n, int k) {
        // i --> 2 * i
        // i + n ---> 2 * i + 1
        for (int i = 0; i < k; ++i) {
        //一次洗牌的过程
            int[] newCards = new int[cards.length];
        //遍历编号为 0 ~ n - 1
            for (int j = 0; j < n; ++j) {
                newCards[2 * j] = cards[j];
                newCards[2 * j + 1] = cards[j + n];
            }
            cards = newCards;
        }
        //从上往下打印
        printCard(cards);
    }

    public static void printCard(int[] cards) {
        for (int i = 0; i < cards.length - 1; ++i) {
            System.out.print(cards[i] + " ");
        }
        System.out.println(cards[cards.length - 1]);
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int groups = s.nextInt();
        for (int i = 0; i < groups; ++i) {
        //读入每组数据
            int n = s.nextInt();
            int k = s.nextInt();
            int[] cards = new int[2 * n];
            for (int j = 0; j < 2 * n; ++j) {
                cards[j] = s.nextInt();
            }
        //洗牌
            playCard(cards, n, k);
        }
    }

}
