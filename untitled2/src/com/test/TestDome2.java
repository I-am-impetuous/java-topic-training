package com.test;

import java.util.*;

public class TestDome2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //1、处理输入
        String[] strings = sc.nextLine().split(",");
        int m = Integer.parseInt(strings[0]);
        int n = Integer.parseInt(strings[1]);
        int i = Integer.parseInt(strings[2]);
        int j = Integer.parseInt(strings[3]);
        int k = Integer.parseInt(strings[4]);
        int l = Integer.parseInt(strings[5]);
        int[][] arr = new int[m][n];
        for(int i1=0; i1<m; i1++){
            Arrays.fill(arr[i1], 1025);

        }
        Queue<int[]> queue = new LinkedList<>();
        if(i>=0&&i<m&&j>=0&&j<n){
            arr[i][j] = 0;
            queue.offer(new int[]{i,j});
        }
        if(k>=0&&k<m&&l>=0&&l<n){
            arr[k][l] = 0;
            queue.offer(new int[]{k,l});
        }
        //2、使用广度优先算法对四周进行标记
        //3、对两个点都进行标记，取最小值
        int[][] pos = new int[][]{{0,1},{0,-1},{1,0},{-1,0}};
        while (!queue.isEmpty()){
            int[] tempArr = queue.poll();
            for(int i1=0; i1<4; i1++){
                int x1 = tempArr[0]+pos[i1][0];
                int y1 = tempArr[1]+pos[i1][1];
                if(x1>=0&&x1<m&&y1>=0&&y1<n){
                    if(arr[tempArr[0]][tempArr[1]]+1<arr[x1][y1]){
                        arr[x1][y1] = arr[tempArr[0]][tempArr[1]]+1;
                        queue.offer(new int[]{x1, y1});
                    }
                }
            }
        }
        //4、输出
        int minTime = 0;
        for(int i1=0; i1<m; i1++){
            for(int j1=0; j1<n; j1++){
                if(minTime < arr[i1][j1]){
                    minTime = arr[i1][j1];
                }
            }
        }
        System.out.println(minTime);
    }

}
