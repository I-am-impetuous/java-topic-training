package com.test;

import java.util.*;

public class TestDome3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        String[] strings = sc.nextLine().split(" ");
        int[] arr = new int[strings.length];
        int len = arr.length;
        int sum = 0;
        for(int i=0; i<len; i++){
            arr[i] = Integer.parseInt(strings[i]);
            sum+=arr[i];
        }
        int leftNum = sum/2;
        int minDiff = sum;
        for(; leftNum<sum; leftNum++){
            int temp = findLeftNumIsBoolean(arr, leftNum, sum);
            if(minDiff >= temp && minDiff!=0){
                minDiff = temp;
            }else {
                break;
            }
        }
        System.out.println(minDiff);
    }

    private static int findLeftNumIsBoolean(int[] arr, int target, int sum) {
        int[][] dp = new int[arr.length+1][target+1];
        int len = arr.length;
        for(int i=1; i<=len; i++){
            for(int j=1; j<=target; j++){
                if(j < arr[i-1]){
                    dp[i][j] = dp[i-1][j];
                }else {
                    int last = dp[i-1][j];
                    int now = dp[i-1][j-arr[i-1]]+arr[i-1];
                    dp[i][j] = Math.max(last, now);
                }
            }
        }
        return Math.abs((sum-dp[len][target]) - dp[len][target]);
    }
}
