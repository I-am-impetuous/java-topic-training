package com.test;

import java.util.*;

public class TestDome4 {
    public static int minDiff = Integer.MAX_VALUE;

    public static void main(String[] args) {
        //1、处理输入
        Scanner sc = new Scanner(System.in);
        int[] arrNum = new int[10];
        boolean[] booleans = new boolean[10];
        for(int i=0; i<10; i++){
            arrNum[i] = sc.nextInt();
        }
        //2、
        Arrays.sort(arrNum);
        int index = 0;
        int tempLevel = 0;
        int tempSum = 0;
        //3、
        dfs(index, tempLevel, tempSum, arrNum, booleans);
        //处理输入
        System.out.println(minDiff);
    }

    private static void dfs(int index, int length, int sum, int[] arrNum,  boolean[] booleans) {
        if(length == 5){
            int tempSum = 0;
            for(int a: arrNum){
                tempSum+=a;
            }
            int diff = Math.abs(sum - (tempSum-sum));
            minDiff = Math.min(minDiff, diff);
            return;
        }
        for(int i=index; i<10; i++){
            if(i>index&&arrNum[i]==arrNum[i-1] && !booleans[i-1]){
                continue;
            }
            booleans[i] = true;
            dfs(i+1, length+1, sum+arrNum[i], arrNum, booleans);
            booleans[i] = false;
        }
    }
}
