package com.test;

import java.util.*;

// 注意类名必须为 Main, 不要有任何 package xxx 信息

public class TestDome1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {// 注意，如果输入是多个测试用例，请通过while循环处理多个测试用例
            //1、处理输入
            String input = sc.nextLine();
            char[] chars = input.toCharArray();
            //2、对字符串进行排序
            Arrays.sort(chars);
            String tempInput = Arrays.toString(chars);
            int len = chars.length;
            //3、对排序的字符串和原来的字符串进行比较
            int index = -1;
            for (int i = 0; i < len; i++) {
                if (input.charAt(i) != chars[i]) {
                    index = i;
                    break;
                }
            }
            //如果和原来的字符不匹配就在原来的字符串中寻找这个字符，然后进行交换
            int index2 = index;
            if (index != -1) {
                for (int i = index2; i < len; i++) {
                    if ( chars[index] == input.charAt(i)) {
                        index2 = i;
                    }
                }
            } else {
                System.out.println(input);
                return;
            }
            char[] chars2 = input.toCharArray();
            char temp = input.charAt(index);
            chars2[index] = chars2[index2];
            chars2[index2] = temp;
            //4、处理输入
            String out = "";
            for (char c : chars2) {
                out += c;
            }
            System.out.println(out);
        }
    }
}
