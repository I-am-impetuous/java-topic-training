import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-30
 * Time: 17:21
 */
public class TestDome2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        while(num != 0){
            int count = 0;
            while(num >= 2){
                num /= 2;
                count++;
            }
            System.out.println(count);
            num = sc.nextInt();
        }
    }
}
