import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-20
 * Time: 13:34
 */
public class TestDome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        if(!sc.hasNextInt()){
            System.out.println(0);
            return;
        }
        int num = sc.nextInt();
        if(num == 0){
            System.out.println(0);
            return;
        }
        int[] array = new int[num];
        for(int i=0; i<num; i++){
            array[i] = sc.nextInt();
        }
        long count = 0; //病毒数量
        int sum = 0; //生存天数
        for(int arr: array){
            if(arr>=-10 && arr<=30){
                if(count == 0){
                    count = 1;
                }else {
                    count = 2*count;
                    sum++;
                }
            }else {
                if(count == 0){
                    continue;
                }else if(count<=4){
                    System.out.println(sum+1);
                    return;
                }else {
                    count = count/2;
                    sum++;
                }
            }
        }
        System.out.println(sum);
    }
}
