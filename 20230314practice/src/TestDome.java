import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-14
 * Time: 9:49
 */
public class TestDome {
    public int[] twoSum0 (int[] numbers, int target) {
        // write code here
        int[] array = new int[2];
        for(int i=0; i<numbers.length; i++){
            for(int j=i+1; j<numbers.length; j++){
                if(numbers[i] + numbers[j] == target){
                    array[0] = i+1;
                    array[1] = j+1;
                    return array;
                }
            }
        }
        return array;
    }

    public int[] twoSum (int[] numbers, int target) {
        // write code here
        int[] array = new int[2];
        int length = numbers.length;
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for(int i=0; i<length; i++){
            if(hashMap.containsKey(target - numbers[i])){
                array[0] = hashMap.get(target - numbers[i]) + 1;
                array[1] = i+1;
            }else{
                hashMap.put(numbers[i], i);
            }
        }
        return array;
    }


    public static int[] FindNumsAppearOnce (int[] array) {
        // write code here
        HashMap<Integer, Integer> map = new HashMap<>();
        int length = array.length;
        for(int i=0; i<length; i++){
            if(map.containsKey(array[i])){
                map.remove(array[i], 1);
            }else{
                map.put(array[i], 1);
            }
        }
        int[] ret = new int[2];
        int count = 0;
        for(int i=0; i<length; i++){
            if(map.containsKey(array[i])){
                ret[count++] = array[i];
            }
        }
        if(ret[0] > ret[1]){
            int temp = ret[0];
            ret[0] = ret[1];
            ret[1] = temp;
        }
        return ret;
    }

    public static void main(String[] args) {
        int[] array = new int[]{4,6,1,1,1,1};
        System.out.println(FindNumsAppearOnce(array));
    }

}
