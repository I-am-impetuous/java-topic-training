import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-11-09
 * Time: 22:06
 */
public class TestDome {

    public int Fibonacci1(int n) {
        if(n==1 || n==2){
            return 1;
        }
        int frist = 1;
        int second = 1;
        int thrid = 2;
        while(n > 2){
            n--;
            thrid = frist + second;
            frist = second;
            second = thrid;
        }
        return thrid;
    }

    public int Fibonacci2(int n) {
        if(n==1 || n==2){
            return 1;
        }
        return Fibonacci2(n-1) + Fibonacci2(n-2);
    }


    private Map<Integer,Integer> map = new HashMap<>();

    public int Fibonacci3(int n) {
        if(n==1 || n==2){
            return 1;
        }
        int frist;
        if(map.containsKey(n-2)){
            frist = map.get(n-2);
        }else{
            frist = Fibonacci3(n-2);
            map.put(n-2, frist);
        }
        int second;
        if(map.containsKey(n-1)){
            second = map.get(n-1);
        }else{
            second = Fibonacci3(n-1);
            map.put(n-1, second);
        }
        return frist + second;
    }

}
