/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-10-20
 * Time: 23:22
 */
public class TestDome {
    public int minNumberInRotateArray(int [] array) {
        if(array == null || array.length == 0){
            return 0;
        }
        int left = 0;
        int right = array.length - 1;
        int mid = 0;
        while(array[left] >= array[right]){
            if(right - left == 1){
                mid = right;
                break;
            }
            mid = left + ((right - left)>>1);
            if(array[left] == array[right] && array[mid] == array[left]){ //1
                int result = array[left];
                for(int i = left+1; i < right; i++){ //left和right值是相等的
                    if(array[i] < result){
                        result = array[i];
                    }
                }
                return result;
            }
            if(array[mid] >= array[left]){
                left = mid;
            }
            else{
                right = mid;
            }
        }
        return array[mid];
    }
}
