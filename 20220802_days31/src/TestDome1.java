import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-08-02
 * Time: 18:05
 */
public class TestDome1 {

    private static ArrayList<Integer> list = null;

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int num = sc.nextInt();
            list = new ArrayList<>();
            func(num);
            System.out.print(num + " = " + list.get(0));
            for(int i=1; i<list.size(); i++){
                System.out.print(" * " + list.get(i));
            }
            System.out.println();
        }
    }

    public static void func(int num){
        for(int i=2; i<=Math.sqrt(num); i++){
            if(num % i == 0){
                list.add(i);
                int temp = num/i;
                func(temp);
                return;
            }
        }
        list.add(num);
    }
}
