import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description: 链表相加
 * User: 我很浮躁
 * Date: 2023-08-25
 * Time: 22:50
 */

class ListNode{

    public int val;
    public ListNode next;

    public ListNode(int val){
        this.val = val;
    }
}

public class TestDome {
    public ListNode addInList (ListNode head1, ListNode head2) {
        // write code here
        //1.判断特殊情况
        if(head1 == null){
            return head2;
        }
        if(head2 == null){
            return head1;
        }
        //2.设置是否进制位
        int flag = 0;
        //3.把长度大小设置成固定的
        String s = "";
        while(head1 != null){
            s += head1.val;
            head1 = head1.next;
        }
        String t = "";
        while(head2 != null){
            t += head2.val;
            head2 = head2.next;
        }
        if(t.length() < s.length()){
            String temp = s;
            s = t;
            t = temp;
        }
        //4.核心运算相加
        int lengthS = s.length();
        int lengthT = t.length();
        StringBuffer buffer = new StringBuffer();
        while(lengthT != 0){
            int temp = 0;
            if(lengthS > 0){
                temp = (int)((s.charAt(lengthS-1) + t.charAt(lengthT-1) - '0' - '0') + flag);
            }else{
                temp = (int)((t.charAt(lengthT-1) - '0') + flag);
            }
            flag = temp / 10;
            temp = temp % 10;
            buffer.append((char)(temp+'0'));
            lengthS--;
            lengthT--;
        }
        //5.是否要新加节点
        if(flag == 1){
            buffer.append('1');
        }
        String temp  = buffer.toString();
        ListNode head = null;
        for(int i=0; i<temp.length(); i++){
            if(head == null){
                ListNode node = new ListNode(temp.charAt(i) - '0');
                head = node;
            }else{
                ListNode node = new ListNode(temp.charAt(i) - '0');
                node.next = head;
                head = node;
            }
        }
        return head;
    }
}
