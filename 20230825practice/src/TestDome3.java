import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description: 扑克牌比大小
 * User: 我很浮躁
 * Date: 2023-08-26
 * Time: 10:40
 */
public class TestDome3 {

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] strings = str.split("-");  //使用split来分割成两手牌
        String[] temp0 = strings[0].split(" ");   //为了计算牌的个数
        String[] temp1 = strings[1].split(" ");
        String str2 = "34567891JQKA2";   //为了比较牌的大小
        if(strings[0].equals("joker JOKER") || strings[1].equals("joker JOKER")) {
            System.out.println("joker JOKER");   //第一种情况，出现大小王的情况
        }else if((strings[0].equals("joker")&&(!(strings[1].equals("JOKER")))) ||
                ((strings[1].equals("joker"))&&(!(strings[0].equals("JOKER"))))){
            System.out.println("joker");
        }else if(strings[0].equals("JOKER") || strings[1].equals("JOKER")){
            System.out.println("JOKER");
        }else if(temp0.length == temp1.length){   //第二种情况，牌的个数相等
            int num0 = str2.indexOf(strings[0].charAt(0));
            int num1 = str2.indexOf(strings[1].charAt(0));
            if(num0 > num1){
                System.out.println(strings[0]);
            }else {
                System.out.println(strings[1]);
            }
        }else if(temp0.length == 4){   //第三种情况，牌的个数为4的和牌的个数不为4的牌比较
            System.out.println(strings[0]);
        }else if(temp1.length == 4){
            System.out.println(strings[1]);
        }else{                      //第四种情况，两手牌无法比较。
            System.out.println("ERROR");
        }
    }
}
