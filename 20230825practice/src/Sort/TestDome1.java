package Sort;

import com.sun.xml.internal.ws.addressing.WsaActionUtil;

/**
 * Created with IntelliJ IDEA
 * Description: 各种排序
 * User: 我很浮躁
 * Date: 2023-08-26
 * Time: 16:08
 */
public class TestDome1 {

    //直接插入排序
    //1.在 i 坐标前面的元素已经排序，然后从 i-1 坐标开始和 i 坐标的值开始比较，然后交换值
    //2.如果是升序，当 i 坐标的值大于 i-1 坐标的值停止;  如果是降序，当 i 当 i 坐标的值小于 i-1 坐标的值停止
    public static int[] insertSort(int[] arr){
        for(int i=1; i<arr.length; i++){
            int temp = arr[i];
            int j=i-1;
            for(; j>=0 && temp<arr[j]; j--){
                arr[j+1] = arr[j];
            }
            arr[j+1] = temp;
        }
        return arr;
    }

    //希尔排序
    //1.把数组分成 d 组，每隔 d 个间隔的元素为一组
    //2.每一组都要进行插入排序
    //3.把 d 变小，重复上面的步骤，直到 d == 1。
    public static int[] shellSort(int[] arr){
        // d 表示是分成几组
        int d = arr.length;
        while(true){
            d = d/2;
            for(int i=0; i<d; i++){  //这个循环表示进行 d 次插入排序。
                for(int j=i+d; j<arr.length; j+=d){  //进行插入排序,间隔是d
                    int temp = arr[j];
                    int k = j-d;
                    for(; k>=0 && arr[k]>temp; k-=d){
                        arr[k+d] = arr[k];
                    }
                    arr[k+d] = temp;
                }
            }
            if(d == 1){
                break;
            }
        }
        return arr;
    }

    //选择排序
    public static int[] selectSort(int[] arr){
        for(int i=0; i<arr.length; i++){
            int j=i+1;
            int temp = i;
            for(; j<arr.length; j++){
                if(arr[temp] > arr[j]){
                    temp = j;
                }
            }
            // 交换位置
            int num = arr[i];
            arr[i] = arr[temp];
            arr[temp] = num;
        }
        return arr;
    }

    //堆排序
    public static int[] HeapSort(int[] arr){

        return null;
    }




//
//    //直接插入排序
//    public static int[] insertSort(int[] arr){
//        return null;
//    }
//
//    //直接插入排序
//    public static int[] insertSort(int[] arr){
//        return null;
//    }
//
//    //直接插入排序
//    public static int[] insertSort(int[] arr){
//        return null;
//    }

    public static void main(String[] args) {
        int[] array1 = {49,72,38,65,23};
        array1 = insertSort(array1);
        int[] array = {1,12,6,3,78,34,54,45,56,100};
        array = shellSort(array);
        for (int arr: array) {
            System.out.print(arr+" ");
        }
    }
}
