/**
 * Created with IntelliJ IDEA
 * Description: 链表相加
 * User: 我很浮躁
 * Date: 2023-08-26
 * Time: 9:23
 */
public class TestDome2 {
    public ListNode addInList (ListNode head1, ListNode head2) {
        // write code here
        if(head1 == null){
            return head2;
        }
        if(head2 == null){
            return head1;
        }
        //1.反转两个链表
        head1 = reverseNode(head1);
        head2 = reverseNode(head2);
        //2.核心代码。判断是否进位，进行相加
        int flag = 0;
        ListNode ret = new ListNode(-1);
        ListNode head = ret;
        while(head1 != null || head2 != null || flag == 1){
            int val1 = 0;
            int val2 = 0;
            if(head1 != null){
                val1 = head1.val;
            }
            if(head2 != null){
                val2 = head2.val;
            }
            int temp = val1 + val2 + flag;
            flag = temp/10;
            temp = temp%10;
            head.next = new ListNode(temp);
            head = head.next;
            if(head1 != null){
                head1 = head1.next;
            }
            if(head2 != null){
                head2 = head2.next;
            }
        }
        ret = ret.next;
        //3.把新的链表反转回来
        ret = reverseNode(ret);
        return ret;
    }

    //反转链表代码
    private ListNode reverseNode(ListNode head) {
        // TODO
        //一个节点一个节点的改指向，有三个变量，一个变量是当前节点，
        //一个变量是前一个节点，一个变量是后一个节点
        if(head == null){
            return null;
        }
        ListNode pre = null;
        while(head != null){
            ListNode curNext = head.next;
            head.next = pre;
            pre = head;
            head = curNext;
        }
        return pre;
    }
}
