import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-13
 * Time: 23:58
 */
public class TestDome2 {

    public int MoreThanHalfNum_Solution1(int [] array) {
        //非空判断、是否有解
        if(array == null || array.length == 0){
            return -1;
        }
        Arrays.sort(array);
        int num = array.length/2;
        int mid = array[num];
        int count = 0;
        for(int i=0; i<array.length; i++){
            if(mid == array[i]){
                count++;
            }
        }
        if(count > num){
            return mid;
        }else{
            return -1;
        }
    }

    public int MoreThanHalfNum_Solution(int [] array) {
        int time = 1;
        int temp = array[0];
        for(int i=1; i<array.length; i++){
            if(time != 0){
                if(temp == array[i]){
                    time++;
                }else{
                    time--;
                }
            }else{
                time = 1;
                temp = array[i];
            }
        }
        return temp;
    }


    public static void main(String[] args) {

    }
}
