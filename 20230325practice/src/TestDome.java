/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-25
 * Time: 23:11
 */
public class TestDome {
    public static int uniquePaths1 (int m, int n) {
        // write code here
        if(n==1 || m==1){
            return 1;
        }
        return uniquePaths1(m-1, n) + uniquePaths1(m, n-1);
    }


    public static int uniquePaths (int m, int n) {
        // write code here
        int[][] dp = new int[m][n];
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                if(i==0){
                    dp[i][j] = 1;
                    continue;
                }
                if(j==0){
                    dp[i][j] = 1;
                    continue;
                }
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return dp[m-1][n-1];
    }



    public static void main(String[] args) {
    }
}
