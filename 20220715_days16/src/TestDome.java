import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-15
 * Time: 17:08
 */
public class TestDome {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if(n == 1){
            return;
        }
        int count = 0;
        int sum = 1;
        for(int i=2; i<=n; i++){
            sum = 1;
            double sqrt = Math.sqrt(i);
            for(int j=2; j<=sqrt; j++){
                if(i % j == 0){
                    sum += j;
                    int temp = i/j;
                    if(j != temp){
                        sum += temp;
                    }
                }
            }
            if(sum == i){
                count++;
            }
        }
        System.out.println(count);
    }
}
