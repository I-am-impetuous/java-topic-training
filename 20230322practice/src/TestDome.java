/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-22
 * Time: 19:19
 */
public class TestDome {
    public static int SecondMax(int[] array){
        int max = array[0];
        int temp = 0;
        for(int i=1; i<array.length; i++){
            if(temp < array[i] && max > array[i]){
                temp = array[i];
            }
            if(max < array[i]){
                temp = max;
                max = array[i];
            }
        }
        return temp;
    }
    public static void main(String[] args) {
        int[] array = {333,502, 89, 350,23, 56, 78, 96, 101, 201};
        int temp = SecondMax(array);
        System.out.println(temp);
    }
}
