/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-07
 * Time: 14:43
 */
public class TestDome1 {

    public static int addAB(int A, int B) {
        // write code here
        int xor, and;
        while(B != 0){
            xor = A ^ B;
            and = (A & B) << 1;
            A = xor;
            B = and;
        }
        return A;
    }

    public static void main(String[] args) {

    }
}
