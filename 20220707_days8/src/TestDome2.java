import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-07
 * Time: 14:54
 */
public class TestDome2 {
    public static int func(int n, int m){
        if((n == 1 && m >= 1) || (m == 1 && n >= 1)){
            return m + n;
        }
        return func(n-1, m) + func(n, m-1);
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int sum = func(n, m);
        System.out.println(sum);
    }
}
