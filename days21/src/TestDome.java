import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-08-29
 * Time: 22:50
 */
public class TestDome {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int group = sc.nextInt();
        for(int i=0; i<group; i++){
            int card = sc.nextInt();
            int frequency = sc.nextInt();
            int[] array = new int[2 * card];
            for(int k=0; k<2*card; k++){
                array[k] = sc.nextInt();
            }
            //洗牌
            for(int j=0; j<frequency; j++){
                array = shuffleCard(array, card);
            }
            //输出函数
            print(array);
        }
    }

    public static int[] shuffleCard(int[] array, int card){
        int[] temp = new int[2 * card];
        int k=0;
        for(int i=0; i<2*card; i+=2){
            temp[i] = array[k];
            temp[i+1] = array[k+card];
            k++;
        }
        return temp;
    }

    public static void print(int[] array){
        for(int i=0; i<array.length-1; i++){
            System.out.print(array[i] + " ");
        }
        System.out.println(array[array.length-1]);
    }
}
