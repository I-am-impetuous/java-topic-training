import java.io.IOException;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-08-29
 * Time: 23:27
 */
public class TestDome2 {

    private static int front = 1;
    private static int rear = 1;
    private static int index = 1;

    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        int sum = sc.nextInt();
        if(sum >= 4){
            rear = 4;
        }else {
            rear = sum;
        }
        String str = sc.next();
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i) == 'U'){
                moveUp(sum);
            }else if(str.charAt(i) == 'D'){
                moveDown(sum);
            }
        }
        //打印
        for(int i=front; i<=rear; i++){
            System.out.print(i + " ");
        }
        System.out.println();
        System.out.println(index);
    }

    //Down
    private static void moveDown(int sum) {
        //最后一个位置
        if(index == sum && index == rear){
            front = 1;
            if(sum >= 4){
                rear = 4;
            }else {
                rear = sum;
            }
            index = 1;
        }else if(index == rear && index < sum){  //不是最后一个位置，用翻页
            index++;
            front++;
            rear++;
        }else if(index < rear && index >= front){ //不用翻页
            index++;
        }
    }

    //Up
    private static void moveUp(int sum) {
        //第一个位置
        if(index == front && index == 1){
            rear = sum;
            if(sum >= 4){
                front = sum-3;
            }else {
                front = 1;
            }
            index = sum;
        }else if(index == front && index > 1){  //不是第一个位置，用翻页
            index--;
            rear--;
            front--;
        }else if(index > front && index <= rear){  //不用翻页
            index--;
        }
    }
}
