import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-15
 * Time: 9:54
 */
public class TestDome {
    public ArrayList<ArrayList<Integer>> threeSum(int[] num) {
        ArrayList<ArrayList<Integer>> arrayLists = new ArrayList<>();
        //排序
        Arrays.sort(num);
        int length = num.length;
        for(int i=0; i<length; i++){
            if(i!=0 && num[i] == num[i-1]){
                continue;
            }
            int start = i+1;
            int end = num.length-1;
            int target = -num[i];
            while(end > start){
                if(num[start]+num[end] == target){
                    ArrayList<Integer> arrayList = new ArrayList<>();
                    arrayList.add(num[i]);
                    arrayList.add(num[start]);
                    arrayList.add(num[end]);
                    arrayLists.add(arrayList);
                    end--;
                    start++;
                    while (start+1<end && num[start] == num[start+1]){
                        start++;
                    }
                    while (end-1>start && num[end] == num[end-1]){
                        end--;
                    }
                }else if(num[start]+num[end] > target){
                    end--;
                }else if(num[start]+num[end] < target){
                    start++;
                }
            }
        }
        return arrayLists;
    }
}
