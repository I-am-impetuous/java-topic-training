import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * Description: 剑指Offer
 * User: 我很浮躁
 * Date: 2022-10-26
 * Time: 20:07
 */
class ListNode{
    int val;
    ListNode next;
}

public class TestDome {
    //第一种方法: 使用Stack数据结构
    public ArrayList<Integer> printListFromTailToHead1(ListNode listNode) {
        Stack<Integer> stack = new Stack<>();
        ArrayList<Integer> arrayList = new ArrayList();
        while(listNode != null){
            stack.push(listNode.val);
            listNode = listNode.next;
        }
        int stackLength = stack.size();
        for(int i=0; i<stack.size(); i++){
            arrayList.add(stack.pop());
        }
        return arrayList;
    }

    //第二种方法: 使用数组逆置方法
    public ArrayList<Integer> printListFromTailToHead2(ListNode listNode) {
        ArrayList<Integer> arrayList = new ArrayList();
        while (listNode != null) {
            arrayList.add(listNode.val);
            listNode = listNode.next;
        }
        int i = 0;
        int j = arrayList.size() - 1;
        while (i < j){
            int temp = arrayList.get(i);
            arrayList.set(i, arrayList.get(j));
            arrayList.set(j, temp);
            i++;
            j--;
        }
        return arrayList;
    }

    private void fromTailToHead(ArrayList<Integer> arrayList, ListNode listNode) {
        if(listNode == null){
            return;
        }
        fromTailToHead(arrayList, listNode.next);
        arrayList.add(listNode.val);
    }
    //第三种方法: 使用递归方式
    public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        ArrayList<Integer> arrayList = new ArrayList();
        fromTailToHead(arrayList, listNode);
        return arrayList;
    }

}
