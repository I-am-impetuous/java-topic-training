/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-06-12
 * Time: 21:35
 */


// 指定位置链表反转
public class TestDome2 {
    public void newReverse(ListNode leftNode, ListNode rightNode){
        rightNode.next = null;
        ListNode head = null;
        rightNode = leftNode;
        while(leftNode != null){
            ListNode temp = leftNode.next;
            leftNode.next = head;
            head = leftNode;
            leftNode = temp;
        }
    }
    public ListNode reverseBetween (ListNode head, int m, int n) {
        // write code here
        ListNode newHead = new ListNode(0);
        newHead.next = head;
        ListNode startNode = newHead;
        ListNode endNode = newHead;
        // 1.找到第一个位置
        while (m != 0 && startNode != null) {
            m--;
            startNode = startNode.next;
        }
        ListNode leftNode = startNode.next;
        // 2.找到第二个位置
        while (n != 0 && endNode != null) {
            n--;
            endNode = endNode.next;
        }
        ListNode rightNode = endNode.next;
        endNode = rightNode.next;
        // 2.建立一个新的链表记录从第一个位置到第二个位置反转
        newReverse(leftNode, rightNode);
        startNode.next = leftNode;
        rightNode.next = endNode;
        return newHead.next;
    }
}
