/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-06-12
 * Time: 21:22
 */

// 反转列表
public class TestDome1 {
    public ListNode ReverseList(ListNode head) {
        ListNode newNode = null;
        while(head != null){
            ListNode temp = head.next;
            head.next = newNode;
            newNode = head;
            head = temp;
        }
        return newNode;
    }
    public static void main(String[] args) {

    }
}
