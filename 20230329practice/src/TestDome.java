import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-29
 * Time: 23:36
 */
public class TestDome {
    public static int solve (String nums) {
        // write code here
        if(nums.equals("0")){
            return 0;
        }
        if(nums.equals("10") || nums.equals("20")){
            return 1;
        }
        int length = nums.length();

        for(int i=1; i<length; i++){
            if(nums.charAt(i) == '0' && nums.charAt(i-1) != '1' && nums.charAt(i-1) != '2'){
                return 0;
            }
        }
        int[] dp = new int[length+1];
        Arrays.fill(dp, 1);
        for(int i=2; i<length+1; i++){
            if(nums.charAt(i-1) != '0' && nums.charAt(i-2) == '1' ||
                    (nums.charAt(i-1) >= '1' && nums.charAt(i-1) <= '6' && nums.charAt(i-2) == '2')){
                dp[i] = dp[i-1] + dp[i-2];
            }else {
                dp[i] = dp[i-1];
            }
        }
        return dp[length];
    }

    public static void main(String[] args) {
        String str = "11";
        System.out.println(solve(str));
    }
}
