/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-10-28
 * Time: 22:50
 */
public class TestDome {
        public int minNumberInRotateArray(int [] array) {
            if (array == null || array.length == 0) {
                return 0;
            }
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    return array[i + 1];
                }
            }
            return array[0];
        }
}
