import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-10-31
 * Time: 16:07
 */
public class TestDome {

    //二分查找
    public static int MyBinarySearch(int[] array, int key){
        int left = 0;
        int right = array.length-1;
        int mid = 0;
        while (left <= right){
            mid = (left + right)/2;
            if(array[mid] == key){
                return mid;
            }else if(key > array[mid]){
                left = mid + 1;
            }else {
                right = mid - 1;
            }
        }
        return -1;
    }

    public static void myBubbleSort(int[] array){
        int length = array.length-1;
        for(int i=0; i<length; i++){
            for(int j=0; j<length-i; j++){
                if(array[j] > array[j+1]){
                    swap(array, j);
                }
            }
        }
    }

    private static void swap(int[] array, int j) {
        int temp = array[j];
        array[j] = array[j+1];
        array[j+1] = temp;
    }

    public static void main(String[] args) {
        int [] array = {4, 15, 16, 24, 31, 68, 79, 111};
        Arrays.sort(array);
        int ret = MyBinarySearch(array, 50);
        System.out.println(ret);

        int [] arr = {50, 20, 60, 40, 30, 25, 55, 99, 192, 111, 35};
        myBubbleSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
