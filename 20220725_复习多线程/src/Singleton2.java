/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-25
 * Time: 8:46
 */
public class Singleton2 {
    private volatile static Singleton2 instance = null;

    private Singleton2(){}

    public static Singleton2 getInstance(){

        if(instance == null){
            synchronized (Singleton2.class){
                if(instance == null){
                    instance = new Singleton2();
                }
            }
        }

        return instance;
    }
}
