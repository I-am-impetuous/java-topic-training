package october.t9th;

/**
 * Created with IntelliJ IDEA
 * Description: 数值的整数次方
 * User: 我很浮躁
 * Date: 2023-10-09
 * Time: 20:39
 */
public class TestDome3 {
    public double Power(double base, int exponent) {
        double sum = 1;
        if(exponent < 0){
            base = 1/base;
            exponent = -exponent;
        }
        while(exponent != 0){
            exponent--;
            sum = sum * base;
        }
        return sum;
    }
}
