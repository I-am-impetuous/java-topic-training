package october.t9th;

/**
 * Created with IntelliJ IDEA
 * Description:有效三角形的个数
 * User: 我很浮躁
 * Date: 2023-10-09
 * Time: 19:32
 */
public class TestDome2 {
    public int validTriangle (int[] nums) {
        // write code here
        if(nums.length < 3){
            return 0;
        }
        int length = nums.length;
        int sum = 0;
        for(int i=0; i<length-2; i++){
            int num1 = nums[i];
            for(int j=i+1; j<length-1; j++){
                int num2 = nums[j];
                int low = Math.abs(num1- num2);
                int hight = Math.abs(num1+num2);
                for(int k=j+1; k<length; k++){
                    if(nums[k] < hight && nums[k] > low){
                        sum++;
                    }
                }
            }
        }
        return sum;
    }
}
