package october.t9th;

/**
 * Created with IntelliJ IDEA
 * Description:打印从1到最大的n位数
 * User: 我很浮躁
 * Date: 2023-10-09
 * Time: 20:44
 */
public class TestDome4 {
    public int[] printNumbers (int n) {
        // write code here
        int sum = (int)Math.pow(10.0, (double)n);
        int[] arr = new int[sum-1];
        for(int i=1; i<=sum-1; i++){
            arr[i-1] = i;
        }
        return arr;
    }
}
