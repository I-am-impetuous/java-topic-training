package october.t8th;

/**
 * Created with IntelliJ IDEA
 * Description: 剪绳子
 * User: 我很浮躁
 * Date: 2023-10-08
 * Time: 16:34
 */
public class TestDome2 {
    public int cutRope (int n) {
        // write code here
        if(n<=3){
            return n-1;
        }
        int[] dp = new int[n+1];
        dp[1] = 1;
        dp[2] = 2;
        dp[3] = 3;
        dp[4] = 4;
        for(int i=5; i<=n; i++){
            int left = 1;
            int right = i-1;
            while(left <= right){
                dp[i] = Math.max(dp[left] * dp[right], dp[i]);
                left++;
                right--;
            }
        }
        return dp[n];
    }
}
