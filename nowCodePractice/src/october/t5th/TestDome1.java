package october.t5th;

/**
 * Created with IntelliJ IDEA
 * Description: 矩阵中的路径
 * User: 我很浮躁
 * Date: 2023-10-05
 * Time: 20:46
 */
public class TestDome1 {
    public boolean hasPath (char[][] matrix, String word) {
        // write code here
        if(matrix == null || matrix.length == 0){
            return false;
        }
        int m = matrix.length;
        int n = matrix[0].length;
        boolean[][] flag = new boolean[m][n];
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                if(dfs(matrix, m, n, i, j, word, 0, flag)){
                    return true;
                }
            }
        }
        return false;
    }

    private boolean dfs(char[][] matrix, int m, int n, int i, int j, String word, int k, boolean[][] flag) {
        if(i<0 || i>=m || j<0 || j>=n || matrix[i][j] != word.charAt(k) || flag[i][j] == true){
            return false;
        }
        if(k == word.length()-1){
            return true;
        }
        flag[i][j] = true;
        if(dfs(matrix, m, n, i-1, j, word, k+1, flag) ||
                dfs(matrix, m, n, i+1, j, word, k+1, flag) ||
                dfs(matrix, m, n, i, j-1, word, k+1, flag) ||
                dfs(matrix, m, n, i, j+1, word, k+1, flag)){
            return true;
        }
        flag[i][j] = false;
        return false;
    }
}
