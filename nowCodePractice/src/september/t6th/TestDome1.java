package september.t6th;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Description: 计算字符串中字符的频率
 * User: 我很浮躁
 * Date: 2023-09-06
 * Time: 10:25
 */
public class TestDome1 {
    public static char largeStr(String str){
        Map<Character, Integer> map = new HashMap<>();
        char c = str.charAt(0);
        int maxLen = 1;
        map.put(str.charAt(0), 1);
        for(int i=1; i<str.length(); i++){
            if(map.containsKey(str.charAt(i))){
                map.put(str.charAt(i), map.get(str.charAt(i))+1);
                if(map.get(str.charAt(i))>maxLen){
                    maxLen = map.get(str.charAt(i));
                    c = str.charAt(i);
                }
            }else {
                map.put(str.charAt(i), 1);
            }
        }
        maxLen = 0;
        c = ' ';
        for(Map.Entry<Character, Integer> entry : map.entrySet()){
            if(maxLen < entry.getValue()){
                maxLen = entry.getValue();
                c = entry.getKey();
            }
        }
        return c;
    }
    public static void main(String[] args) {
        String str = "1423173445242426747777777";
        System.out.println(largeStr(str));
    }
}
