package september.t6th;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 * Description: 求最长子串
 * User: 我很浮躁
 * Date: 2023-09-06
 * Time: 15:37
 */
public class TestDome2 {
    public static String maxStr(String str){
        HashMap<Character, Integer> map = new HashMap<>();
        int len = str.length();
        int left = 0;
        int maxLen = 0;
        int index = 0;
        for(int i=0; i<len; i++){
            if(!(map.containsKey(str.charAt(i)))){
                map.put(str.charAt(i), 1);
            }else{
                map.put(str.charAt(i), map.get(str.charAt(i))+1);
            }
            while(left <= i &&  map.get(str.charAt(i))>1){
                map.put(str.charAt(left), map.get(str.charAt(left))-1);
                left++;
            }
            if(maxLen < (i-left+1)){
                index = left;
                maxLen = i-left+1;
            }
        }
        String ret = str.substring(index, index+maxLen);
        System.out.println(maxLen);
        return ret;
    }

    public static void main(String[] args) {
        //1.使用HashMap来存放字符
        //3.使用双指针来记录最左端和最右端的数字
        //4.通过map来判断字串中是否有重复字符
        String str = "abcaaddcegfa";

        System.out.println(maxStr(str));
    }
}
