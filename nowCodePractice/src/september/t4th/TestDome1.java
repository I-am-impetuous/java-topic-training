package september.t4th;

/**
 * Created with IntelliJ IDEA
 * Description: 快速排序
 * User: 我很浮躁
 * Date: 2023-09-04
 * Time: 11:05
 */
public class TestDome1 {
    public static void main(String[] args) {
        int[] array = {5, 3, 1, 7, 9, 2, 4, 6, 8};
        quickSort(array, 0, array.length-1);
        for(int arr : array){
            System.out.print(arr + " ");
        }
    }

    private static void quickSort(int[] array, int left, int right) {
        if(left > right){
            return;
        }
        int i=left;
        int j=right;
        int temp = array[left];
        while(i<j){
            while(i<j && array[j] > temp){
                j--;
            }
            if(i<j){
                array[i] = array[j];
            }
            while (i<j && array[i] <= temp){
                i++;
            }
            if(i<j){
                array[j] = array[i];
            }
        }
        array[i] = temp;
        quickSort(array, left, i-1);
        quickSort(array, i+1, right);
    }
}
