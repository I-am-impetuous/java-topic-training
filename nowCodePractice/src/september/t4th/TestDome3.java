package september.t4th;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-04
 * Time: 14:42
 */
public class TestDome3 {
    //1.使用中心扩散法
    //2.以一个或者两个字符为中心，向两边进行扩散，直到不相等。
    public static int LCS(String str){
        int len = str.length();
        int sum = 1;
        for(int i=0; i<len-1; i++){
            int temp = Math.max(maxLength(str, i, i), maxLength(str, i, i+1));
            sum = Math.max(temp, sum);
        }
        return sum;
    }
    public static int maxLength(String str, int i, int j){
        while(i>=0 && j<str.length() && str.charAt(i) == str.charAt(j)){
            i--;
            j++;
        }
        return j-i+1-2;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        System.out.println(LCS(str));
    }
}
