package september.t17th;


/**
 * Created with IntelliJ IDEA
 * Description: 出栈顺序
 * User: 我很浮躁
 * Date: 2023-09-17
 * Time: 11:34
 */
public class TestDome2 {
    public static void main(String[] args) {
        String str = "abc";
        all_order(str, "", "");
    }

    //第一个参数为：要进行入栈的字符串
    //第二个参数为：栈中存放的字符串
    //第三个参数为：已经出栈的字符串
    private static void all_order(String str, String stack, String out) {
        //1.递归结束条件：当str为空和stack为空时，输出out
        //2.入栈，当str不为空时
        //3.出栈，当stack不为空时
        if(str.length() == 0 && stack.length() == 0){
            System.out.println(out);
        }
        if(str.length() != 0){
            all_order(str.substring(1), stack + str.charAt(0), out);
        }
        if(stack.length() != 0){
            all_order(str, stack.substring(0, stack.length()-1), out + stack.charAt(stack.length()-1));
        }
    }
}
