package september.t17th;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-17
 * Time: 10:46
 */
public class TestDome1 {
    static void all_order(String str, String stack, String out) {
        if (str.length() == 0 && stack.length() == 0) {
            System.out.println(out);
        }
        if (str.length() != 0) {
            all_order(str.substring(1), stack + str.charAt(0), out);
        }
        if (stack.length() != 0) {
            all_order(str, stack.substring(0, stack.length() - 1), out + stack.charAt(stack.length() - 1));
        }
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String str = input.nextLine();
        all_order(str, "", "");
    }
}
