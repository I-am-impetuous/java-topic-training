package september.t3th;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA
 * Description: 最小的K个数
 * User: 我很浮躁
 * Date: 2023-09-03
 * Time: 23:44
 */
public class TestDome8 {
    //1.小根堆，根节点为小根堆中的最小值
    public ArrayList<Integer> GetLeastNumbers_Solution (int[] input, int k) {
        // write code here
        ArrayList<Integer> list = new ArrayList<>();
        if(k==0){
            return list;
        }
        PriorityQueue<Integer> queue = new PriorityQueue<>(new Comparator<Integer>(){
            @Override
            public int compare(Integer o1, Integer o2){
                return o2 - o1;
            }
        });
        for(int i=0; i<input.length; i++){
            if(queue.size()<k){
                queue.offer(input[i]);
            }else{
                if(queue.peek() > input[i]){
                    queue.poll();
                    queue.offer(input[i]);
                }
            }
        }

        while(!queue.isEmpty()){
            list.add(queue.poll());
        }
        return list;
    }
    public static void main(String[] args) {
        PriorityQueue<Integer> queue = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return 0;
            }
        });
    }
}
