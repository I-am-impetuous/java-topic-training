package september.t3th;

/**
 * Created with IntelliJ IDEA
 * Description:买卖股票的最好时机(一)
 * User: 我很浮躁
 * Date: 2023-09-03
 * Time: 14:30
 */
public class TestDome2 {
    //1.初始状态：dp[i][0]表示不持股的状态，dp[i][1]表示持股的状态
    //2.状态方程，dp[i][0] = max(dp[i-1][0], dp[i-1][0]+prices)
    //不持股的状态，前一次不持股和持股把股票卖了的最大值
    //3.dp[i][1] = max(dp[i-1][1], dp[i-1][0]-prices)
    //持股状态，前一次持股和不持股买股票
    public int maxProfit (int[] prices) {
        // write code here
        int n = prices.length;
        int[][] dp = new int[n][2];
        dp[0][0] = 0;
        dp[0][1] = -prices[0];
        for(int i=1; i<n; i++){
            dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1]+prices[i]);
            dp[i][1] = Math.max(dp[i-1][1], -prices[i]);
        }
        return dp[n-1][0];
    }
}
