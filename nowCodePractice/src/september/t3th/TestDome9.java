package september.t3th;

import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA
 * Description: 寻找第K大
 * User: 我很浮躁
 * Date: 2023-09-03
 * Time: 23:59
 */
public class TestDome9 {
    //1.使用小根堆-优先队列
    public int findKth (int[] a, int n, int K) {
        // write code here
        PriorityQueue<Integer> queue = new PriorityQueue<>();
        for(int i=0; i<n; i++){
            if(queue.size() < K){
                queue.offer(a[i]);
            }else{
                if(queue.peek()<a[i]){
                    queue.poll();
                    queue.offer(a[i]);
                }
            }
        }

        return queue.peek();
    }
}
