package september.t3th;

/**
 * Created with IntelliJ IDEA
 * Description: 打家劫舍(一)
 * User: 我很浮躁
 * Date: 2023-09-03
 * Time: 19:31
 */
public class TestDome6 {
    //1.使用动态规划，
    //2.初始状态 dp[0] = 0; dp[1] = nums[0];
    //3.如果选择偷这一家，那么上一家就不能偷
    //dp[i] = max(dp[i-1], dp[i-2] + nums[i-1]);
    public int rob (int[] nums) {
        // write code here
        int len = nums.length;
        int[] dp = new int[len+1];
        dp[0] = 0;
        dp[1] = nums[0];
        for(int i=2; i<=len; i++){
            dp[i] = Math.max(dp[i-1], dp[i-2]+nums[i-1]);
        }
        return dp[len];
    }
}
