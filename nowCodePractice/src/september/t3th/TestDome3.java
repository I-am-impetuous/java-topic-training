package september.t3th;

/**
 * Created with IntelliJ IDEA
 * Description: 最长公共子序列(二)
 * User: 我很浮躁
 * Date: 2023-09-03
 * Time: 14:30
 */
public class TestDome3 {
    //1.判断特殊情况
    //2.定义两个数组，dp动态规划数组，flag存放方向位置的数组
    //3.dp[i][j] 中的值表示s1在下表 和 s2在j下表中的最大公共子序列
    //4.如果s1.charAt(i) == s2.chcharAt(j), dp[i][j] = dp[i-1][j-1] + 1；
    //5.如果s1.charAt(i) ！= s2.chcharAt(j)，dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
    //6.其中flag == 1，表示从左上方来的数据，flag == 2表示从左方来的，flag == 3表示从上方来的
    public String LCS (String s1, String s2) {
        // write code here
        String ret = "";
        int length1 = s1.length();
        int length2 = s2.length();
        if (length1 == 0 || length2 == 0) {
            return "-1";
        }
        int[][] dp = new int[length1 + 1][length2 + 1];
        int[][] flag = new int[length1 + 1][length2 + 1];
        for (int i = 1; i <= length1; i++) {
            for (int j = 1; j <= length2; j++) {
                if (s1.charAt(i-1) == s2.charAt(j-1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                    flag[i][j] = 1;
                } else {
                    if (dp[i - 1][j] > dp[i][j - 1]) {
                        dp[i][j] = dp[i - 1][j];
                        flag[i][j] = 2;
                    }else{
                        dp[i][j] = dp[i][j-1];
                        flag[i][j] = 3;
                    }
                }
            }
        }
        ret = strLCS(s1, s2, dp[length1][length2], flag);
        if(ret.length() == 0){
            return "-1";
        }
        return ret;
    }

    private String strLCS(String s1, String s2, int size, int[][] flag) {
        // TODO
        StringBuffer buffer = new StringBuffer();
        int i=s1.length();
        int j=s2.length();
        while(size != 0){
            if(flag[i][j] == 1){
                buffer.append(s1.charAt(i-1));
                i--;
                j--;
                size--;
            }else if(flag[i][j] == 2){
                i--;
            }else if(flag[i][j] == 3){
                j--;
            }
        }
        return buffer.reverse().toString();
    }
}
