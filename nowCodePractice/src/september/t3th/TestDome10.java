package september.t3th;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * Description: 二叉树的中序遍历（非递归）
 * User: 我很浮躁
 * Date: 2023-09-04
 * Time: 0:29
 */

class TreeNode{
    public int val;
    public TreeNode left;
    public TreeNode right;
}
public class TestDome10 {
    //1.只要左节点不为空，一值访问左节点的值，并且把值存放在栈中
    //然后出栈，收集这个节点的值
    //把root = node.next；
    //2.定义一个list放整数，定义一个栈存放节点
    //3.判断特殊情况
    public int[] inorderTraversal (TreeNode root) {
        // write code here
        ArrayList<Integer> list = new ArrayList<>();
        if(root == null){
            return new int[0];
        }
        Stack<TreeNode> stack = new Stack<>();
        while(root != null || !stack.empty()){
            while(root != null){
                stack.push(root);
                root = root.left;
            }
            TreeNode node = stack.pop();
            list.add(node.val);
            root = node.right;
        }
        int[] array = new int[list.size()];
        for(int i=0; i<list.size(); i++){
            array[i] = list.get(i);
        }
        return array;
    }
}
