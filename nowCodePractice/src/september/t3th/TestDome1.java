package september.t3th;

/**
 * Created with IntelliJ IDEA
 * Description: 买卖股票的最好时机(二)
 * User: 我很浮躁
 * Date: 2023-09-03
 * Time: 14:30
 */
public class TestDome1 {
    public int maxProfit (int[] prices) {
        // write code here
        int n = prices.length;
        int[][] dp = new int[n][2];
        dp[0][0] = 0;
        dp[0][1] = -prices[0];
        for(int i=1; i<n; i++){
            dp[i][0] = Math.max(dp[i-1][0], dp[i-1][1] + prices[i]);
            dp[i][1] = Math.max(dp[i-1][1], dp[i-1][0] - prices[i]);
        }
        return dp[n-1][0];
    }
}
