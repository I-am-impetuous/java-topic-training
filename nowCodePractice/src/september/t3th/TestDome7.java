package september.t3th;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description: 打家劫舍(二)
 * User: 我很浮躁
 * Date: 2023-09-03
 * Time: 19:32
 */
public class TestDome7 {
    //1.分两种情况来谈论，偷第一家不偷最后一家
    //2.不偷第一家，偷最后一家
    //3.比较两种情况的大小
    public int rob (int[] nums) {
        // write code here
        int len = nums.length;
        int[] dp = new int[len+1];
        //不偷最后一家
        dp[0] = 0;
        dp[1] = nums[0];
        for(int i=2; i<len; i++){
            dp[i] = Math.max(dp[i-1], dp[i-2] + nums[i-1]);
        }
        int temp = dp[len-1];
        //不偷第一家
        Arrays.fill(dp ,0);
        dp[0] = 0;
        dp[1] = 0;
        dp[2] = nums[1];
        for(int i=3; i<=len; i++){
            dp[i] = Math.max(dp[i-1], dp[i-2] + nums[i-1]);
        }
        return Math.max(temp, dp[len]);
    }
}
