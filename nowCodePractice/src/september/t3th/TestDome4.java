package september.t3th;

/**
 * Created with IntelliJ IDEA
 * Description: 最长公共子串
 * User: 我很浮躁
 * Date: 2023-09-03
 * Time: 14:31
 */
public class TestDome4 {
    //1.判断特殊情况
    //2.定义一个dp动态规划数组，dp[i][j] 表示当前连续相等字串的长度
    //3.定义两个变量，一个变量large记录字串的最大长度，一个变量index记录最长字串的末尾下表
    //4.如果两个字符串i和j下表的字符相等，dp[i][j] = dp[i-1][j-1] + 1;
    //5.如果不相等，dp[i][j] = 0；
    //6.根据index和large截取字符串
    public String LCS (String str1, String str2) {
        // write code here
        int length1 = str1.length();
        int length2 = str2.length();
        if(length1 == 0 || length2 == 0){
            return "";
        }
        int[][] dp = new int[length1+1][length2+1];
        int large = 0;
        int index = 0;
        for(int i=1; i<=length1; i++){
            for(int j=1; j<=length2; j++){
                if(str1.charAt(i-1) == str2.charAt(j-1)){
                    dp[i][j] = dp[i-1][j-1] + 1;
                    if(large < dp[i][j]){
                        large = dp[i][j];
                        index = i;
                    }
                }else{
                    dp[i][j] = 0;
                }
            }
        }
        String ret = str1.substring(index-large, index);
        return ret;
    }
}
