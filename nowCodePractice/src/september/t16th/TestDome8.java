package september.t16th;

import com.sun.org.apache.xerces.internal.impl.xs.SchemaNamespaceSupport;

import javax.swing.plaf.IconUIResource;
import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-16
 * Time: 20:36
 */
public class TestDome8 {
    public static void main0(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        int[] temp = new int[n];
        for(int i=0; i<n; i++) {
            arr[i] = sc.nextInt();
            int count = 0;
            while (arr[i] % Math.pow(2, count) == 0) {
                count++;
            }
            temp[i] = count;
        }
        Arrays.sort(temp);
        System.out.println(temp[n-2]);
    }

    public static void fun(int[] arr, int n) {
        int[] temp = new int[n];
        for(int i=0; i<n; i++) {
            int count = 0;
            while (arr[i] % Math.pow(2, count) == 0) {
                count++;
            }
            temp[i] = count;
        }
        Arrays.sort(temp);
        System.out.println(temp[n-2]);
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 20, 28};
        fun(arr, arr.length);
    }
}
