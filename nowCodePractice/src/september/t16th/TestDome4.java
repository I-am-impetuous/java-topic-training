package september.t16th;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-16
 * Time: 16:03
 */
public class TestDome4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        List<Integer> list = new ArrayList<>();
        Map<Integer, Integer> map = new HashMap<>();
        while (sc.hasNextInt()) { // 注意 while 处理多个 case
            list.add(sc.nextInt());
        }
        int left = 0;
        int right = 0;
        int count = 1;
        for(; right<list.size(); right++){
            if(!map.containsKey(list.get(right))){
                map.put(list.get(right), 1);
                count = Math.max(count, right-left+1);
            }else {
                map.put(list.get(right), map.get(list.get(right))+1);
            }
            while (left < right && map.get(list.get(right)) > 1){
                map.put(list.get(left), map.get(list.get(left)) -1);
                left++;
            }
        }
        System.out.println(count);
    }
}
