package september.t16th;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-16
 * Time: 20:10
 */
public class TestDome7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int day = sc.nextInt(); //天数
        int a = sc.nextInt(); //箱子有多少格子
        int b = sc.nextInt(); //每一个格子能放多少物品
        Map<String, Integer> map = new HashMap<>();
        String str1 = sc.nextLine();
        for(int i=0; i<day; i++){
            String str = sc.nextLine();
            String[] strings = str.split(" ");
            int num = Integer.parseInt(strings[1]);
            if(!map.containsKey(strings[0])){
                map.put(strings[0], num);
            }else {
                map.put(strings[0], map.get(strings[0]) + num);
            }
        }
        int count = 0; // 需要的格子数
        for (Map.Entry<String, Integer> entry : map.entrySet()){
            count = count + entry.getValue()/b;
            if(entry.getValue() % b != 0){
                count++;
            }
        }
        if(count%a == 0){
            count = count/a;
        }else {
            count = count/a+1;
        }
        System.out.println(count);
    }
}
