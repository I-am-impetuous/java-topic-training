package september.t16th;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-16
 * Time: 19:26
 */
public class TestDome6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        String s = sc.nextLine();
        int n = Integer.parseInt(s);
        for(int i=0; i<n; i++){
            String str = sc.nextLine();
            String[] strings = str.split("_");
            if(strings.length != 3){
                System.out.println("error");
                continue;
            }
            int sum = 1;
            //判断区域号
            if(!(strings[0].length() == 2 || strings[0].length() == 0)){
                sum = 0;
            }
            for(int j=0; j<strings[0].length(); j++){
                if(!(strings[0].charAt(j) > '0' && strings[0].charAt(j) <= '9')){
                    sum = 0;
                    break;
                }
            }
            //判断楼号
            if(strings[1].length() != 3 || strings[1].charAt(0) == '1'){
                sum = 0;
            }
            for(int j=0; j<strings[1].length(); j++){
                if(!(strings[1].charAt(j) > '0' && strings[1].charAt(j) <= '9')){
                    sum = 0;
                    break;
                }
            }
            //判断门牌号
            if(strings[2].length() != 3){
                sum = 0;
            }
            for(int j=0; j<strings[2].length(); j++){
                if(!(strings[2].charAt(j) > '0' && strings[2].charAt(j) <= '9')){
                    sum = 0;
                    break;
                }
            }
            System.out.println(sum);
        }
    }
}
