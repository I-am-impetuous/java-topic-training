package september.t16th;

/**
 * Created with IntelliJ IDEA
 * Description: 挤奶路径
 * User: 我很浮躁
 * Date: 2023-09-16
 * Time: 14:11
 */
public class TestDome1 {
    public int uniquePathsWithObstacles (int[][] cows) {
        // write code here
        int m = cows.length;
        int n = cows[0].length;
        int[][] dp = new int[m][n];
        dp[1][0] = 1;
        for(int i=0; i<m; i++){
            for(int j=0; j<n; j++){
                if(cows[i][j] == 0){
                    dp[i+1][j+1] = dp[i][j+1] + dp[i+1][j];
                }else {
                    dp[i+1][j+1] = 0;
                }
            }
        }
        return dp[m][n];
    }
}
