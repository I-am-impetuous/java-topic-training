package september.t16th;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-16
 * Time: 14:29
 */
public class TestDome3 {
    public static void main1(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int x = in.nextInt();
            int y = in.nextInt();
            int num = 8;
            for(int i=1; i<y; i++){
                int sum = num * x;
                num = sum % 10;
            }
            System.out.println(num);
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        int x = in.nextInt();
        int y = in.nextInt();
        int num = 8;
        for (int i = 1; i < y; i++) {
            int sum = num * x;
            num = sum % 10;
        }
        System.out.println(num);
    }
}
