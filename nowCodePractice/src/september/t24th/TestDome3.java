package september.t24th;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA
 * Description: 二叉树的下一个结点
 * User: 我很浮躁
 * Date: 2023-09-24
 * Time: 17:15
 */

class TreeLinkNode {
    int val;
    TreeLinkNode left = null;
    TreeLinkNode right = null;
    TreeLinkNode next = null;

    TreeLinkNode(int val) {
        this.val = val;
    }
}

public class TestDome3 {
    public ArrayList<TreeLinkNode> list = new ArrayList<>();
    public TreeLinkNode GetNext(TreeLinkNode pNode) {
        TreeLinkNode root = pNode;
        while(root.next != null){
            root = root.next;
        }
        inOrder(root);
        for(int i=0; i<list.size()-1; i++){
            if(list.get(i) == pNode){
                return list.get(i+1);
            }
        }
        return null;
    }

    private void inOrder(TreeLinkNode root) {
        // TODO
        if(root == null){
            return;
        }
        inOrder(root.left);
        list.add(root);
        inOrder(root.right);
    }
}
