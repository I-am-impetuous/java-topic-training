package september.t24th;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-24
 * Time: 16:40
 */
public class TestDome1 {
    public int duplicate (int[] numbers) {
        // write code here
        Map<Integer, Integer> map = new HashMap<>();
        for(int i=0; i<numbers.length; i++){
            if(map.containsKey(numbers[i])){
                return numbers[i];
            }else{
                map.put(numbers[i], 1);
            }
        }
        return -1;
    }
    public static void main(String[] args) {

    }
}
