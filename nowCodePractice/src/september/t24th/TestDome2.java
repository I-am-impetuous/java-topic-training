package september.t24th;

/**
 * Created with IntelliJ IDEA
 * Description: 替换空格
 * User: 我很浮躁
 * Date: 2023-09-24
 * Time: 16:49
 */
public class TestDome2 {
    public String replaceSpace (String s) {
        // write code here
        StringBuffer buffer = new StringBuffer();
        for(int i=0; i<s.length(); i++){
            if(s.charAt(i) == ' '){
                buffer.append("%20");
            }else{
                buffer.append(s.charAt(i));
            }
        }
        return buffer.toString();
    }
}
