package september.t21th;

/**
 * Created with IntelliJ IDEA
 * Description: 旋转数组
 * User: 我很浮躁
 * Date: 2023-09-21
 * Time: 16:26
 */
public class TestDome1 {
    public int[] solve (int n, int m, int[] a) {
        // write code here
        //1.旋转三次
        //第一次全部反转
        //第二次开头旋转
        //第三次末尾旋转
        m = m%n;
        arrayReserve(a, 0, n-1);
        arrayReserve(a, 0, m-1);
        arrayReserve(a, m, n-1);
        return a;
    }

    public void arrayReserve(int[] a, int start, int end){
        while(start < end){
            int temp = a[start];
            a[start] = a[end];
            a[end] = temp;
            start++;
            end--;
        }
    }
}
