package september.t21th;

/**
 * Created with IntelliJ IDEA
 * Description: 顺时针旋转矩阵
 * User: 我很浮躁
 * Date: 2023-09-21
 * Time: 17:02
 */
public class TestDome3 {
    public int[][] rotateMatrix (int[][] mat, int n) {
        // write code here
        int[][] arr = new int[n][n];
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                arr[i][j] = mat[n-1-j][i];
            }
        }
        return arr;
    }
}
