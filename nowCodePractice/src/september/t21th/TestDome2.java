package september.t21th;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA
 * Description: 螺旋矩阵
 * User: 我很浮躁
 * Date: 2023-09-21
 * Time: 16:44
 */
public class TestDome2 {
    public ArrayList<Integer> spiralOrder (int[][] matrix) {
        // write code here
        ArrayList<Integer> list = new ArrayList<>();
        if(matrix.length == 0){
            return list;
        }
        int left = 0;
        int right = matrix[0].length-1;
        int up = 0;
        int down = matrix.length-1;
        while(left <= right && up <= down){
            for(int i=left; i<=right; i++){
                list.add(matrix[up][i]);
            }
            up++;
            if(up > down){
                break;
            }
            for(int j=up; j<=down; j++){
                list.add(matrix[j][right]);
            }
            right--;
            if(right < left){
                break;
            }
            for(int i=right; i>=left; i--){
                list.add(matrix[down][i]);
            }
            down--;
            if(up > down){
                break;
            }
            for(int j=down; j>=up; j--){
                list.add(matrix[j][left]);
            }
            left++;
        }
        return list;
    }
}
