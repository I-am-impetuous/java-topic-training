package september.t1th;

/**
 * Created with IntelliJ IDEA
 * Description: 数组中的逆序对
 * User: 我很浮躁
 * Date: 2023-09-01
 * Time: 15:31
 */
public class TestDome2 {
    //先把归并排序写出来
    //1.递归的划分区间
    //2.合并有序的数组
    //3.设置一个全局变量
    public int ret = 0;
    public int InversePairs (int[] nums) {
        // write code here
        mergeSort(nums, 0, nums.length-1);
        return ret;
    }

    private void mergeSort(int[] nums, int left, int right) {
        // TODO
        if(left >= right){
            return;
        }
        int mid = (left + right)/2;
        mergeSort(nums, left, mid);
        mergeSort(nums, mid+1, right);
        merge(nums, left, mid, right);
    }

    private void merge(int[] nums, int left, int mid, int right) {
        // TODO
        int[] temp = new int[right-left+1];
        int k=0;
        int s1 = left;
        int e1 = mid;
        int s2 = mid+1;
        int e2 = right;
        while(s1 <= e1 && s2 <= e2){
            if(nums[s1] > nums[s2]){
                temp[k++] = nums[s2++];
                ret += (mid - s1 + 1);
                ret = ret%1000000007;
            }else{
                temp[k++] = nums[s1++];
            }
        }
        while(s1<=e1){
            temp[k++] = nums[s1++];
        }
        while(s2<=e2){
            temp[k++] = nums[s2++];
        }
        for(k=0, s1=left; k<temp.length; s1++, k++){
            nums[s1] = temp[k];
        }
    }
}
