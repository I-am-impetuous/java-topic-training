package september.t1th;

/**
 * Created with IntelliJ IDEA
 * Description: 旋转数组的最小数字
 * User: 我很浮躁
 * Date: 2023-09-01
 * Time: 17:25
 */
public class TestDome4 {
    //1.因为是升序数组，把旋转后的升序数组从中间分开
    //2.那么这两个数组肯定有一个是升序的
    //3，如果中间值小于右边界值，右边是升序的，在最左边
    //4，如果中间值大于右边界值，那么右边不是升序的，在右边
    //5，如果中间值等于有边界值，右边界收缩
    public int minNumberInRotateArray(int [] array) {
        int left = 0;
        int right = array.length-1;

        while(left < right){
            int mid = (left+right)/2;
            if(array[mid] < array[right]){
                right = mid;
            }else if(array[mid] == array[right]){
                right--;
            }else{
                left = mid + 1;
            }
        }
        return array[left];
    }
}
