package september.t1th;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description: 重建二叉树 （使用中序遍历和前序遍历）
 * User: 我很浮躁
 * Date: 2023-09-01
 * Time: 11:09
 */
class TreeNode{
    public TreeNode left;
    public TreeNode right;
    public int val;
    public TreeNode(int val){
        this.val = val;
    }
}
public class TestDome1 {
    //1.前序遍历结果中的第一个值是根节点
    //2.在中序遍历中找到根节点的下标，下表的左边是左子树中的值，右边是右子树的值
    //3.根据递归依次去遍历
    public TreeNode reConstructBinaryTree (int[] preOrder, int[] vinOrder) {
        // write code here
        if(preOrder == null || vinOrder == null || preOrder.length == 0 || vinOrder.length == 0){
            return null;
        }
        TreeNode root = new TreeNode(preOrder[0]);
        for(int i=0; i<vinOrder.length; i++){
            if(preOrder[0] == vinOrder[i]){
                root.left = reConstructBinaryTree(Arrays.copyOfRange(preOrder, 1, i+1), Arrays.copyOfRange(vinOrder, 0, i));
                root.right = reConstructBinaryTree(Arrays.copyOfRange(preOrder, i+1, preOrder.length), Arrays.copyOfRange(vinOrder, i+1, vinOrder.length));
                break;
            }
        }
        return root;
    }
}
