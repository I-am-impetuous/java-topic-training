package september.t12th;

import java.util.*;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-12
 * Time: 18:51
 */
public class TestDome7 {
    public static void main(String[] arge){
        Scanner sc = new Scanner(System.in);
        int day = sc.nextInt(); //总天数
        int accepted = sc.nextInt();//已接受任务数
        int finillyDay = sc.nextInt();//完成天数
        int count = sc.nextInt();//可以同时执行的任务数量
        int[] start = new int[accepted]; //任务开始时间
        int[] end = new int[accepted]; //任务结束时间
        for(int i=0; i<accepted; i++){
            start[i] = sc.nextInt();
        }
        for(int i=0; i<accepted; i++){
            end[i] = sc.nextInt();
        }
        int[] arr = new int[day+1];
        Arrays.fill(arr, 0);
        for(int i=0; i<accepted; i++){
            for(int j=start[i]; j<=end[i]; j++){
                arr[j] += 1;
            }
        }
        int left = 1;
        int right = 1;
        int sum = 0;
        for(int i=1; i<=day; i++){
            if(arr[i] < count){
                if(i == day){
                    sum += (right-left-finillyDay+2);
                }
                right++;
            }else{
                if(right-left >= finillyDay){
                    sum += (right-left-finillyDay+1);
                }
                right++;
                left = right;
            }
        }
        System.out.println(sum);
    }
}
