package september.t12th;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created with IntelliJ IDEA
 * Description: 合并区间
 * User: 我很浮躁
 * Date: 2023-09-12
 * Time: 14:16
 */


class Interval {
    int start;
    int end;

    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }
}


public class TestDome2 {
    public ArrayList<Interval> merge (ArrayList<Interval> intervals) {
        // write code here
        //1.排序，按照start排序
        //2.合并排序之后的数列
        //3.当前一个数组的end < 后一个数组start时，后一个数组直接加入
        //4.当前一个数组的end > 后一个数组start时，创建一个新的元素
        ArrayList<Interval> ret = new ArrayList<>();
        if (intervals.size() == 0) {
            return ret;
        }
        Collections.sort(intervals, new Comparator<Interval>() {
            @Override
            public int compare(Interval o1, Interval o2) {
                if (o1.start != o2.start) {
                    return o1.start - o2.start;
                } else {
                    return o1.end - o2.end;
                }
            }
        });
        int count = 0;
        ret.add(intervals.get(0));
        for (int i = 1; i < intervals.size(); i++) {
            Interval temp = intervals.get(i);
            Interval interval = ret.get(count);
            if (temp.start > interval.end) {
                ret.add(temp);
                count++;
            } else {
                Interval buffer = new Interval(interval.start, temp.end);
                if(temp.end < interval.end){
                    buffer.end = interval.end;
                }
                ret.remove(count);
                ret.add(buffer);
            }
        }
        return ret;
    }
}
