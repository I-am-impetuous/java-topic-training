package september.t12th;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description: 分糖果问题
 * User: 我很浮躁
 * Date: 2023-09-12
 * Time: 18:36
 */
public class TestDome6 {
    public int candy (int[] arr) {
        // write code here
        //1.从前向后遍历如果前一个数小于后一个数，就+1
        //2.从后向前遍历，如果后一个数比前一个数小，就+1
        int[] dp = new int[arr.length];
        Arrays.fill(dp, 1);
        for(int i=1; i<dp.length; i++){
            if(arr[i-1] < arr[i]){
                dp[i] = dp[i-1]+1;
            }
        }
        int sum = dp[arr.length-1];
        for(int i=dp.length-2; i>=0; i--){
            if(arr[i] > arr[i+1] && dp[i] <= dp[i+1]){
                dp[i] = dp[i+1]+1;
            }
            sum += dp[i];
        }
        return sum;
    }
}
