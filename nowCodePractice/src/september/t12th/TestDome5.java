package september.t12th;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description: 主持人调度（二）
 * User: 我很浮躁
 * Date: 2023-09-12
 * Time: 17:35
 */
public class TestDome5 {
    public int minmumNumberOfHost (int n, int[][] startEnd) {
        // write code here
        int[] end = new int[n];
        int[] start = new int[n];
        for(int i=0; i<n; i++){
            end[i] = startEnd[i][1];
            start[i] = startEnd[i][0];
        }
        Arrays.sort(end);
        Arrays.sort(start);
        int j=0;
        int ret = 0;
        for(int i=0; i<n; i++){
            if(start[i] >= end[j]){
                j++;
            }else{
                ret++;
            }
        }
        return ret;
    }
}
