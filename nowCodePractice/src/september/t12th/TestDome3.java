package september.t12th;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 * Description: 最小覆盖子串
 * User: 我很浮躁
 * Date: 2023-09-12
 * Time: 15:17
 */
public class TestDome3 {
    public String minWindow (String S, String T) {
        // write code here
        //1.使用Hash来把T中的字符全部放进去，并记录其中每个字符的次数
        //2.使用i和j来表示滑动窗口的下标
        //3.如果滑动窗口中已经包含了T的全部字符，就可以移动i来缩小滑动窗口
        HashMap<Character, Integer> ht = new HashMap<>();
        HashMap<Character, Integer> hs = new HashMap<>();
        int lenHS = 0;
        for (int i = 0; i < T.length(); i++) {
            ht.put(T.charAt(i), ht.getOrDefault(T.charAt(i), 0) + 1);
        }
        int lenHT = ht.size();
        int count = Integer.MAX_VALUE;
        String ret = "";
        for (int i = 0, j = 0; j < S.length(); j++) {
            hs.put(S.charAt(j), hs.getOrDefault(S.charAt(j), 0) + 1);
            if (ht.containsKey(S.charAt(j)) && ht.get(S.charAt(j)) == hs.get(S.charAt(j))) {
                lenHS++;
            }
            while (lenHS == lenHT && i <= j && (!ht.containsKey(S.charAt(i)) ||
                    ht.get(S.charAt(i)) < hs.get(S.charAt(i)))) {
                hs.put(S.charAt(i), hs.get(S.charAt(i)) - 1);
                i++;
            }
            if (lenHS == lenHT) {
                if (count > j - i + 1) {
                    count = j - i + 1;
                    ret = S.substring(i, j + 1);
                }
            }
        }
        return ret;
    }
}
