package september.t12th;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * Description:最长的括号子串
 * User: 我很浮躁
 * Date: 2023-09-12
 * Time: 10:42
 */
public class TestDome1 {
    //1.使用栈来存放左括号下标的位置
    //2.使用一个变量记录上一次结束的位置
    //3.当为左括号时，直接存放到栈中
    //3.当为右括号时，如果栈为空这是一次结束的位置
    //当找不为空，比较和最大字串的长度
    public int longestValidParentheses (String s) {
        // write code here
        int index = -1;
        int ret = 0;
        Stack<Integer> stack = new Stack<>();
        for(int i=0; i<s.length(); i++){
            if(s.charAt(i) == '('){
                stack.push(i);
            }else{
                if(stack.isEmpty()){
                    index = i;
                }else{
                    stack.pop();
                    if(stack.isEmpty()){
                        ret = Math.max(ret, i-index);
                    }else{
                        ret = Math.max(ret, i-stack.peek());
                    }
                }
            }
        }
        return ret;
    }
}
