package september.t12th;

/**
 * Created with IntelliJ IDEA
 * Description: 接雨水问题
 * User: 我很浮躁
 * Date: 2023-09-12
 * Time: 17:20
 */
public class TestDome4 {
    public long maxWater (int[] arr) {
        // write code here
        //1.使用双指针来记录左右的边界
        //2.较短的边移动，并记录移动这一步所能接的水
        if(arr == null || arr.length == 0){
            return 0;
        }
        int sum = 0;
        int left = 0;
        int right = arr.length-1;
        int maxL = 0;
        int maxR = 0;
        while(left < right){
            maxL = Math.max(maxL, arr[left]);
            maxR = Math.max(maxR, arr[right]);
            if(maxL < maxR){
                sum += maxL-arr[left++];
            }else{
                sum += maxR-arr[right--];
            }
        }
        return sum;
    }
}
