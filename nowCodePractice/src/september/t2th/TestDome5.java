package september.t2th;

/**
 * Created with IntelliJ IDEA
 * Description: 删除有序链表中重复的元素-II
 * User: 我很浮躁
 * Date: 2023-09-02
 * Time: 17:31
 */
public class TestDome5 {
    //1.使用双指针
    public ListNode deleteDuplicates (ListNode head) {
        // write code here
        if (head == null || head.next == null) {
            return head;
        }
        ListNode pre = new ListNode(-1);
        pre.next = head;
        ListNode cur = pre;
        ListNode curNext = pre.next;
        while (curNext != null && curNext.next != null) {
            if (curNext.val == curNext.next.val) {
                int temp = curNext.val;
                while (curNext != null &&
                        curNext.val == temp) {
                    cur.next = curNext.next;
                    curNext = cur.next;
                }
            } else {
                cur = curNext;
                curNext = curNext.next;
            }
        }
        return pre.next;
    }
}
