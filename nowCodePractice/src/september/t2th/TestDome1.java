package september.t2th;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created with IntelliJ IDEA
 * Description: 单链表的排序
 * User: 我很浮躁
 * Date: 2023-09-02
 * Time: 15:11
 */
class ListNode{
    public ListNode next;
    public int val;
    public ListNode(int val){
        this.val = val;
    }
}
public class TestDome1 {
    //1.把node节点的值放在数组中
    //2.对数组进行排序
    //3.重新建节点
    public ListNode sortInList (ListNode head) {
        // write code here
        ArrayList<Integer> arrayList = new ArrayList<>();
        ListNode cur = head;
        while(cur != null){
            arrayList.add(cur.val);
            cur = cur.next;
        }
        Collections.sort(arrayList);
        cur = head;
        for(int i=0; i<arrayList.size(); i++){
            cur.val = arrayList.get(i);
            cur = cur.next;
        }
        return head;
    }
}
