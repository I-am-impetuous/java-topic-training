package september.t2th;

/**
 * Created with IntelliJ IDEA
 * Description: 链表的奇偶重排
 * User: 我很浮躁
 * Date: 2023-09-02
 * Time: 16:01
 */
public class TestDome2 {
    //1.使用双指针，一个指针指向奇数位节点，一个指针指向偶数位节点
    public ListNode oddEvenList (ListNode head) {
        // write code here
        if(head == null || head.next == null){
            return head;
        }
        ListNode p1 = head;
        ListNode p2 = head.next;
        ListNode head2 = p2;
        while(p2 != null && p2.next != null){
            p1.next = p2.next;
            p1 = p1.next;
            p2.next = p1.next;
            p2 = p2.next;
        }
        p1.next = head2;
        return head;
    }
}
