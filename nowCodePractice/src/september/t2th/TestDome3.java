package september.t2th;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA
 * Description: 判断一个链表是否为回文结构
 * User: 我很浮躁
 * Date: 2023-09-02
 * Time: 16:02
 */
public class TestDome3 {
    //1.把数组放在数组中
    //2.通过数组来判断是否是回文
    public boolean isPail (ListNode head) {
        // write code here
        List<Integer> list = new ArrayList<>();
        ListNode cur = head;
        while(cur != null){
            list.add(cur.val);
            cur = cur.next;
        }
        int i=0;
        int j=list.size()-1;
        while(i < j){
            if(!(list.get(i).equals(list.get(j)))){
                return false;
            }
            j--;
            i++;
        }
        return true;
    }
}
