package september.t2th;

/**
 * Created with IntelliJ IDEA
 * Description: 删除有序链表中重复的元素-I
 * User: 我很浮躁
 * Date: 2023-09-02
 * Time: 16:23
 */
public class TestDome4 {
    //1.使用双指针，一个指针在前，一个指针在后，然后一个一个的判断
    public ListNode deleteDuplicates (ListNode head) {
        // write code here
        if(head == null || head.next == null){
            return head;
        }
        ListNode cur = head;
        ListNode curNext = head.next;
        while(curNext != null && cur != null){
            if(cur.val == curNext.val){
                cur.next = curNext.next;
                if(cur != null){
                    curNext = cur.next;
                }
            }else{
                cur = cur.next;
                curNext = curNext.next;
            }
        }
        return head;
    }
}
