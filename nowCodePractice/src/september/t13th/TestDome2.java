package september.t13th;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Description: 数据的查找
 * User: 陈子强
 * Date: 2023-09-13
 * Time: 18:41
 */
public class TestDome2 {
    public static Map<Integer, Integer> findNum(int[][] arr, int target){
        //1.从左下角开始
        //2.如果target大于这元素就往右走
        //3.如果target小于这元素就往上走
        if(arr == null){
            return null;
        }
        int col = arr.length;
        int row = arr[0].length;
        int i=col-1;
        int j=0;
        Map<Integer, Integer> map = new HashMap<>();
        while (i>=0 && j<row){
            if(arr[i][j] == target){
                map.put(i,j);
            }else if(arr[i][j] > target){
                i--;
            }else if(arr[i][j] < target){
                j++;
            }
        }
        return map;
    }
    public static void main(String[] args) {
        int[][] array = {  {1,   4,  7, 11, 15},
                {2,   5,  8, 12, 19},
                {3,   6,  9, 16, 22},
                {10, 13, 14, 17, 24},
                {18, 21, 23, 26, 30}};
        Map<Integer,Integer> map = findNum(array, 5);
        int count = map.size();
    }
}
