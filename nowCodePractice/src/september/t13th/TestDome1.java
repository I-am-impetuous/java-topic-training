package september.t13th;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-13
 * Time: 9:55
 */
public class TestDome1 {
    //1.使用hash来记录每一个字符出现的次数
    //如果出现的次数少于两次，则不能组成重复字串
    //如果出现了至少两次，则可能构成重复字串
    //2.找到两个字符出现的位置，判断是否构成重复字串
    public static int solve (String a) {
        // write code here
        int sum = 0;
        int n = a.length();
        if(a==null || n==0){
            return sum;
        }
        Map<Character, Integer> map = new HashMap<>();
        for(int i=0; i<n; i++){
            if(map.containsKey(a.charAt(i))){
                map.put(a.charAt(i), map.get(a.charAt(i)) + 1);
            }else{
                map.put(a.charAt(i), 1);
            }
        }
        for(int i=0; i<n; i++){
            int count = map.get(a.charAt(i));
            if(count >= 2){
                int j=i+1;
                int left = i;
                while(count != 1 && j<n){
                    if(a.charAt(i) == a.charAt(j)){
                        break;
                    }
                    j++;
                }
                int right = j;
                while(right<n && left<j && a.charAt(left) == a.charAt(right)){
                    left++;
                    right++;
                }
                if(left == j){
                    sum = Math.max(sum, (j-i)*2);
                }
            }
        }
        return sum;
    }

    public static void main(String[] args) {
        String str = "ababc";
        System.out.println(solve(str));
    }
}
