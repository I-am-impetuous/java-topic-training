package september.t22th;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-23
 * Time: 17:09
 */
public class TestDome3 {
    public static int minmumNumberOfHost (int n, int[][] startEnd) {
        // write code here
        int[] end = new int[n];
        int[] start = new int[n];
        for(int i=0; i<n; i++){
            end[i] = startEnd[i][1];
            start[i] = startEnd[i][0];
        }
        Arrays.sort(end);
        Arrays.sort(start);
        int j=0;
        int ret = 0;
        for(int i=0; i<n; i++){
            if(start[i] >= end[j]){
                j++;
            }else{
                ret++;
            }
        }
        return ret;
    }
    public static void main(String[] args) {
        int[][] startEnd = new int[3][2];
        startEnd[0][0] = 1;
        startEnd[1][0] = 2;
        startEnd[2][0] = 3;
        startEnd[0][1] = 2;
        startEnd[1][1] = 4;
        startEnd[2][1] = 5;
        minmumNumberOfHost(3, startEnd);
    }
}
