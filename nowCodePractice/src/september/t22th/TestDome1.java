package september.t22th;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-22
 * Time: 19:00
 */
public class TestDome1 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        //接收数据
        int n = in.nextInt();  //卡片的数量
        ArrayList<Integer> list = new ArrayList<>(); //存放元素的数组
        for (int i = 0; i < n; i++) {
            list.add(in.nextInt());
        }
        int count = in.nextInt();  //分裂的次数
        //当卡片的大小为1时直接返回
        if (n == 0) {
            System.out.println(0);
            return;
        }
        int maxNum = 0;  //最大的数字
        int second = 0;
        if (n == 1) {
            if (list.get(0) % (count + 1) == 0) {
                System.out.println(list.get(0) / (count + 1));
            } else {
                System.out.println(list.get(0) / (count + 1) + 1);
            }
            return;
        } else {
            while (count != 0) {
                Collections.sort(list);
                int m = list.size();
                maxNum = list.get(m-1);
                second = list.get(m-2);
                if ((float)maxNum / (float)(count + 1) < second) {
                    list.remove(m-1);
                    if (maxNum % 2 == 0) {
                        list.add(maxNum / 2);
                        list.add(maxNum / 2);
                    } else {
                        list.add(maxNum / 2);
                        list.add(maxNum / 2 + 1);
                    }
                } else {
                    if (maxNum % (count + 1) == 0) {
                        System.out.println(maxNum / (count + 1));
                    } else {
                        System.out.println(maxNum / (count + 1) + 1);
                    }
                    return;
                }
                count--;
            }
            Collections.sort(list);
            System.out.println(list.get(list.size()-1));
        }
    }
}
