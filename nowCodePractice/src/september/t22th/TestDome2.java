package september.t22th;

import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-09-22
 * Time: 20:08
 */
public class TestDome2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        ArrayList<Integer> list = new ArrayList<>();
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            list.add(in.nextInt());
        }
        int n = list.size();
        int[][] dp = new int[n][5];
        Arrays.fill(dp[0], -100);
        dp[0][0] = 0;
        dp[1][0] = -list.get(0);
        for(int i=1; i<n; i++){
            dp[i][0] = 0;
            dp[i][1] = Math.max(dp[i-1][1], dp[i-1][0]-list.get(i));//第一次买入
            dp[i][2] = Math.max(dp[i-1][2], dp[i-1][1]+list.get(i));//第一次卖出
            dp[i][3] = Math.max(dp[i-1][3], dp[i-1][2]-list.get(i));//第二次买入
            dp[i][4] = Math.max(dp[i-1][4], dp[i-1][3]+list.get(i));//第二次卖出
        }
        System.out.println(Math.max(dp[n-1][2], dp[n-1][4]));
    }
}
