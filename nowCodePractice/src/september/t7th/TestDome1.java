package september.t7th;

/**
 * Created with IntelliJ IDEA
 * Description: 反转字符串
 * User: 我很浮躁
 * Date: 2023-09-10
 * Time: 20:34
 */
public class TestDome1 {
    public String solve (String str) {
        // write code here
        char[] buffer = str.toCharArray();
        int left = 0;
        int right = str.length()-1;
        while(left < right){
            char c = buffer[left];
            buffer[left] = buffer[right];
            buffer[right] = c;
            left++;
            right--;
        }
        return new String(buffer);
    }
}
