package september.t11th;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA
 * Description: 数字字符串转化成IP地址
 * User: 我很浮躁
 * Date: 2023-09-11
 * Time: 18:09
 */
public class TestDome4 {
    public ArrayList<String> restoreIpAddresses (String s) {
        // write code here
        //1.使用枚举法，把每一种情况都列举出来
        //2.然后判断
        //数字是否大于了255
        //是否有0前缀
        ArrayList<String> ret = new ArrayList<>();
        int n = s.length();
        for (int i = 1; i < 4 && i < n - 2; i++) {
            for (int j = i + 1; j < i + 4 && j < n - 1; j++) {
                for (int k = j + 1; k < j + 4 && k < n; k++) {
                    if (n - k >= 4) {
                        continue;
                    }
                    String a = s.substring(0, i);
                    String b = s.substring(i, j);
                    String c = s.substring(j, k);
                    String d = s.substring(k);
                    if (Integer.parseInt(a) > 255 || Integer.parseInt(b) > 255 ||
                            Integer.parseInt(c) > 255 || Integer.parseInt(d) > 255) {
                        continue;
                    }
                    if ((a.length() != 1 && a.charAt(0) == '0') ||
                            (b.length() != 1 && b.charAt(0) == '0') ||
                            (c.length() != 1 && c.charAt(0) == '0') ||
                            (d.length() != 1 && d.charAt(0) == '0')) {
                        continue;
                    }
                    String temp = a + "." + b + "." + c + "." + d;
                    ret.add(temp);
                }
            }
        }
        return ret;
    }
}
