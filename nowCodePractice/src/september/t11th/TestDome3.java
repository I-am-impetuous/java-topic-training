package september.t11th;

import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA
 * Description: 滑动窗口的最大值
 * User: 我很浮躁
 * Date: 2023-09-11
 * Time: 17:13
 */
public class TestDome3 {
    public ArrayList<Integer> maxInWindows (int[] num, int size) {
        // write code here
        //1.维护一个双向列表，保持里面的数组是递减的
        //2.先加入第一个窗口的值
        //如果队尾的元素大于要加入的元素，直接在队尾添加元素
        //如果队尾的元素小于要加入的元素，则队尾元素pop，直到小于要添加的元素
        //3.将队首元素添加到结果中
        //4.然后窗口向后移动
        //判断前一个值是否和最大值相等，如果相等则最大值从队首pop出去
        //如果不相等则执行上面的第二个步骤
        ArrayList<Integer> ret = new ArrayList<>();
        if(size <= num.length && size != 0){
            ArrayDeque<Integer> dq = new ArrayDeque<>();
            int i=0;
            for(; i<size; i++){
                while(!dq.isEmpty() && num[dq.peekLast()] < num[i]){
                    dq.pollLast();
                }
                dq.add(i);
            }
            for(i=size; i<num.length; i++){
                ret.add(num[dq.peekFirst()]);
                while(!dq.isEmpty() && num[dq.peekLast()] < num[i]){
                    dq.pollLast();
                }
                while(!dq.isEmpty() && dq.peekFirst() < (i-size+1)){
                    dq.pollFirst();
                }
                dq.add(i);
            }
            ret.add(num[dq.pollFirst()]);
        }
        return ret;
    }
}
