package september.t11th;

/**
 * Created with IntelliJ IDEA
 * Description: 比较两个版本号的大小
 * User: 我很浮躁
 * Date: 2023-09-11
 * Time: 14:56
 */
public class TestDome1 {
    public int compare (String version1, String version2) {
        // write code here
        //1.使用两个指针，分别指向这两个字符串的下表
        //2.以点为分界线，然后把他化为数字
        //3.判断比较两个数字的大小
        int len1 = version1.length();
        int len2 = version2.length();
        int i=0;
        int j=0;
        while(i<len1 || j<len2){
            int sum1 = 0;
            while(i<len1 && version1.charAt(i) != '.'){
                sum1 = sum1*10 + (version1.charAt(i) - '0');
                i++;
            }
            i++;
            int sum2 = 0;
            while(j<len2 && version2.charAt(j) != '.'){
                sum2 = sum2*10 + (version2.charAt(j) - '0');
                j++;
            }
            j++;
            if(sum1 > sum2){
                return 1;
            }else if(sum2 > sum1){
                return -1;
            }
        }
        return 0;
    }
}
