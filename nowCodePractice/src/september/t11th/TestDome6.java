package september.t11th;

/**
 * Created with IntelliJ IDEA
 * Description: 正则表达式匹配
 * User: 我很浮躁
 * Date: 2023-09-11
 * Time: 21:12
 */
public class TestDome6 {
    public boolean match (String str, String pattern) {
        // write code here
        //1.使用动态规划
        //2.初始状态
        //dp[0][0] = true;
        //3.动态变化
        //当没有碰见 * 时，当条件满足 str[i-1] == pattern[j-1] 或者是 pattern[j-1] == '.'，dp[i][j] = dp[i-1][j-1];
        //当碰到了 * 时，表示出现一次 str[i-1] == pattern[j-2] 或者是 pattern[j-2] == '.', dp[i][j] = dp[i-1][j];
        //表示不出现，dp[i][j] = dp[i-1][j]
        int len1 = str.length();
        int len2 = pattern.length();
        boolean[][] dp = new boolean[len1 + 1][len2 + 1];
        dp[0][0] = true;
        for (int i = 0; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (pattern.charAt(j - 1) != '*') {
                    if (i > 0 && (pattern.charAt(j - 1) == str.charAt(i - 1) ||
                            pattern.charAt(j - 1) == '.')) {
                        dp[i][j] = dp[i - 1][j - 1];
                    }
                } else {
                    if (j >= 2) {
                        dp[i][j] |= dp[i][j - 2];
                    }
                    if (i >= 1 && j >= 2 && (pattern.charAt(j - 2) == str.charAt(i - 1) ||
                            pattern.charAt(j - 2) == '.')) {
                        dp[i][j] |= dp[i - 1][j];
                    }
                }
            }
        }
        return dp[len1][len2];
    }
}
