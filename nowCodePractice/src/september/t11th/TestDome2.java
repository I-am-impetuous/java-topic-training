package september.t11th;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA
 * Description: 数据流中的中位数
 * User: 我很浮躁
 * Date: 2023-09-11
 * Time: 15:15
 */
public class TestDome2 {
    //1.使用插入排序，每一次插入一个数据就进行排序
    //2.使用数组来进行保存，排序
    //3.直接通过下标就可以直接访问数据了
    private ArrayList<Integer> array = new ArrayList<>();
    public void Insert(Integer num) {
        if (array.isEmpty()) {
            array.add(num);
        } else {
            int i = 0;
            for (; i < array.size(); i++) {
                if (num <= array.get(i)) {
                    break;
                }
            }
            array.add(i, num);
        }
    }

    public Double GetMedian() {
        double num = 0;
        int len = array.size();
        if (len % 2 == 0) {
            num = (double)(array.get(len / 2) + array.get(len / 2 - 1)) / (double)2;
            return num;
        } else {
            num = (double)(array.get(len / 2));
            return num;
        }
    }

}
