package september.t11th;

/**
 * Created with IntelliJ IDEA
 * Description: 编辑距离(一)
 * User: 我很浮躁
 * Date: 2023-09-11
 * Time: 20:01
 */
public class TestDome5 {
    public int editDistance (String str1, String str2) {
        // write code here
        //1.使用动态规划
        //2.初始状态，当str1为空时，str2每增加一个字符，str1就要多一步增加操作
        //当str2为空时，str1每增加一个字符，str1就要多一步删除操作
        //3.变化状态，当前str1和str2相同时，dp[i][j] = dp[i-1][j-1]
        //当str1和str2不相同时，dp[i][j] = min(dp[i-1][j-1], min(dp[i][j-1], dp[i-1][j]))
        int len1 = str1.length();
        int len2 = str2.length();
        int[][] dp = new int[len1+1][len2+1];
        for(int i=0; i<=len1; i++){
            dp[i][0] = i;
        }
        for(int i=0; i<=len2; i++){
            dp[0][i] = i;
        }
        for(int i=1; i<=len1; i++){
            for(int j=1; j<=len2; j++){
                if(str1.charAt(i-1) == str2.charAt(j-1)){
                    dp[i][j] = dp[i-1][j-1];
                }else{
                    dp[i][j] = Math.min(dp[i-1][j-1], Math.min(dp[i-1][j], dp[i][j-1]))+1;
                }
            }
        }
        return dp[len1][len2];
    }
}
