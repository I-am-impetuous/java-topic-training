package August.t30th;

/**
 * Created with IntelliJ IDEA
 * Description: 寻找峰值
 * User: 我很浮躁
 * Date: 2023-08-30
 * Time: 20:31
 */
public class TestDome5 {
    public int findPeakElement (int[] nums) {
        // write code here
        //1.取中间下表的值，入如中间下标的值，大于右边的值，那么右边一定有峰值，
        //2.如果中间下表的值，小于右边的值，那么右边的值不一定有峰值
        int left = 0;
        int right = nums.length-1;
        while(left < right){
            int mid = (left+right)/2;
            if(nums[mid] > nums[mid+1]){
                right = mid;
            }else{
                left = mid + 1;
            }
        }
        return left;
    }
}
