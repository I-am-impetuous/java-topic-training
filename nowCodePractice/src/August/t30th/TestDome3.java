package August.t30th;

/**
 * Created with IntelliJ IDEA
 * Description: 判断是不是平衡二叉树
 * User: 我很浮躁
 * Date: 2023-08-30
 * Time: 17:51
 */
public class TestDome3 {
    //1.根据求深度的代码改编成判断平衡二叉树
    //2.然后优化平衡二叉树的代码
    public boolean IsBalanced_Solution (TreeNode pRoot) {
        // write code here
        int count = depth(pRoot);
        if(count==-1){
            return false;
        }
        return true;
    }

    private int depth(TreeNode root) {
        // TODO
        if(root == null){
            return 0;
        }
        int left = depth(root.left);
        if(left == -1){
            return -1;
        }
        int right = depth(root.right);
        if(right == -1){
            return -1;
        }
        if(Math.abs(left-right)>1){
            return -1;
        }
        return Math.max(left, right)+1;
    }
}
