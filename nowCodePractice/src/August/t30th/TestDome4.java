package August.t30th;

/**
 * Created with IntelliJ IDEA
 * Description: 在二叉树中找到两个节点的最近公共祖先
 * User: 我很浮躁
 * Date: 2023-08-30
 * Time: 20:04
 */
public class TestDome4 {
    //1.如果 root.val 的值等于 o1 和 o2 那么最近公共祖先就是 root
    //2.如果 不等于，就在左子树或者右子树中
    //3.如果这个root的左子树有一个数相等，右子树中也有一个数相等，那么公共祖先就是 root
    public int lowestCommonAncestor (TreeNode root, int o1, int o2) {
        // write code here
        if(root == null){
            return -1;
        }
        if(root.val == o1 || root.val == o2){
            return root.val;
        }
        int left = lowestCommonAncestor(root.left, o1, o2);
        int right = lowestCommonAncestor(root.right, o1, o2);
        if(left == -1){
            return right;
        }
        if(right == -1){
            return left;
        }
        return root.val;
    }
}
