package August.t30th;

/**
 * Created with IntelliJ IDEA
 * Description: 判断是不是二叉搜索树
 * User: 我很浮躁
 * Date: 2023-08-30
 * Time: 17:19
 */
public class TestDome2 {
    //1.设置一个pre
    //2.使用中序遍历来判断输出的数字是有序，如果没有序就直接返回false
    //3.使用递归遍历
    public int pre = Integer.MIN_VALUE;
    public boolean isValidBST (TreeNode root) {
        // write code here
        if(root == null){
            return true;
        }
        if(!isValidBST(root.left)){
            return false;
        }
        if(root.val < pre){
            return false;
        }
        pre = root.val;
        return isValidBST(root.right);
    }
}
