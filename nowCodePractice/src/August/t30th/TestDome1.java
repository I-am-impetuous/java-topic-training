package August.t30th;

/**
 * Created with IntelliJ IDEA
 * Description: 二叉搜索树与双向链表
 * User: 我很浮躁
 * Date: 2023-08-30
 * Time: 15:08
 */
class TreeNode{
    public TreeNode left;
    public TreeNode right;
    public int val;
}

public class TestDome1 {
    //1。由于是搜索书，所以最左端的数，一定是最小的，最右端的数一定是最大的
    //通过 中序遍历就可以得到一个由小到大的数列
    //2.设置两个变量一个是 head（头节点），一个是 pre（上一个节点），先使用递归找到最小的数字，
    //如果是第一个节点node，head=node, pre=node
    //如果不是第一个节点node, pre.right=root, root.left=pre, pre=root。
    public TreeNode head = null;
    public TreeNode pre = null;
    public TreeNode Convert(TreeNode pRootOfTree) {
        if(pRootOfTree == null){
            return null;
        }
        Convert(pRootOfTree.left);
        if(pre == null){
            pre = pRootOfTree;
            head = pRootOfTree;
        }else{
            pre.right = pRootOfTree;
            pRootOfTree.left = pre;
            pre = pRootOfTree;
        }
        Convert(pRootOfTree.right);
        return head;
    }
}
