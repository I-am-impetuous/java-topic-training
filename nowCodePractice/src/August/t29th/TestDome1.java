package August.t29th;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created with IntelliJ IDEA
 * Description: 按之字形顺序打印二叉树
 * User: 我很浮躁
 * Date: 2023-08-29
 * Time: 9:35
 */
class TreeNode{
    public TreeNode left;
    public TreeNode right;
    public int val;
}

public class TestDome1 {
    public ArrayList<ArrayList<Integer>> Print (TreeNode pRoot) {
        // write code here
        //1.和写层序遍历差不了太多
        //2.使用时队列，存放节点
        //3.主要时判断每一层有多少个节点
        //4.当是偶数层时，反转数组，奇数层列表不变，使用一个flag标志位
        ArrayList<ArrayList<Integer>> ret = new ArrayList<>();
        if (pRoot == null) {
            return ret;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(pRoot);
        TreeNode temp = null;
        boolean flag = false;
        while (!queue.isEmpty()) {
            int length = queue.size();
            ArrayList<Integer> array = new ArrayList<>();
            for (int i = 0; i < length; i++) {
                temp = queue.poll();
                array.add(temp.val);
                if (temp.left != null) {
                    queue.offer(temp.left);
                }
                if (temp.right != null) {
                    queue.offer(temp.right);
                }
            }
            if(flag){
                Collections.reverse(array);
            }
            flag = !flag;
            ret.add(array);
        }
        return ret;
    }
}
