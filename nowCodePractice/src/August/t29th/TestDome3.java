package August.t29th;

/**
 * Created with IntelliJ IDEA
 * Description: 二叉树中和为某一值的路径(一)
 * User: 我很浮躁
 * Date: 2023-08-29
 * Time: 14:27
 */
public class TestDome3 {
    //1.终止条件为：节点为空，返回false；sum的值等于零并且为叶子节点，返回true
    //2.递归左右子树，判断是否有true；
    public boolean hasPathSum (TreeNode root, int sum) {
        // write code here
        if(root == null){
            return false;
        }
        if(root.left == null && root.right == null && sum - root.val == 0){
            return true;
        }
        return hasPathSum(root.left, sum-root.val) || hasPathSum(root.right, sum-root.val);
    }
}
