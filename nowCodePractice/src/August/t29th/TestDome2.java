package August.t29th;

/**
 * Created with IntelliJ IDEA
 * Description: 求二叉树的最大深度
 * User: 我很浮躁
 * Date: 2023-08-29
 * Time: 14:12
 */
public class TestDome2 {
    //1.判断条件为：如果节点等于空，返回0；
    //2.递归调用这个，返回左子树和右子树中的最大长度
    public int maxDepth (TreeNode root) {
        // write code here
        if(root == null){
            return 0;
        }
        return Math.max(maxDepth(root.left), maxDepth(root.right))+1;
    }
}
