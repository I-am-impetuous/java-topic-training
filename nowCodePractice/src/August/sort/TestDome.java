package August.sort;

/**
 * Created with IntelliJ IDEA
 * Description: 7中排序
 * User: 我很浮躁
 * Date: 2023-08-28
 * Time: 15:32
 */
public class TestDome {
    //=================================  直接插入排序  ================================
    //1.从第二个数开始，和前面已经排好序的数列进行比较，然后移位
    //2.升序 --- 当这个数大于数列中的数时停止，把这个数插入数列中
    public static int[] insertSort(int[] arr){
        for(int i=1; i<arr.length; i++){
            int target = arr[i];
            for(int j=i-1; j>=0; j--){
                if(target > arr[j]){
                    arr[j+1] = target;
                    break;
                }
                arr[j+1] = arr[j];
            }
        }
        return arr;
    }

    //=================================   希尔排序   =================================
    //1.把数组分成n组，每一组数每隔n取一次
    //2.把每一次进行排序，插入排序
    //3.把n变小，重复上面两个步骤，直到n==1。
    public static int[] shellSort(int[] arr){
        int n = arr.length;
        while(true){  //直到 n==1 才结束循环
            n = n/2;
            for(int i=0; i<n; i++){  //把n组数据，都进行插入排序。
                for(int j=i+n; j<arr.length; j+=n){ //一次插入排序
                    int target = arr[j];
                    for(int k=j-n; k>=0-n; k-=n){
                        if(k<0 || target > arr[k]){
                            arr[k+n] = target;
                            break;
                        }
                        arr[k+n] = arr[k];
                    }
                }
            }
            if(n == 1){
                break;
            }
        }
        return arr;
    }

    //=================================   选择排序   =================================
    //1.分成两块，一块是排好序的，一块是未排序的
    //2.在没有排好序的数据中找到最小的数据，然后交换数据
    public static int[] selectSort(int[] arr){
        for(int i=0; i<arr.length; i++){
            int index = i;
            for(int j=i+1; j<arr.length; j++){
                if(arr[index] > arr[j]){
                    index = j;
                }
            }
            //交换
            int temp = arr[index];
            arr[index] = arr[i];
            arr[i] = temp;
        }
        return arr;
    }

    //=================================    堆排序    =================================
    //1.建大根堆，那么根元素的值，一定是最大的
    //2.根元素的值和最后一个节点交换，然后从0节点开始调整堆
    public static int[] headSort(int[] arr){
        int length = arr.length;
        //建一个大根堆
        for(int i=length/2-1; i>=0; i--){
            buildMaxHeap(arr, i, length);
        }
        //交换位置，调整堆
        for(int i=length-1; i>=0; i--){
            int temp = arr[i];
            arr[i] = arr[0];
            arr[0] = temp;
            buildMaxHeap(arr, 0, i);
        }
        return arr;
    }

    //arr 为数组， index 要调整的节点， length 有多少个节点
    //判断是否要调整子树, 建堆函数，调整一个节点
    public static void buildMaxHeap(int[] arr, int index, int length){
        for(int i=2*index+1; i<length; i = 2*i+1){
            if(i+1 < length && arr[i] < arr[i+1]){
                i++;
            }
            if(arr[index] < arr[i]){
                int temp = arr[index];
                arr[index] = arr[i];
                arr[i] = temp;
                index = i;
            }
        }
    }

    //=================================   冒泡排序   =================================
    //依次比较相邻的两个数，如果数字的顺序不对就交换位置
    public static int[] bubbleSort(int[] arr){
        for(int i=0; i<arr.length; i++){
            for(int j=1; j<arr.length-i; j++){
                if(arr[j-1] > arr[j]) {
                    int temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }

    //=================================   快速排序   =================================
    //1.找到一个基准，然后把大于这个基准的数都放在右边，把小于这个基准的放在左边
    //2.然后再根据已经分成两部分的这个数，再进行基准的设定，然后再分开
    public static void quickSort(int[] arr, int left, int right){
        //左边第一个元素为基准
        if(left > right){
            return;
        }
        int i=left;
        int j=right;
        int temp = arr[left];
        while(i < j){
            while (i<j && arr[j] >= temp){
                j--;
            }
            if(i<j){
                arr[i] = arr[j];
            }
            while (i<j && arr[i] < temp){
                i++;
            }
            if(i<j){
                arr[j] = arr[i];
            }
        }
        arr[i] = temp;
        quickSort(arr, left, i-1);
        quickSort(arr, i+1, right);
    }

    //=================================   归并排序   =================================
    //1.把数组中的所有数都分解
    //2.合并所有的数
    public static void mergerSort(int[] arr ,int left, int right){
        if(right <= left){
            return;
        }
        int mid = (left+right)/2;
        mergerSort(arr, left, mid);
        mergerSort(arr, mid+1, right);
        merger(arr, left, right, mid);
    }

    //合并数组
    private static void merger(int[] arr, int left, int right, int mid) {
        int s1 = left;
        int s2 = mid+1;
        int[] temp = new int[right-left+1];
        int k=0;
        while(s1 <= mid && s2 <= right){
            if(arr[s1] >= arr[s2]) {
                temp[k++] = arr[s2++];
            }else{
                temp[k++] = arr[s1++];
            }
        }
        while (s1<=mid){
            temp[k++] = arr[s1++];
        }
        while (s2<=right){
            temp[k++] = arr[s2++];
        }
        for(int i=0; i<temp.length; i++){
            arr[left+i] = temp[i];
        }
    }


    //=================================   验证排序是否准确   =================================
    public static void main(String[] args) {  //5处
        //插入排序
        int[] numInsert = {1,54,6,3,78,34,12,45,56,100};
        numInsert = insertSort(numInsert);
        for (int num: numInsert) {
            System.out.print(num + " ");
        }
        System.out.println();

        //希尔排序
        int[] numShell = {1,54,6,3,78,34,12,45,56,100};
        numShell = shellSort(numShell);
        for (int num: numShell) {
            System.out.print(num + " ");
        }
        System.out.println();

        //选择排序
        int[] numSelect = {1,54,6,3,78,34,12,45,56,100};
        numSelect = selectSort(numSelect);
        for (int num: numSelect) {
            System.out.print(num + " ");
        }
        System.out.println();

        //堆排序
        int[] numHeap = {1,54,6,3,78,34,12,45,56,100};
        numHeap = headSort(numHeap);
        for (int num: numHeap) {
            System.out.print(num + " ");
        }
        System.out.println();

        //冒泡排序
        int[] numBubble = {1,54,6,3,78,34,12,45,56,100};
        numBubble = bubbleSort(numBubble);
        for (int num: numBubble) {
            System.out.print(num + " ");
        }
        System.out.println();

        //快速排序
        int[] numQuick = {1,54,6,3,78,34,12,45,56,100};
        quickSort(numQuick, 0, numQuick.length-1);
        for (int num: numQuick) {
            System.out.print(num + " ");
        }
        System.out.println();

        //归并排序
        int[] numMerge = {1,54,6,3,78,34,12,45,56,100};
        mergerSort(numMerge, 0, numMerge.length-1);
        for (int num: numMerge) {
            System.out.print(num + " ");
        }
    }
}
