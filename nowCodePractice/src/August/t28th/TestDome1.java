package August.t28th;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description: 三数之和
 * User: 我很浮躁
 * Date: 2023-08-28
 * Time: 10:23
 */
public class TestDome1 {
    public ArrayList<ArrayList<Integer>> threeSum (int[] num) {
        // write code here
        ArrayList<ArrayList<Integer>> ret = new ArrayList<>();
        //1.考虑特殊情况，如果数组的长度小于3，直接返回空数组
        if(num.length < 3){
            return ret;
        }
        //2.因为结果要从小到大进行排序，所以要对数组进行排序
        Arrays.sort(num);
        //3.先找到组成元素的最小值，然后从后面的数组中找到剩余两个数
        //4.数组是排序的，所以使用双指针，来进行判断。如果相等就添加上去
        //5.考虑去重的情况
        for(int i=0; i<num.length; i++){
            if(i != 0 && num[i] == num[i-1]){
                continue;
            }
            int left = i+1;
            int right = num.length-1;
            int target = -num[i];
            while(left < right){
                if(num[left] + num[right] == target){
                    ArrayList<Integer> arrayList = new ArrayList<>();
                    arrayList.add(num[i]);
                    arrayList.add(num[left]);
                    arrayList.add(num[right]);
                    ret.add(arrayList);
                    while(left+1<right && num[left+1]==num[left]){
                        left++;
                    }
                    while(right-1>left && num[right] == num[right-1]){
                        right--;
                    }
                    left++;
                    right--;
                }else if(num[left] + num[right] < target){
                    left++;
                }else if(num[left] + num[right] > target){
                    right--;
                }
            }
        }
        return ret;
    }
}
