package August.t28th;

/**
 * Created with IntelliJ IDEA
 * Description: 盛水最多的容器
 * User: 我很浮躁
 * Date: 2023-08-28
 * Time: 14:29
 */
public class TestDome3 {
    public int maxArea (int[] height) {
        // write code here
        //1.排除特殊情况，数组长度小于2
        if(height.length < 2){
            return 0;
        }
        //2.使用贪心算法，双指针从两边开始，每次让较短的边进行移动
        //3.记录每次移动的数字
        int left = 0;
        int right = height.length - 1;
        int area = 0;
        while(left < right){
            int length = right-left;
            int h = Math.min(height[left], height[right]);
            area = Math.max(area, length*h);
            if(height[left]>height[right]){
                right--;
            }else{
                left++;
            }
        }
        return area;
    }
}
