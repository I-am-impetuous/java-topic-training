package August.t28th;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 * Description: 最长无重复子数组（子数组是数组中连续的数）
 * User: 我很浮躁
 * Date: 2023-08-28
 * Time: 11:17
 */
public class TestDome2 {
    public int maxLength (int[] arr) {
        // write code here
        //1.使用双指针来固定滑动窗口
        //2.使用Hash来加快执行效率，key代表值，val代表出现的次数
        //3.把滑动窗口中的值，放入map中，如果就出现一次，right继续往后走，否则left往后走
        // 直到所有数都只出现一次
        HashMap<Integer, Integer> map = new HashMap<>();
        int count = 0;
        int left = 0;
        int right = 0;
        while(right < arr.length){
            if(map.containsKey(arr[right])){
                map.put(arr[right], map.get(arr[right])+1);
            }else{
                map.put(arr[right], 1);
            }
            while(map.get(arr[right]) > 1){
                map.put(arr[left], map.get(arr[left])-1);
                left++;
            }
            count = Math.max(count, right-left+1);
            right++;
        }
        return count;
    }
}
