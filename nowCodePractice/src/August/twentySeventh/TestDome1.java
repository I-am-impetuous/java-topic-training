package August.twentySeventh;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA
 * Description: 两数之和（两数相加等于特定值，返回两数的下标）
 * User: 我很浮躁
 * Date: 2023-08-27
 * Time: 14:15
 */
public class TestDome1 {
    public int[] twoSum (int[] numbers, int target) {
        // write code here
        // 1.使用哈希表来加快第二次遍历
        HashMap<Integer, Integer> map = new HashMap<>();
        // 2.当哈希表中没有（target - numbers[i]）时，把 numbers[i] 放入哈希表中
        // 3.当哈希表中有（target - numbers[i]）时，直接返回两个下标就可以了
        for(int i=0; i<numbers.length; i++){
            if(map.containsKey(target - numbers[i])){
                return new int[]{map.get(target-numbers[i])+1, i+1};
            }else{
                map.put(numbers[i], i);
            }
        }
        return null;
    }
}
