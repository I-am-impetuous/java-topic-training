package August.twentySeventh;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description: 三数之和（三数之和等于零，计算出数组中有多少对）
 * User: 我很浮躁
 * Date: 2023-08-27
 * Time: 14:46
 */
public class TestDome2 {
    public ArrayList<ArrayList<Integer>> threeSum (int[] num) {
        // write code here
        ArrayList<ArrayList<Integer>> arrayLists = new ArrayList<>();
        //1.小于3个的情况
        if(num.length < 3){
            return arrayLists;
        }
        //2.把数组进行排序
        Arrays.sort(num);
        //3.找到三个元素中的最小值
        //4.在剩余的元素中找到两个数相加等于负最小值
        for(int i=0; i<num.length; i++){
            if(i != 0 && num[i] == num[i-1]){
                continue;
            }
            int start = i+1;
            int end = num.length - 1;
            int target = -num[i];
            while(start < end){
                if(num[start] + num[end] == target){
                    ArrayList<Integer> arrayList = new ArrayList<>();
                    arrayList.add(num[i]);
                    arrayList.add(num[start]);
                    arrayList.add(num[end]);
                    arrayLists.add(arrayList);
                    //5.去除重复的元素
                    while(start+1<end && num[start] == num[start+1]){
                        start++;
                    }
                    while(start<end-1 && num[end-1] == num[end]){
                        end--;
                    }
                    start++;
                    end--;
                }else if(num[start] + num[end] < target){
                    start++;
                }else if(num[start] + num[end] > target){
                    end--;
                }
            }
        }
        return arrayLists;
    }
}
