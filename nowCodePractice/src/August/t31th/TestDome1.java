package August.t31th;

import java.util.HashSet;

/**
 * Created with IntelliJ IDEA
 * Description: 去除重复的字符，并且不能换位置
 * User: 我很浮躁
 * Date: 2023-08-31
 * Time: 17:05
 */
public class TestDome1 {

    //1.去除重复的字符，并且不能换位置
    //2.遍历字符串，把不是重复的字符放在hash表中
    public static String test(String str){
        HashSet<Character> set = new HashSet<>();
        String ret = "";
        for(int i=0; i<str.length(); i++){
            if(!(set.contains(str.charAt(i)))){
                ret += str.charAt(i);
                set.add(str.charAt(i));
            }
        }
        return ret;
    }
    public static void main(String[] args) {
        String str = "hello";
        System.out.println(test(str));
    }
}
