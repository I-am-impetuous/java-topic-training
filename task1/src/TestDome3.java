/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-10
 * Time: 18:38
 */
public class TestDome3 {
    public static void main(String[] args) {

        Thread thread1 = new Thread(()->{
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("thread1");
        });

        Thread thread2 = new Thread(()->{
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("thread2");
        });

        Thread thread3 = new Thread(()->{
            System.out.println("thread3");
        });

        thread1.start();
        thread2.start();
        thread3.start();
    }
}
