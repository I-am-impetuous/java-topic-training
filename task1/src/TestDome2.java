/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-10
 * Time: 18:16
 */

class MyThread extends Thread{
    @Override
    public void run() {
        while(true){

        }
    }
}

class MyRunnable implements Runnable{
    @Override
    public void run() {
        while(true){

        }
    }
}

public class TestDome2 {
    public static void main(String[] args) {
        Thread thread1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){

                }
            }
        });
        thread1.start();

        Thread thread2 = new Thread(){
            @Override
            public void run() {
                while(true){

                }
            }
        };
        thread2.start();

        Thread thread3 = new Thread(()->{
            while(true){

            }
        });
        thread3.start();

        Thread thread4 = new MyThread();
        thread4.start();

        Thread thread5 = new Thread(new MyRunnable());
        thread5.start();
    }
}
