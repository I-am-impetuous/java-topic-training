import sun.security.provider.Sun;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-10
 * Time: 17:44
 */

 class Sum{
    private int sum = 0;
    //实现线程安全
    synchronized public void increase(int num){
        sum += num;
    }
    public int getSum() {
        return sum;
    }
}

public class TestDome1 {
    private static final int COUNT = 1000_0000;

    public static void main(String[] args) throws InterruptedException {
        int sum2 = 0;
        int[] array = new int[COUNT];
        //给数组赋值
        for(int i=0; i<COUNT; i++){
            array[i] = (int)(100*Math.random()) + 1;
        }
        Sum sum = new Sum();
        //记录开始时间
        Long begin = System.currentTimeMillis();
        Thread thread1 = new Thread(()->{
            for(int i=0; i<COUNT; i+=2){
                //所有偶数下标相加
                sum.increase(array[i]);
            }
        });
        thread1.start();

        Thread thread2 = new Thread(()->{
            for(int i=1; i<COUNT; i+=2){
                //所有奇数下标相加
                sum.increase(array[i]);
            }
        });
        thread2.start();

        //等待thread1线程结束
        thread1.join();
        //等待thread2线程结束
        thread2.join();
        //记录结束时间
        Long end = System.currentTimeMillis();

        System.out.println("用时多长时间: " + (end - begin));
        System.out.println("总数和为: " + (sum.getSum()));
    }
}
