/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-09
 * Time: 20:31
 */
public class TestDome5 {
    public int addAB(int A, int B) {
        // write code here
        int xor, and;
        while(B != 0){   //判断进位数是为零
            xor = A ^ B;   //两个数相加不考虑进位
            and = A & B;   //判断是否进位
            A = xor;
            B = and << 1;
        }
        return A;
    }
}
