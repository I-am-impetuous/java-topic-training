import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-09
 * Time: 20:30
 */
public class TestDome4 {
    public static int func(int m, int n){
        if((m == 1 && n >= 1) || (n == 1 && m >=1)){
            return m + n;
        }
        return func(n, m-1) + func(n-1, m);
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int m = sc.nextInt();
        int n = sc.nextInt();
        int sum = func(m, n);
        System.out.println(sum);
    }
}
