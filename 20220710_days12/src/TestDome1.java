import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-10
 * Time: 17:26
 */
public class TestDome1 {
    public static boolean isPrime(int num){
        if(num == 2){
            return true;
        }
        for(int i=2; i<Math.sqrt(num)+1; i++){
            if(num % i == 0){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int i = 0, j = num, num1 = 0, num2 = 0;
        while(i <= j){
            if(isPrime(i) && isPrime(j)){
                num1 = i;
                num2 = j;
            }
            i++;
            j--;
        }
        System.out.println(num1);
        System.out.println(num2);
    }
}
