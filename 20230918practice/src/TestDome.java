/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-18
 * Time: 23:33
 */
public class TestDome {
    public String LCS (String s1, String s2) {
        // write code here
        int length1 = s1.length();
        int length2 = s2.length();
        int[][] array = new int[length1+1][length2+1];
        for(int i=1; i<=length1; i++){
            for(int j=1; j<=length2; j++){
                if(s1.charAt(i-1) == s2.charAt(j-1)){
                    array[i][j] = array[i-1][j-1] + 1;
                }else{
                    array[i][j] = Math.max(array[i][j-1], array[i-1][j]);
                }
            }
        }
        if(array[length1][length2] == 0){
            return "-1";
        }
        StringBuffer stringBuffer = new StringBuffer();
        while (length1>0 && length2>0){
            if(s1.charAt(length1-1) == s2.charAt(length2-1)){
                stringBuffer.append(s1.charAt(length1-1));
                length1--;
                length2--;
            }else {
                if(array[length1-1][length2] > array[length1][length2-1]){
                    length1--;
                }else {
                    length2--;
                }
            }
        }
        return stringBuffer.reverse().toString();
    }
}
