/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-19
 * Time: 23:54
 */
public class TestDome2 {
    public String LCS (String str1, String str2) {
        // write code here
        int n=str1.length();
        int m=str2.length();
        int max = 0;
        int pos = 0;
        int[][] dp = new int[n+1][m+1];
        for(int i=1; i<=n; i++){
            for(int j=1; j<=m; j++){
                if(str1.charAt(i-1) == str2.charAt(j-1)){
                    dp[i][j] = dp[i-1][j-1]+1;
                }else{
                    dp[i][j] = 0;
                }
                if(dp[i][j] > max){
                    max = dp[i][j];
                    pos = i-1;
                }
            }
        }
        return str1.substring(pos-max+1, pos+1);
    }
}
