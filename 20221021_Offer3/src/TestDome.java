import java.util.Arrays;

/**
 * Created with IntelliJ IDEA
 * Description:剑指Offer
 * User: 我很浮躁
 * Date: 2022-10-21
 * Time: 23:19
 */
public class TestDome {
    public static void reOrderArray(int [] array) {
        int k = 0;
        for(int i=0; i<array.length; i++){
            if(array[i]%2 == 1){
                int j = i;
                int temp = array[i];
                while (j>k){
                    array[j] = array[j-1];
                    j--;
                }
                array[k++] = temp;
            }
        }
        System.out.println(Arrays.toString(array));
    }

    public static void main(String[] args) {
        int array[] = {1, 2, 3, 4, 5, 6};
        System.out.println(Arrays.toString(array));
        reOrderArray(array);
    }
}
