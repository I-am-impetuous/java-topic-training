/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-10-24
 * Time: 9:37
 */
public class TestDome {
    public String replaceSpace(StringBuffer str) {
        int spaceCount = 0;
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i) == ' '){
                spaceCount++;
            }
        }
        int oldLength = str.length();
        int newLength = oldLength + spaceCount * 2;
        str.setLength(newLength);
        int oldIndex = oldLength - 1;
        int newIndex = newLength - 1;
        while (oldIndex >= 0 && newIndex >= 0){
            if(str.charAt(oldIndex) == ' '){
                str.setCharAt(newIndex--, '0');
                str.setCharAt(newIndex--, '2');
                str.setCharAt(newIndex--, '%');
            }else {
                str.setCharAt(newIndex--, str.charAt(oldIndex));
            }
            oldIndex--;
        }
        return str.toString();
    }
}
