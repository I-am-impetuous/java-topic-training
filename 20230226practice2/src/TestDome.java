import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-02-26
 * Time: 22:37
 */
public class TestDome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x = sc.nextInt();
        double y = 0;
        if(x < -5){
            y = 3*x*x + 2*x - 1;
        }else if(x <= 5){
            y = x*Math.sin(x) + Math.pow(2,x);
        }else{
            y = Math.pow((x-5),1/2) + Math.log10(x);
        }
        System.out.println(y);
    }
}
