/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-12-17
 * Time: 23:55
 */

class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}

public class TestDome {
    public ListNode deleteDuplication(ListNode pHead) {
        if(pHead == null){
            return null;
        }
        ListNode head = new ListNode(0);
        head.next = pHead;
        ListNode fast = head.next;
        ListNode slow = head;
        while(fast != null && fast.next != null){
            if(fast.val != fast.next.val){
                fast = fast.next;
                slow = slow.next;
            }
            int flag = 0;
            while(fast != null && fast.next != null && fast.val == fast.next.val){
                fast = fast.next;
                flag = 1;
            }
            if(flag == 1){
                fast = fast.next;
                slow.next = fast;
            }
        }
        return head.next;
    }
}
