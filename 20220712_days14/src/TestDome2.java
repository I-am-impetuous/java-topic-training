import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-12
 * Time: 22:05
 */
public class TestDome2{
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int[] array = new int[num];
        for(int i=0; i<num; i++){
            array[i] = sc.nextInt();
        }
        Arrays.sort(array);
        int sum = 0;
        int mul = 1;
        int count = 0;
        for(int i=0; i<num; i++){
            sum = 0; mul = 1;
            for(int j=0; j<array.length - i; j++){
                sum += array[j];
                mul *= array[j];
            }
            if(sum > mul){
                count++;
            }
        }
        System.out.println(count);
    }
}
