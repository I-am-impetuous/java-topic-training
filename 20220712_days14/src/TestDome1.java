import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-12
 * Time: 17:49
 */
public class TestDome1 {
    public static boolean isPrime(int year){
        return ((year%4 == 0) && (year%100 != 0)) || year%400 == 0;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        int month = sc.nextInt();
        int days = sc.nextInt();
        int sum = 0;
        for(int i=1; i<month; i++){
            switch(i){
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    sum += 31;
                    break;
                case 2:
                    if(isPrime(year)){
                        sum += 29;
                    }else{
                        sum += 28;
                    }
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    sum += 30;
                    break;
            }
        }
        sum += days;
        System.out.println(sum);
    }
}
