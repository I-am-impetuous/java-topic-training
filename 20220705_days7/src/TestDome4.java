import java.util.Stack;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-08
 * Time: 16:18
 */
public class TestDome4 {
    public static boolean chkParenthesis(String A, int n) {
        // write code here
        if(A.length()%2 != 0){
            return false;
        }
        Stack<Character> stack = new Stack();
        for(char c: A.toCharArray()){
            if(c == '('){
                stack.push(c);
            }else if(c == ')'){
                if(stack.isEmpty()){
                    return false;
                }else if(stack.peek() == '('){
                    stack.pop();
                }else{
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {

    }
}
