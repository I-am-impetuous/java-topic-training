import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-05
 * Time: 15:44
 */
public class TestDome2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        int fib1 = 0;
        int fib2 = 1;
        while(!(num <= fib2 && num >= fib1)){
            int temp = fib2;
            fib2 = fib2 + fib1;
            fib1 = temp;
        }
        int count1 = num-fib1;
        int count2 = fib2-num;
        if(count1 > count2){
            System.out.println(count2);
        }else{
            System.out.println(count1);
        }
    }

}
