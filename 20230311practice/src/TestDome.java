import java.util.HashSet;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-11
 * Time: 20:24
 */
public class TestDome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String string1 = sc.nextLine();
        String string2 = sc.nextLine();
        HashSet<Character> hashSet = new HashSet<>();
        for(int i=0; i<string2.length(); i++){
            hashSet.add(string2.charAt(i));
        }
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<string1.length(); i++){
            if(!hashSet.contains(string1.charAt(i))){
                sb.append(string1.charAt(i));
            }
        }
        System.out.println(sb.toString());
    }
}
