import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-07
 * Time: 16:50
 */

public class TestDome4 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int W = sc.nextInt();
        int H = sc.nextInt();
        int array[][] = new int[W][H];
        int flag = 1;
        int count = 0;
        for(int i=0; i<W; i++){
            for(int j=0; j<H; j++){
                flag = 1;
                if(j+2<H && array[i][j+2] == 1){
                    flag = 0;
                }
                if(j-2>=0 && array[i][j-2] == 1){
                    flag = 0;
                }
                if(i-2>=0 && array[i-2][j] == 1){
                    flag = 0;
                }
                if(i+2<W && array[i+2][j] == 1){
                    flag = 0;
                }
                if(flag == 1){
                    count++;
                    array[i][j] = 1;
                }
            }
        }
        System.out.println(count);
    }
}
