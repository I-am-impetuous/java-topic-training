/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-07
 * Time: 16:20
 */
public class TestDome3 {
    public static int StrToInt(String str) {
        if(str.isEmpty() || str == null){
            return 0;
        }
        int flag = 1;
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i) == '-'){
                flag = -1;
                continue;
            }else if(str.charAt(i) >= '0' && str.charAt(i) <= '9'){
                sb.append(str.charAt(i));
            }else if(str.charAt(i) == '+' || str.charAt(i) == '/'){
                continue;
            }else{
                return 0;
            }
        }
        int sum = 0;
        for(int i=0; i<sb.length(); i++){
            sum = sum*10 + (sb.charAt(i) - '0');
        }
        return flag * sum;
    }

    public static void main(String[] args) {
        int sum = StrToInt("##");
        System.out.println(sum);
    }
}
