import java.util.*;
/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-03
 * Time: 16:59
 */
public class TestDome2 {

    public static void main(String[] args) {
        int sum = StrToInt("-*+2147483647");
        System.out.println(sum);
    }


    public static int StrToInt(String str) {
        StringBuffer sb = new StringBuffer();
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i) >= '0' && str.charAt(i) <= '9'){
                sb.append(str.charAt(i));
            }else{
                if(str.charAt(i) != '+' && str.charAt(i) != '-' && str.charAt(i) != '/'){
                    return 0;
                }
            }
        }
        int sum = 0;
        for(int i=0; i<sb.length(); i++){
            int temp = (int)(sb.charAt(i)-'0');
            sum += temp * Math.pow(10, str.length()-1-i);
        }
        return sum;
    }

}
