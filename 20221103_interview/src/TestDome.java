import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-11-03
 * Time: 10:50
 */
public class TestDome {

    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();
        map.put("Java", "Java Value");
        map.put("MySQL", "MySQL Value");
        map.put("C++", "C++ Value");

        //EntrySet 遍历
        System.out.println("EntrySet 遍历");
        for(Map.Entry<String, String> entry : map.entrySet()){
            System.out.print(entry.getKey() + "->" + entry.getValue() + "  ");
        }
        System.out.println();

        //KeySet 遍历
        System.out.println("KeySet 遍历");
        for(String key : map.keySet()){
            System.out.print(key + "->" + map.get(key) + "  ");
        }
        System.out.println();

        //EntrySet 迭代器遍历
        System.out.println("EntrySet 迭代器遍历");
        Iterator<Map.Entry<String, String>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()){
            Map.Entry<String, String> entry = iterator.next();
            System.out.print(entry.getKey() + "->" + entry.getValue() + "  ");
        }
        System.out.println();

        //KeySet 迭代器遍历
        System.out.println("KeySet 迭代器遍历");
        Iterator<String> iterator1 = map.keySet().iterator();
        while (iterator1.hasNext()){
            String key = iterator1.next();
            System.out.print(key + "->" + map.get(key) + "  ");
        }
        System.out.println();

        //Lambda 遍历
        System.out.println("Lambda 遍历");
        map.forEach((key, value) -> {
            System.out.print(key + "->" + value + "  ");
        });
        System.out.println();

        //Stream 单线程遍历
        System.out.println("Stream 单线程遍历");
        map.entrySet().stream().forEach((entry) -> {
            System.out.print(entry.getKey() + "->" + entry.getValue() + "  ");
        });
        System.out.println();

        //Stream 多线程遍历
        System.out.println("Stream 多线程遍历");
        map.entrySet().stream().parallel().forEach((entry) -> {
            System.out.print(entry.getKey() + "->" + entry.getValue() + "  ");
        });
        System.out.println();


    }
}
