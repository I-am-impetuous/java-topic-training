/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-12-04
 * Time: 17:42
 */
public class SingletonLazy {
    private volatile static SingletonLazy singletonLazy = null;

    private SingletonLazy(){}

    public static SingletonLazy getSingletonLazy(){
        if(singletonLazy == null){
            synchronized (SingletonLazy.class){
                if(singletonLazy == null){
                    return new SingletonLazy();
                }
            }
        }
        return singletonLazy;
    }
}
