/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-12-04
 * Time: 17:43
 */
public class SingletonHunger {
    private static SingletonHunger singletonHunger = new SingletonHunger();

    private SingletonHunger (){}

    public static SingletonHunger getSingletonHunger(){
        return singletonHunger;
    }
}
