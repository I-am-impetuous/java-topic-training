import java.util.*;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-03-31
 * Time: 16:17
 */
public class TestDome {



    public double get_max_profit (double M, int N, double[] historyPrices, int K) {
        // write code here
        List<List<Double>> lists = new ArrayList<>();
        for(int i=1; i<N; i++){
            int start = i-1;
            List<Double> list = new ArrayList<>();
            while (historyPrices[i] >= historyPrices[i-1] && i<N){
                i++;
            }
            double weight = 0;
            weight = historyPrices[i-1]/historyPrices[start];
            list.add((double) start);
            list.add((double) i-1);
            list.add(weight);
            lists.add(list);
        }
        double sum = 0;
        for(int i=0; i<lists.size(); i++){
            double weight = lists.get(i).get(3);
            sum = sum * weight;
        }
        return sum;
    }


//    public static void main(String[] args) {
//        System.out.println(Math.pow(2,4));
//    }

    public static void main0(String[] args) {
        Scanner sc = new Scanner(System.in);
        int maxNum = sc.nextInt();
        for(int i=maxNum; i>10; i--){
            int temp = i;
            int sum = 0;
            int length = (temp+"").length();
            while (temp > 9){
                int num = temp%10;
                temp = temp/10;
                sum += Math.pow(num, length);
            }
            sum += Math.pow(temp, length);
            if(i == sum){
                System.out.println(sum);
            }
        }
    }
}
