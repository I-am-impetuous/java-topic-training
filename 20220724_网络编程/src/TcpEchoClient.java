import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-24
 * Time: 15:59
 */
public class TcpEchoClient {
    //创建 Socket 对象
    private Socket socket = null;

    //这里传入的是服务器的地址，和端口号，为了能找到服务器
    public TcpEchoClient(String serverIP, int serverPort) throws IOException {
        socket = new Socket(serverIP, serverPort);
    }

    public void start(){
        System.out.println("链接建立成功");
        Scanner scanner = new Scanner(System.in);
        try(InputStream inputStream = socket.getInputStream()) {
            try(OutputStream outputStream = socket.getOutputStream()){
                while(true){
                    //客户端发出请求
                    String request = scanner.next();
                    //向文件中写入请求
                    PrintWriter printWriter = new PrintWriter(outputStream);
                    printWriter.println(request);
                    printWriter.flush();

                    //接收文件中返回来的响应
                    Scanner scanner1 = new Scanner(inputStream);
                    String response = scanner1.next();
                    System.out.printf("request: %s, response: %s\n", request, response);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //启动客户端
    public static void main(String[] args) throws IOException {
        TcpEchoClient tcpEchoClient = new TcpEchoClient("127.0.0.1", 9090);
        tcpEchoClient.start();
    }
}
