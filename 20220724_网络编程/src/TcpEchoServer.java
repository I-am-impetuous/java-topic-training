import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-24
 * Time: 15:26
 */
public class TcpEchoServer {
    //创建ServerSocket对象，是为了建立连接和接收客户端所传的信息
    private ServerSocket serverSocket = null;

    //服务器要指定端口号，方便客户端找到服务器
    public TcpEchoServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void start() throws IOException {
        System.out.println("程序启动!!");
        while(true) {
            //serverSocket 使用 accept 方法建立连接，这个方法会返回客户端的请求信息。
            //用 Socket 这个类来接收客户端的信息
            Socket socket = serverSocket.accept();
            //因为要建立连接，processConnection 方法要执行完，也就是客户端断开链接，才能执行下一个客户端请求。
            //这样就不能同时处理多个客户端的请求了，为了解决这个问题，使用了多线程，每一个线程负责一个客户端。
            Thread thread = new Thread(()->{
                processConnection(socket);
            });
            thread.start();
        }
    }

    private void processConnection(Socket socket) {
        System.out.printf("[%s, %d] 建立连接!!\n", socket.getInetAddress().toString(), socket.getPort());
        //Socket 对象要使用文件来进行操作，每一个对象中都自带有一个文件。
        try(InputStream inputStream = socket.getInputStream()) {
            try(OutputStream outputStream = socket.getOutputStream()){
                //使用Scanner类，为了简化代码，写起来更加的方便
                Scanner scanner = new Scanner(inputStream);
                while(true){
                    //如果客户端的请求接收完成，就断开链接，退出循环
                    if(!scanner.hasNext()){
                        System.out.printf("[%s, %d] 断开链接!!!", socket.getInetAddress().toString(),socket.getPort());
                        break;
                    }
                    //接收客户端的请求
                    String request = scanner.next();
                    //服务器对客户端的请求，做出响应
                    String response = process(request);
                    //把客服端的响应写入文件中，让客户端接收
                    PrintWriter printWriter = new PrintWriter(outputStream);
                    printWriter.println(response);
                    //其中的 flush 是刷新缓冲区，为了让客户端及时接收到数据
                    printWriter.flush();
                    System.out.printf("[%s, %d], request:%s, response:%s\n", socket.getInetAddress().toString(),
                            socket.getPort(), request, response);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                //关闭文件
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //服务器根据请求做出响应
    private String process(String request) {
        return request;
    }

    //启动服务器
    public static void main(String[] args) throws IOException {
        TcpEchoServer server = new TcpEchoServer(9090);
        server.start();
    }
}
