import java.io.IOException;
import java.net.*;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-24
 * Time: 8:43
 */
//源 IP：本机 IP
//源端口：系统分配的端口
//目的 IP：服务器的 IP
//目的端口：服务器端口号
//协议类型：UDP

public class UdpEchoClient {
    //网络编程的基础要有一个socket对象。
    private DatagramSocket socket = null;
    private String serverIP;
    private int serverPort;

    //使用构造函数来初始化socket对象，服务器的IP和端口号
    public UdpEchoClient() throws SocketException {
        socket = new DatagramSocket();
        serverIP = "127.0.0.1";
        serverPort = 9090;
    }

    public void start() throws IOException {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("-> ");
            //输入要传给服务器的内容。
            String request = sc.nextLine();
            //要传给服务器的内容要封装成一个数据报（DatagramPacket），数据报中要有目标地址的IP和端口号
            DatagramPacket requestPacket = new DatagramPacket(request.getBytes(), request.getBytes().length,
                    InetAddress.getByName(serverIP), serverPort);
            //传送数据报
            socket.send(requestPacket);
            //接收要用数据报来接收，先构造出数据报
            DatagramPacket responsePacket = new DatagramPacket(new byte[1024], 1024);
            //接收服务器传来的数据报
            socket.receive(responsePacket);
            //把数据报中的内容变成字符串
            String response = new String(responsePacket.getData(), 0, responsePacket.getLength(), "UTF-8");
            System.out.printf("request: %s, response: %s\n", request, response);
        }
    }

    public static void main(String[] args) throws IOException {
        UdpEchoClient client = new UdpEchoClient();
        client.start();
    }
}
