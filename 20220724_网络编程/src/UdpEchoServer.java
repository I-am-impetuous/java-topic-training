import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-24
 * Time: 8:42
 */
//源 IP：本机 IP
//源端口：服务器绑定的端口
//目的 IP：客户端的 IP
//目的端口：客户端的端口号
//协议类型：UDP

public class UdpEchoServer {
    //网络编程的基础要有一个socket对象。
    private DatagramSocket socket = null;

    //在初始化的时候，要给服务器定义一个端口号，我们只有知道了这个端口号，才能从客户端发来请求。
    public UdpEchoServer(int port) throws SocketException {
        //创建一个带有端口号的socket对象。
        socket = new DatagramSocket(port);
    }

    public void start() throws IOException {
        System.out.println("启动服务器！！！");
        while(true){
            //因为UDP接收的是一个数据报，所以我们要先定义一个空的数据报，来接收数据。
            DatagramPacket requestPacket = new DatagramPacket(new byte[1024], 1024);
            //其中的requestPacket是一个输出型参数，这个参数可以带出来数据，这个数据就是客户端传来的数据
            socket.receive(requestPacket);
            //先把数据报转换成字符串，为了方便处理
            String request = new String(requestPacket.getData(), 0,requestPacket.getLength(), "UTF-8");
            //使用process函数来实现服务器根据请求，来生成响应。
            String response = process(request);
            //因为UDP传输的是一个数据报，所以要生成一个要传输出去的数据报，其中包含了响应的内容和地址。
            DatagramPacket responsePacket = new DatagramPacket(request.getBytes(), request.getBytes().length,
                    requestPacket.getSocketAddress());
            //响应客户端的请求。
            socket.send(responsePacket);
            System.out.printf("[%s,%d], request:%s, response:%s\n",
                    requestPacket.getAddress(), requestPacket.getPort(),request,response);
        }
    }

    //这里我们实现的是一个回显效果，就是接收什么，响应什么。
    private String process(String request) {
        return request;
    }

    public static void main(String[] args) throws IOException {
        UdpEchoServer server = new UdpEchoServer(9090);
        server.start();
    }

}
