import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-21
 * Time: 18:27
 */
public class TestDome1 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        for(int i=0; i<num; i++){
            int n = sc.nextInt();
            int k = sc.nextInt();
            int array1[] = new int[2*n];
            int array2[] = new int[2*n];
            for(int j=0; j<2*n; j++){
                array1[j] = sc.nextInt();
            }
            for(int m=0; m<k; m++){
                for(int j=0; j<n; j++){
                    array2[2*j] = array1[j];
                }
                for(int j=n; j<2*n; j++){
                    array2[2*(j-n)+1] = array1[j];
                }
                int[] temp = array1;
                array1 = array2;
                array2 = temp;
            }
            for(int m=0; m<2*n; m++){
                if(m == 0){
                    System.out.print(array1[m]);
                }else{
                    System.out.print(" " + array1[m]);
                }
            }
            System.out.println();
        }
    }
}
