import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-25
 * Time: 17:20
 */
public class TestDome3 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String str = sc.next();
            while(str.length() > 1) {
                int sum = 0;
                for (int i = 0; i < str.length(); i++) {
                    sum += (str.charAt(i) - '0');
                }
                str = String.valueOf(sum);
            }
            System.out.println(str);
        }
    }
}
