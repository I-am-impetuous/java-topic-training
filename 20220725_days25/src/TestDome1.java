import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-25
 * Time: 16:35
 */
public class TestDome1 {
    public static void main(String[] args){
        int[] array = new int[10001];
        array[0] = 1;
        array[1] = 1;
        for(int i=2; i<10001; i++){
            array[i] = array[i-1] + array[i-2];
            array[i] = array[i]%10000;
        }
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int num = sc.nextInt();
            StringBuilder str = new StringBuilder();
            for(int i=0; i<num; i++){
                int n = sc.nextInt();
                int temp = array[n];
                str.append(String.format("%04d", temp));
            }
            System.out.println(str);
        }

    }
}
