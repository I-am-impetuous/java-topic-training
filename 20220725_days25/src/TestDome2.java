import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-25
 * Time: 17:12
 */
public class TestDome2 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            long num = sc.nextInt();
            while(num > 9){
                long temp1 = num;
                int sum = 0;
                while(temp1 > 9){
                    long temp2 = temp1%10;
                    sum += temp2;
                    temp1 /= 10;
                }
                num = sum + temp1;
            }
            System.out.println(num);
        }
    }
}
