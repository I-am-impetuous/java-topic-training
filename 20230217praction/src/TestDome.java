import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2023-02-17
 * Time: 23:33
 */
public class TestDome {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        String str1 = sc.next();
        String str2 = sc.next();

        // 字符串转换为数组
        char[] c1 = str1.toCharArray();
        char[] c2 = str2.toCharArray();

        // 构建数组
        int[][] array = new int[c1.length+1][c2.length+2];

        // 初始化第一行
        for(int i=0; i<=c1.length; i++){
            array[i][0] = 0;
        }

        // 初始化第一列
        for(int j=0; j<c2.length; j++){
            array[0][j] = 0;
        }

        // 主内容
        int temp = 0;
        for(int i=1; i<=c1.length; i++){
            for(int j=1; j<=c2.length; j++){
                if(c1[i-1] == c2[j-1]){
                    array[i][j] = array[i-1][j-1] + 1;
                }else{
                    array[i][j] = 0;
                }
                temp = Math.max(temp, array[i][j]);
            }
        }
        System.out.println(temp);
    }
}
