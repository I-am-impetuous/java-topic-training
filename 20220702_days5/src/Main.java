/**
 * Created with IntelliJ IDEA
 * Description:
 * User: 我很浮躁
 * Date: 2022-07-04
 * Time: 12:35
 */
import java.util.Scanner;

public class Main{

    public static int getMax(int a, int b){
        return a>b? a : b;
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        int size = sc.nextInt();
        int[] array = new int[size];
        for(int i=0; i<array.length; i++){
            array[i] = sc.nextInt();
        }
        int sum = array[0];
        int max = array[0];

        for(int i=1; i<array.length; i++){
            sum = getMax(sum + array[i], array[i]);
            if(max < sum){
                max = sum;
            }
        }
        System.out.println(max);
    }
}