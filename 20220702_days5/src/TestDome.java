import java.util.Scanner;

/**
 * Created with IntelliJ IDEA
 * Description:求回文子串
 * User: 我很浮躁
 * Date: 2022-07-02
 * Time: 15:15
 */
public class TestDome {

    public static boolean isPalindrome(StringBuilder str){
        for(int i=0; i<str.length()/2; i++){
            if(str.charAt(i) != str.charAt(str.length()-i-1)){
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        StringBuilder sb1 = new StringBuilder(scanner.nextLine());
        StringBuilder sb2 = new StringBuilder(scanner.nextLine());
        int count = 0;
        for(int i=0; i<sb1.length()+1; i++){
            StringBuilder sb3 = new StringBuilder();
            sb3.append(sb1,0, i);
            sb3.append(sb2);
            sb3.append(sb1,i,sb1.length());
            if(isPalindrome(sb3)){
                count++;
            }
        }
        System.out.println(count);
    }
}
